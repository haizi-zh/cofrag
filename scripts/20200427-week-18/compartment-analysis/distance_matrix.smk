from snakemake.remote.S3 import RemoteProvider as S3RemoteProvider
S3 = S3RemoteProvider()

envvars:
    "AWS_ACCESS_KEY_ID",
    "AWS_SECRET_ACCESS_KEY"

def eprint(*args, **kwargs):
    import sys

    sys.stderr.write('\x1b[33m')
    print(*args, file=sys.stderr, **kwargs)
    sys.stderr.write('\x1b[0m')

# >>> Configuration >>>
OUTPUT_DIR = config["OUTPUT_DIR"]
CORES = config.get("CORES", 1)
METRICS = config["METRICS"].split(",") if "METRICS" in config else ["ks"]

# chr_spec is the config line: CHROM=21,22 or CHROM=chr21,X
# When the input is an integer, snakemake will do some implicit conversion, so that
# CHROM=21,22 results in (21, 22). However, CHROM=chr21,X indicates "chr21,X"
# We need some consistency here.
# Same for BIN_SIZE
def process_integer_config(cfg):
    if isinstance(cfg, tuple):
        return [str(v) for v in cfg]
    elif cfg:
        return cfg.split(",")
    else:
        return []

CHROM = process_integer_config(config.get("CHROM", "22"))
BIN_SIZE = process_integer_config(config.get("BIN_SIZE", "500"))

eprint("Config: ")
eprint(f"CORES: {CORES}")
eprint(f"METRICS: {METRICS}")
eprint(f"CHROM: {CHROM}")
eprint(f"BIN_SIZE: {BIN_SIZE}")
# <<< Configuration <<<

rule all:
    input:
        expand("%s/dm.chr{chrom}.{bin_size}kb.{metrics}.txt.gz" % OUTPUT_DIR, \
            chrom=CHROM, bin_size=BIN_SIZE, metrics=METRICS)

rule distance_matrix:
    input:
        "results/20200406-week-15/bam_filter/bh01.chr{chrom}.filtered.bam",
        "results/20200406-week-15/bam_filter/bh01.chr{chrom}.filtered.bam.bai"
    output:
        "%s/dm.chr{chrom}.{bin_size}kb.{metrics}.txt.gz" % OUTPUT_DIR
    threads: CORES
    shell:
        """
        Rscript src/cfhic.R distance -m {wildcards.metrics} -s {wildcards.bin_size}000 -b 1000000 -o {output} -n {threads} {input[0]}
        """