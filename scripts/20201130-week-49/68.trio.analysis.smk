# Extract separated reads and generate a subset BAM file
# Based on the separated fragments
rule subset_bam_separated:
    input:
        bam="results/20201116-week-47/trio-variant-calling/bam-bqsr/{s1}_plasma.chr{chrom}.bqsr.bam",
        bai="results/20201116-week-47/trio-variant-calling/bam-bqsr/{s1}_plasma.chr{chrom}.bqsr.bam.bai",
        frag="results/20201123-week-48/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.frags.chr{chrom}.bed.gz",
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
    output:
        bam="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.chr{chrom}.bam",
        bai="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.chr{chrom}.bam.bai",
    params:
        label=lambda wildcards: f"subset_bam_separated.{wildcards.s1}_plasma.chr{wildcards.chrom}",
    threads: 2
    resources:
        time=4320,
        time_min=1440,
        mem_mb=lambda wildcards, threads: threads * 4200,
    script: "68.trio.analysis.py"


# Calculate the GC bias metrics for the separated reads
rule gc_bias_separated:
    input:
        bam="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.chr{chrom}.bam",
        bai="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.chr{chrom}.bam.bai",
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
    output:
        gc_bias="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.gc_bias.chr{chrom}.txt",
        gc_bias_chart="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.gc_bias.chr{chrom}.pdf",
        gc_bias_summary="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.gc_bias.summary.chr{chrom}.txt",
    params:
        label=lambda wildcards: f"gc_bias_separated.{wildcards.s1}_plasma.chr{wildcards.chrom}",
    threads: 2
    resources:
        time=4320,
        time_min=1440,
        mem_mb=lambda wildcards, threads: threads * 4200,
    script: "68.trio.analysis.py"


# Calculate fragment stats in each bin, including GC-corrected coverage
# Data preview:
# chr     start   end     corrected_cov   cov_raw gc      mean    median  sd      skewness        kurtosis        short2long      peak    normalized_cov
# 1       500000  1000000 48790.08358149363       40902   50.17862388685175       172.3116229035255       172.3116229035255       29.569044693062583      1.1473523219614647      6.525585636949865       0.2253812277180263    168.12584581216163      0.34468840525133343
# 1       1000000 1500000 105298.06524819047      89336   59.8332 171.68816602489477      171.68816602489477      29.344975869237345      1.1610833550618485      6.588499054415178       0.23433182270365868     168.8058265472134     0.7439016193900628
rule frag_stats:
    input:
        frag="results/20201123-week-48/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.frags.chr{chrom}.bed.gz",
        gc_bias="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.gc_bias.chr{chrom}.txt",
        gc_contents="results/20200914-week-38/gc/gc_content.chr{chrom}.50kb.bed.gz",
    output:
        "results/20201130-week-49/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.frag_stats.{res}.chr{chrom}.bed.gz",
    params:
        label=lambda wildcards: f"frag_stats.{wildcards.s1}_plasma.chr{wildcards.chrom}",
    threads: 1
    resources:
        time=4320,
        time_min=240,
        mem_mb=lambda wildcards, threads: threads * 4200,
    script: "68.trio.analysis.py"


# Prepare the predictor dataset for the random forest model
rule prepare_predictor_dataset:
    input:
        cm="results/20201123-week-48/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.gm.{metrics}.{res}.seed{seed}.ss{ss}k.chr{chrom}.bed.gz",
        frag_stats="results/20201130-week-49/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.frag_stats.{res}.chr{chrom}.bed.gz",
        mappability="data/mappability/mappability.{res}.bed",
    output:
        "results/20201130-week-49/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.predictor.{metrics,[^\\.]+}.{res,[^\\.]+}.seed{seed}.ss{ss}k.chr{chrom}.bed.gz",
    params:
        label=lambda wildcards: f"prepare_predictor_dataset.{wildcards.s1}_plasma.chr{wildcards.chrom}",
    threads: 1
    resources:
        time=4320,
        time_min=240,
        mem_mb=lambda wildcards, threads: threads * 4200,
    script: "68.trio.analysis.py"


# Make predictions using random forest models
# Config: H2O_IP/H2O_PORT
rule rf_predict_cm:
    input:
        "results/20201130-week-49/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.predictor.{metrics}.{res}.seed{seed}.ss{ss}k.chr{chrom}.bed.gz",
    output:
        "results/20201130-week-49/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.rf_predicted.{metrics,[^\\.]+}.{res,[^\\.]+}.seed{seed}.ss{ss}k.chr{chrom}.bed.gz",
    params:
        label=lambda wildcards: f"rf_predict_cm.{wildcards.s1}_plasma.chr{wildcards.chrom}",
    threads: 1
    resources:
        time=4320,
        time_min=240,
        mem_mb=lambda wildcards, threads: threads * 4200,
    script: "68.trio.analysis.py"

    
    # input: 
    #     frag="results/20200914-week-38/training/frags/{entry_id}.hg19.chr{chr}.filtered.frag.len100_350.tsv.gz",
    #     gc_bias="results/20200914-week-38/training/gc/{entry_id}.hg19.gc_bias.txt",
    #     gc_content="results/20200914-week-38/gc/gc_content.chr{chr}.50kb.bed.gz"
    # output: 
    #     "results/20200914-week-38/training/gc/{entry_id,EE[0-9]+}.chr{chr}.gc_corrected.bed.gz"
    # threads: 1
    # resources:
    #     cpus=lambda wildcards, threads: threads,
    #     mem_mb=lambda wildcards, threads: threads * 4200,
    #     time_min=30
    # params:
    #     label=lambda wildcards: f"{wildcards.entry_id}.chr{wildcards.chr}",
    #     partition="RM-shared"
    # shell:
    #     """
    #     R_LIBS=/home/haizizh/Rpackages/4.0/ /opt/packages/R/4.0.0-mkl/bin/Rscript --vanilla \
    #         scripts/20200914-week-38/17.cov.gc_correction.R {wildcards.chr} 50000 500000 {input.frag} {input.gc_bias} {output}
    #     """