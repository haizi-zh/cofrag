__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
import os
import sys

# smk file home location
smk_path = path.normpath(path.dirname(__file__))
sys.path = [smk_path] + sys.path

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell
from snakemake.logging import logger


# Generate memory-related arguments for JRE/JDK CLI
def get_java_mem(resources, factor=1):
    memory = ""
    if "mem_mb" in resources.keys():
        # Only allocate a portion of the total memory
        memory = "-Xmx{}M".format(int(int(resources["mem_mb"]) * factor))

    return memory


# Generate JRE/JDK opts
def get_java_opts(params, resources, factor=1):
    java_opts = params.get("java_opts", "")
    java_mem = get_java_mem(resources, factor)
    if "-Xmx" not in java_opts:
        java_opts = f"{java_opts} {java_mem} "

    return java_opts


# Extract separated reads and generate a subset BAM file
# Based on the separated fragment
def subset_bam_separated():
    # input:
    #     bam="results/20201116-week-47/trio-variant-calling/bam-bqsr/{s1}_plasma.chr$M11189_plasma.chr{chrom}.bqsr.bam",
    #     bai="results/20201116-week-47/trio-variant-calling/bam-bqsr/{s1}_plasma.chr$M11189_plasma.chr{chrom}.bqsr.bam.bai",
    #     frag="results/20201123-week-48/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.frags.chr{chrom}.bed.gz",
    #     ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
    # output:
    #     bam="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.chr{chrom}.bam",
    #     bai="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.chr{chrom}.bam.bai",
    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell("zcat {snakemake.input.frag} | tail -n +2 | cut -d$'\\t' -f5 > {tempdir}/read_ids.txt")
        num_reads = shell("wc -l {tempdir}/read_ids.txt", read=True)
        logger.info(f"Found {num_reads} fragments from {snakemake.input.frag}:")
        shell("head {tempdir}/read_ids.txt")
        logger.info("...")

        java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.9)

        # logger.info("Creating interval list ...")
        # shell(
        #     "gatk --java-options '{java_opts}' "
        #     "BedToIntervalList "
        #     "-I {snakemake.input.frag} "
        #     "-O {tempdir}/intervals.txt "
        #     "-SD {snakemake.input.ref} "
        # )

        # logger.info("Extracting BAM reads the for separated fragments ...")
        # shell(
        #     "gatk --java-options '{java_opts}' "
        #     "FilterSamReads "
        #     "-I {snakemake.input.bam} "
        #     "-O {tempdir}/intervals.bam "
        #     "--INTERVAL_LIST {tempdir}/intervals.txt "
        #     "--FILTER includePairedIntervals "
        # )

        shell(
            "bedtools intersect "
            "-a {snakemake.input.bam} "
            "-b {snakemake.input.frag} "
            "-sorted "
            "-wa > {tempdir}/intervals.bam "
        )
        # shell("samtools index {tempdir}/intervals.bam")
        
        logger.info("Creating the subset BAM file ...")
        shell(
            "gatk --java-options '{java_opts}' "
            "FilterSamReads "
            "-I {tempdir}/intervals.bam "
            "-O {tempdir}/output.bam "
            "--CREATE_INDEX "
            "--READ_LIST_FILE {tempdir}/read_ids.txt "
            "--FILTER includeReadList "
        )
        # logger.info("Indexing ...")
        # shell("samtools index {tempdir}/output.bam")

        logger.info("Copying to destination...")
        shell("cp -a {tempdir}/output.bam {snakemake.output.bam}.tmp")
        shell("mv {snakemake.output.bam}.tmp {snakemake.output.bam}")
        shell("mv {tempdir}/output.bai {snakemake.output.bai}")

# Calculate the GC bias metrics for the separated reads
def gc_bias_separated():
    # input:
    #     bam="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.chr{chrom}.bam",
    #     bai="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.chr{chrom}.bam.bai",
    # output:
    #     gc_bias="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.gc_bias.chr{chrom}.txt",
    #     gc_bias_chart="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.gc_bias.chr{chrom}.pdf",
    #     gc_bias_summary="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.gc_bias.summary.chr{chrom}.txt",
    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")
        
        java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.9)
        shell(
            "gatk --java-options '{java_opts}' "
            "CollectGcBiasMetrics "
            "-I {snakemake.input.bam} "
            "-R {snakemake.input.ref} "
            "-O {tempdir}/gc_bias.txt "
            "-CHART {tempdir}/gc_bias.pdf "
            "-S {tempdir}/gc_bias.summary.txt "
        )

        logger.info("Copying to destination...")
        shell("cp -a {tempdir}/gc_bias.txt {snakemake.output.gc_bias}")
        shell("cp -a {tempdir}/gc_bias.pdf {snakemake.output.gc_bias_chart}")
        shell("cp -a {tempdir}/gc_bias.summary.txt {snakemake.output.gc_bias_summary}")

# Calculate fragment stats in each bin, including GC-corrected coverage
# Data preview:
# chr     start   end     corrected_cov   cov_raw gc      mean    median  sd      skewness        kurtosis        short2long      peak    normalized_cov
# 1       500000  1000000 48790.08358149363       40902   50.17862388685175       172.3116229035255       172.3116229035255       29.569044693062583      1.1473523219614647      6.525585636949865       0.2253812277180263    168.12584581216163      0.34468840525133343
# 1       1000000 1500000 105298.06524819047      89336   59.8332 171.68816602489477      171.68816602489477      29.344975869237345      1.1610833550618485      6.588499054415178       0.23433182270365868     168.8058265472134     0.7439016193900628
def frag_stats():
    # input:
    #     frag="results/20201123-week-48/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.frags.chr{chrom}.bed.gz",
    #     gc_bias="results/20201130-week-49/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.gc_bias.chr{chrom}.txt",
    #     gc_contents="results/20200914-week-38/gc/gc_content.chr{chrom}.50kb.bed.gz",
    # output:
    #     "results/20201130-week-49/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.frag_stats.{res}.chr{chrom}.bed.gz",
    res = snakemake.wildcards.res
    if res.endswith("mb"):
        res = int(int(res[:-2]) * 1e6)
    elif res.endswith("kb"):
        res = int(int(res[:-2]) * 1e3)
    else:
        logger.error(f"Invalid resolution: {res}")
        raise

    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell(
            "Rscript scripts/20201130-week-49/70.infer.fragment.stats.R "
            "--input {snakemake.input.frag} "
            "--gc-bias {snakemake.input.gc_bias} "
            "--gc-contents {snakemake.input.gc_contents} "
            "--bin-size {res} --gc-bin-size 50000 "
            "--chrom {snakemake.wildcards.chrom} "
            "--output {tempdir}/output.bed.gz "
        )
        
        logger.info("Copying to destination...")
        shell("cp -a {tempdir}/output.bed.gz {snakemake.output}.tmp")
        shell("mv {snakemake.output}.tmp {snakemake.output}")

# Prepare the predictor dataset for the random forest mode
def prepare_predictor_dataset():
    # input:
    #     cm="results/20201123-week-48/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.gm.{metrics}.{res}.seed{seed}.ss{ss}k.chr{chrom}.bed.gz",
    #     frag_stats="results/20201130-week-49/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.{res}.frag_stats.chr{chrom}.bed.gz",
    #     mappability="data/mappability/mappability.{res}.bed",
    # output:
    #     "results/20201130-week-49/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.predictor.{metrics,[^\\.]+}.{res,[^\\.]+}.seed{seed}.ss{ss}k.chr{chrom}.bed.gz",
    res = snakemake.wildcards.res
    if res.endswith("mb"):
        res = int(int(res[:-2]) * 1e6)
    elif res.endswith("kb"):
        res = int(int(res[:-2]) * 1e3)
    else:
        logger.error(f"Invalid resolution: {res}")
        raise

    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell(
            "Rscript scripts/20201130-week-49/69.prepare.gm.dataset.R "
            "--cm {snakemake.input.cm} "
            "--frag-stats {snakemake.input.frag_stats} "
            "--mappability data/mappability/mappability.500kb.bed "
            "--chrom {snakemake.wildcards.chrom} --bin-size {res} "
            "--output {tempdir}/output.bed.gz "
        )
        
        logger.info("Copying to destination...")
        shell("cp -a {tempdir}/output.bed.gz {snakemake.output}.tmp")
        shell("mv {snakemake.output}.tmp {snakemake.output}")

# Make predictions using random forest models
def rf_predict_cm():
    # input:
    #     "results/20201130-week-49/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.predictor.{metrics}.{res}.seed{seed}.ss{ss}k.chr{chrom}.bed.gz",
    # output:
    #     "results/20201130-week-49/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.rf_predicted.{metrics,[^\\.]+}.{res,[^\\.]+}.seed{seed}.ss{ss}k.chr{chrom}.bed.gz",        
    res = snakemake.wildcards.res
    if res.endswith("mb"):
        res = int(int(res[:-2]) * 1e6)
    elif res.endswith("kb"):
        res = int(int(res[:-2]) * 1e3)
    else:
        logger.error(f"Invalid resolution: {res}")
        raise

    h2o_ip = snakemake.config.get("H2O_IP")
    h2o_port = snakemake.config.get("H2O_PORT")

    # Determine the model ID
    # chrG1a: chr1, chr7, chr9-12, chr14, chr15
    # chrG2a: chr2, chr3, chr5-6, chr8
    # chrG3a: chr4, chr13, chr18
    # chrG4a: chr16-17
    # chrG5a: chr19, chr22
    # chrG6a: chr20, chr21
    chrom = snakemake.wildcards.chrom
    if chrom in ["1", "7", "9", "10", "11", "12", "14", "15"]:
        model_id = 1
    elif chrom in ["2", "3", "5", "6", "8"]:
        model_id = 2
    elif chrom in ["4", "13", "18"]:
        model_id = 3
    elif chrom in ["16", "17"]:
        model_id = 4
    elif chrom in ["19", "22"]:
        model_id = 5
    elif chrom in ["20", "21"]:
        model_id = 6
    else:
        logger.error(f"Could not determine the RF model for chr{chrom}")
        raise

    model_id = f"rf_model.v7.SG1a.chrG{model_id}a.1"
    logger.info(f"Making predictions using RF model {model_id} ...")

    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell(
            "Rscript scripts/20201130-week-49/72.rf.prediction.R "
            "--predictor {snakemake.input} "
            "--h2o-ip {h2o_ip} "
            "--h2o-port {h2o_port} "
            "--model {model_id} "
            "--bin-size {res} "
            "--output {tempdir}/output.bed.gz "
        )

        logger.info("Copying to destination...")
        shell("cp -a {tempdir}/output.bed.gz {snakemake.output}.tmp")
        shell("mv {snakemake.output}.tmp {snakemake.output}")
        
if snakemake.rule == "subset_bam_separated":
    subset_bam_separated()
elif snakemake.rule == "gc_bias_separated":
    gc_bias_separated()
elif snakemake.rule == "frag_stats":
    frag_stats()
elif snakemake.rule == "prepare_predictor_dataset":
    prepare_predictor_dataset()
elif snakemake.rule == "rf_predict_cm":
    rf_predict_cm()
else:
    raise RuntimeError(f"Invalid rule: {snakemake.rule}")