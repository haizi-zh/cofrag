__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
import os
import sys

# smk file home location
smk_path = path.normpath(path.dirname(__file__))
sys.path = [smk_path] + sys.path

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell
from snakemake.logging import logger

def infer_fragments():
    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        logger.info("Query-sorting the BAM file ...")
        shell(
            "samtools view -f 3 -F 3852 -q 30 -h {snakemake.input.bam} | "
            "samtools sort -@ {snakemake.threads} -n "
            "-m {snakemake.params.mem_mb_per_thread}M "
            "-T {tempdir} -o {tempdir}/temp.qsorted.bam - "
        )

        logger.info("Calculating fragments ...")
        shell(
            "bamToBed -bedpe -mate1 -i {tempdir}/temp.qsorted.bam | "
            """perl -ne 'chomp;@f=split " ";if($f[0] ne $f[3]){{next;}}$s=$f[1];$e=$f[5];if($f[8] eq "-"){{$s=$f[4];$e=$f[2];}}if($e>$s){{print "$f[0]\\t$s\\t$e\\t$f[7]\\t$f[8]\\t$f[6]\\n";}}' | """
            "sort-bed --max-mem {snakemake.params.mem_mb_sortbed}M --tmpdir {tempdir} - | "
            "bgzip > {tempdir}/temp.bed.gz"
        )

        logger.info("Indexing fragments...")
        shell(
            "tabix -p bed {tempdir}/temp.bed.gz"
        )

        logger.info("Copying to destination...")
        shell(
            "cp -a {tempdir}/temp.bed.gz {snakemake.output.frag}.tmp"
        )
        shell(
            "mv {snakemake.output.frag}.tmp {snakemake.output.frag}"
        )
        shell(
            "mv {tempdir}/temp.bed.gz.tbi {snakemake.output.frag_idx}"
        )

def calc_gm():
    res = snakemake.wildcards.res
    if res.endswith("mb"):
        res = int(int(res[:-2]) * 1e6)
    elif res.endswith("kb"):
        res = int(int(res[:-2]) * 1e3)
    else:
        logger.error(f"Invalid resolution: {res}")
        raise

    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        subsample = int(snakemake.wildcards.ss) * 1000

        logger.info(f"Extracting fragments for chr{snakemake.wildcards.chrom} ...")
        shell("tabix {snakemake.input.frag} {snakemake.wildcards.chrom} | bgzip > {tempdir}/temp.bed.gz")

        shell(
            "Rscript scripts/20201123-week-48/66.calc.contact.matrix.R "
            "--input {tempdir}/temp.bed.gz "
            "--res {res} "
            "--metrics ks "
            "--ncores {snakemake.threads} "
            "--bootstrap 10 "
            "--subsample {subsample} "
            "--seed {snakemake.wildcards.seed} "
            "--output {tempdir}/temp.bed.gz "            
        )

        logger.info("Copying to destination...")
        shell(
            "cp -a {tempdir}/temp.bed.gz {snakemake.output}.tmp"
        )
        shell(
            "mv {snakemake.output}.tmp {snakemake.output}"
        )

if snakemake.rule == "infer_fragments":
    infer_fragments()
elif snakemake.rule == "calc_gm":
    calc_gm()
else:
    raise RuntimeError(f"Invalid rule: {snakemake.rule}")