import os
import math

def calc_time_min(file_path):
    size_gb = math.ceil(os.path.getsize(file_path) / 1024**3 )
    hours = min(round(12 + 0.6 * size_gb), 72)
    return hours * 60

def calc_threads(file_path):
    size_gb = math.ceil(os.path.getsize(file_path) / 1024**3 )
    threads = min(round(4 + 0.1 * size_gb), 14)
    return threads

rule infer_fragments:
    input:
        bam="/pylon5/mc5plcp/dnaase/projects/wgs_pilot_pandisease.20200320/bam/preterm/Pilot_{sid}.hg19.samblaster.bam",
        bai="/pylon5/mc5plcp/dnaase/projects/wgs_pilot_pandisease.20200320/bam/preterm/Pilot_{sid}.hg19.samblaster.bam.bai",
    output:
        frag="results/20201130-week-49/preterm/frag/Pilot_{sid}.hg19.frag.bed.gz",
        frag_idx="results/20201130-week-49/preterm/frag/Pilot_{sid}.hg19.frag.bed.gz.tbi",
    params:
        label=lambda wildcards: f"infer_fragments.{wildcards.sid}",
        mem_mb_per_thread=lambda wildcards, threads, resources: int((resources.mem_mb - 2000) * 0.8 / threads),
        mem_mb_sortbed=lambda wildcards, threads, resources: int((resources.mem_mb - 2000) * 0.8),
        input_size=lambda wildcards, input: f"{round(os.path.getsize(input.bam) / 1024**2)}mb",
    threads: lambda wildcards, input: calc_threads(input.bam)
    resources:
        mem_mb=lambda wildcards, threads: threads * 4200,
        time=4320,
        time_min=lambda wildcards, input: calc_time_min(input.bam),
    script: "71.preterm.pipeline.py"

rule calc_gm:
    input: 
        frag="results/20201130-week-49/preterm/frag/Pilot_{sid}.hg19.frag.bed.gz",
        frag_idx="results/20201130-week-49/preterm/frag/Pilot_{sid}.hg19.frag.bed.gz",
    output: 
        "results/20201130-week-49/preterm/gm/Pilot_{sid}.gm.ks.500kb.seed{seed}.ss{ss}k.chr{chrom}.bed.gz",
    params:
        label=lambda wildcards: f"calc_gm.{wildcards.sid}.chr{wildcards.chrom}",
    threads: config.get("NTHREADS", 4)
    resources:
        time=4320,
        time_min=60,
    script: "71.preterm.pipeline.py"