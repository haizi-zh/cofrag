__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
import os
import sys

# smk file home location
smk_path = path.normpath(path.dirname(__file__))
sys.path = [smk_path] + sys.path

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell
from snakemake.logging import logger

# Check if the job is running in a Kubernetes cluster
def is_kubernetes_mode():
    import os

    return bool(os.getenv("KUBERNETES_SERVICE_HOST"))


def fastqc():
    # input: expand("data/xenograft-20201201/fastq/{{fq_id}}_{rg}.fq.gz", rg=[1, 2])
    # output: expand("results/20201130-week-49/xenograft/fastqc/{{fq_id}}_{rg}_fastqc.{ext}", rg=[1, 2], ext=["html", "zip"])
    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell("fastqc -t {snakemake.threads} -o {tempdir} -f fastq {snakemake.input}")

        destdir = os.path.dirname(snakemake.output[0])
        logger.info(f"Copying to destination: {destdir}")

        for f in os.listdir(tempdir):
            if f.endswith("fastqc.html") or f.endswith("fastqc.zip"):
                logger.info("Copying {f}")
                full_path = path.join(tempdir, f)
                shell("mv {full_path} {destdir}")


def trimmomatic():
    # input:
    #     fastq=lambda wildcards: get_full_fastq(wildcards.sample),
    #     adapter="https://raw.githubusercontent.com/timflutre/trimmomatic/master/adapters/TruSeq3-PE-2.fa",
    # output:
    #     trim=expand("results/20201130-week-49/xenograft/trimmomatic/{{sample}}.R{rg}.{pairing}.fq.gz", rg=[1, 2], pairing=["paired", "unpaired"])
    #     log="results/20201130-week-49/xenograft/trimmomatic/{sample}.trimmomatic.summary.txt",
    logger.info(
        f"Running in Kubernetes mode: {'yes' if is_kubernetes_mode() else 'no'}"
    )

    trimmomatic_args = snakemake.config.get(
        "TRIMMOMATIC",
        (
            f"HEADCROP:6 ILLUMINACLIP:{snakemake.input.adapter}:2:30:10 "
            "CROP:120 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:10 AVGQUAL:20 MINLEN:36 "
        ),
    )
    logger.info("Trimmomatic arguments:")
    logger.info(trimmomatic_args)

    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell(
            "trimmomatic PE -threads {snakemake.threads} "
            "{snakemake.input.fastq[0]} {snakemake.input.fastq[1]} "
            "{tempdir}/R1P.fq.gz {tempdir}/R1U.fq.gz "
            "{tempdir}/R2P.fq.gz {tempdir}/R2U.fq.gz "
            "{trimmomatic_args} "
            "2>&1 | tee {snakemake.output.log}"
        )

        logger.info(f"Copying to destination")
        if is_kubernetes_mode():
            shell("mv {tempdir}/R1P.fq.gz {snakemake.output.trim[0]}")
            shell("mv {tempdir}/R1U.fq.gz {snakemake.output.trim[1]}")
            shell("mv {tempdir}/R2P.fq.gz {snakemake.output.trim[2]}")
            shell("mv {tempdir}/R2U.fq.gz {snakemake.output.trim[3]}")
        else:
            shell("mv {tempdir}/R1P.fq.gz {snakemake.output.trim[0]}.tmp")
            shell("mv {tempdir}/R1U.fq.gz {snakemake.output.trim[1]}.tmp")
            shell("mv {tempdir}/R2P.fq.gz {snakemake.output.trim[2]}.tmp")
            shell("mv {tempdir}/R2U.fq.gz {snakemake.output.trim[3]}.tmp")

            shell("mv {snakemake.output.trim[0]}.tmp {snakemake.output.trim[0]}")
            shell("mv {snakemake.output.trim[1]}.tmp {snakemake.output.trim[1]}")
            shell("mv {snakemake.output.trim[2]}.tmp {snakemake.output.trim[2]}")
            shell("mv {snakemake.output.trim[3]}.tmp {snakemake.output.trim[3]}")


def bwa_raw():
    # input:
    #     fastq=expand("results/20201130-week-49/xenograft/trimmomatic/{{sample}}.R{rg}.paired.fq.gz", rg=[1, 2]),
    #     ref=expand("data/ref_genome/hg19_mm10/human_g1k_v37_mm10_merged.fa{ext}", ext=["", ".amb", ".ann", ".bwt", ".fai", ".pac", "sa"]),
    #     ref_dict="data/ref_genome/hg19_mm10/human_g1k_v37_mm10_merged.dict",
    # output:
    #     bam="results/20201130-week-49/xenograft/temp/{sample}.raw.bam,
    #     log="results/20201130-week-49/xenograft/bam/{sample}.samblaster.log",

    # flowcells = {
    #     "cfEW1": "AK13490-AK27563",
    #     "cfEW2": "AK13590-AK32183",
    #     "cfEW3": "7UDI387-AK32184",
    # }

    lane = 8

    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        # logger.info(f"Using temporary directory: {tempdir}")

        sample = snakemake.wildcards.sample
        # flowcell = flowcells[sample]
        flowcell = "HFC22CCX2"
        rgid = f"{sample}.{flowcell}.L{lane}"
        rgpu = rgid
        rglb = sample

        bwa_threads = max(snakemake.threads - 2, 1)
        logger.info(f"Total cores available: {snakemake.threads}")
        logger.info(f"# of cores for bwa-mem: {bwa_threads}")

        cmd = (
            f"bwa mem -t {bwa_threads} "
            # f"-R @RG\\tID:{rgid} "
            f"-R '@RG\\tID:{rgid}\\tSM:{sample}\\tPL:ILLUMINA\\tLB:{rglb}\\tPU:{rgpu}' "
            f"{snakemake.input.ref[0]} {snakemake.input.fastq} | "
            f"samblaster 2> >(tee {tempdir}/samblaster.log >&2) | "
            f"samtools view -b - > "
            f"{tempdir}/output.bam "
        )
        logger.info(cmd)

        shell(cmd)

        logger.info(f"Copying to destination")
        if is_kubernetes_mode():
            shell("mv {tempdir}/output.bam {snakemake.output.bam}")
        else:
            shell("mv {tempdir}/output.bam {snakemake.output.bam}.tmp")
            shell("mv {snakemake.output.bam}.tmp {snakemake.output.bam}")
        shell("mv {tempdir}/samblaster.log {snakemake.output.log}")


def bam_sort():
    # input:
    #     bam="results/20201130-week-49/xenograft/temp/{sample}.raw.bam",
    # output:
    #     bam="results/20201130-week-49/xenograft/bam/{sample}.mdups.bam",
    #     bai="results/20201130-week-49/xenograft/bam/{sample}.mdups.bam.bai"
    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")

        nthreads = snakemake.threads
        total_mem = snakemake.resources.mem_mb
        mem_per_thread = int((total_mem - 2000) / nthreads)
        logger.info(f"# of cores available: {nthreads}")
        logger.info(f"Total memory available: {total_mem}MB")
        logger.info(f"Memory per thread allocated to samtools sort: {mem_per_thread}MB")

        shell(
            "samtools sort -@ {snakemake.threads} "
            "-m {mem_per_thread}M -o {tempdir}/output.bam {snakemake.input.bam}"
        )
        logger.info("Indexing BAM file")
        shell("samtools index -@ {snakemake.threads} {tempdir}/output.bam")

        logger.info(f"Copying to destination")
        if is_kubernetes_mode():
            shell("mv {tempdir}/output.bam {snakemake.output.bam}")
        else:
            shell("mv {tempdir}/output.bam {snakemake.output.bam}.tmp")
            shell("mv {snakemake.output.bam}.tmp {snakemake.output.bam}")
        shell("mv {tempdir}/output.bam.bai {snakemake.output.bai}")


def infer_fragments():
    # input:
    #     bam="results/20201130-week-49/xenograft/temp/{sample}.raw.bam",
    # output:
    #     frag="results/20201130-week-49/xenograft/frag/{sample}.frag.bed.gz",
    #     frag_idx="results/20201130-week-49/xenograft/frag/{sample}.frag.bed.gz.tbi",
    #     frag_hg19="results/20201130-week-49/xenograft/frag/{sample}.frag.hg19.bed.gz",
    #     frag_idx_hg19="results/20201130-week-49/xenograft/frag-hg19/{sample}.frag.hg19.bed.gz.tbi",
    #     frag_mm10="results/20201130-week-49/xenograft/frag/{sample}.frag.mm10.bed.gz",
    #     frag_idx_mm10="results/20201130-week-49/xenograft/frag/{sample}.frag.mm10.bed.gz.tbi",
    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")

        nthreads = snakemake.threads
        total_mem = snakemake.resources.mem_mb
        mem_sortbed = int((total_mem - 2000) * 0.8)
        logger.info(f"# of cores available: {nthreads}")
        logger.info(f"Total memory available: {total_mem}MB")
        logger.info(f"Memory used by sort-bed: {mem_sortbed}MB")

        logger.info(
            "Infer fragments using: "
            "samtools view -h -f 3 -F 3852 -q 10 | grep -v -e 'XA:Z:' -e 'SA:Z:'"
        )
        shell(
            # "samtools view -h -f 3 -F3852 {snakemake.input.bam} | "
            "samtools view -h -f 3 -F 3852 -q 10 {snakemake.input.bam} | "
            "grep -v -e 'XA:Z:' -e 'SA:Z:' | "
            "bamToBed -bedpe -mate1 -i stdin | "
            """perl -ne 'chomp;@f=split " ";if($f[0] ne $f[3]){{next;}}$s=$f[1];$e=$f[5];if($f[8] eq "-"){{$s=$f[4];$e=$f[2];}}if($e>$s){{print "$f[0]\\t$s\\t$e\\t$f[7]\\t$f[8]\\t$f[6]\\n";}}' | """
            "sort-bed --max-mem {mem_sortbed}M --tmpdir {tempdir} - | "
            "bgzip > {tempdir}/output.bed.gz"
        )

        logger.info("Indexing fragments")
        shell("tabix -p bed {tempdir}/output.bed.gz")

        logger.info("Separating human and mouse fragments")
        for chrom in list(range(1, 23)) + ["X", "Y"]:
            shell(
                "tabix {tempdir}/output.bed.gz {chrom} | "
                "bgzip >> {tempdir}/output.hg19.bed.gz"
            )
        shell("tabix -p bed {tempdir}/output.hg19.bed.gz")

        for chrom in list(range(1, 20)) + ["X", "Y"]:
            shell(
                "tabix {tempdir}/output.bed.gz chr{chrom}_mm10 | "
                "bgzip >> {tempdir}/output.mm10.bed.gz"
            )
        shell("tabix -p bed {tempdir}/output.mm10.bed.gz")

        logger.info(f"Copying to destination")
        if is_kubernetes_mode():
            shell("mv {tempdir}/output.bed.gz {snakemake.output.frag}")
            shell("mv {tempdir}/output.hg19.bed.gz {snakemake.output.frag_hg19}")
            shell("mv {tempdir}/output.mm10.bed.gz {snakemake.output.frag_mm10}")
        else:
            shell("mv {tempdir}/output.bed.gz {snakemake.output.frag}.tmp")
            shell("mv {snakemake.output.frag}.tmp {snakemake.output.frag}")
            shell("mv {tempdir}/output.hg19.bed.gz {snakemake.output.frag_hg19}.tmp")
            shell("mv {snakemake.output.frag_hg19}.tmp {snakemake.output.frag_hg19}")
            shell("mv {tempdir}/output.mm10.bed.gz {snakemake.output.frag_mm10}.tmp")
            shell("mv {snakemake.output.frag_mm10}.tmp {snakemake.output.frag_mm10}")
        shell("mv {tempdir}/output.bed.gz.tbi {snakemake.output.frag_idx}")
        shell("mv {tempdir}/output.hg19.bed.gz.tbi {snakemake.output.frag_idx_hg19}")
        shell("mv {tempdir}/output.mm10.bed.gz.tbi {snakemake.output.frag_idx_mm10}")


def bam_stats():
    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")

        nthreads = snakemake.threads
        total_mem = snakemake.resources.mem_mb
        mem_per_thread = int(total_mem / nthreads)
        logger.info(f"# of cores available: {nthreads}")
        logger.info(f"Total memory available: {total_mem}MB")

        shell("samtools stats {snakemake.input.bam} > {tempdir}/stats.txt")
        shell("samtools idxstats {snakemake.input.bam} > {tempdir}/idxstats.txt")
        shell("samtools flagstats {snakemake.input.bam} > {tempdir}/flagstats.txt")

        logger.info(f"Copying to destination")
        shell("mv {tempdir}/stats.txt {snakemake.output.stats}")
        shell("mv {tempdir}/idxstats.txt {snakemake.output.idxstats}")
        shell("mv {tempdir}/flagstats.txt {snakemake.output.flagstats}")


if snakemake.rule in ["fastqc", "fastqc_post_trim"]:
    fastqc()
elif snakemake.rule == "trimmomatic":
    trimmomatic()
elif snakemake.rule == "bwa_raw":
    bwa_raw()
elif snakemake.rule == "bam_sort":
    bam_sort()
elif snakemake.rule == "infer_fragments":
    infer_fragments()
elif snakemake.rule == "bam_stats":
    bam_stats()
else:
    raise RuntimeError(f"Invalid rule: {snakemake.rule}")