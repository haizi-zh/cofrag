from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
HTTP = HTTPRemoteProvider()

# Generate FastQC reports
rule fastqc:
    input: expand("data/xenograft-20201201/fastq/{{sample}}_{rg}.fq.gz", rg=[1, 2])
    output: expand("results/20201130-week-49/xenograft/fastqc/{{sample}}_{rg}_fastqc.{ext}", rg=[1, 2], ext=["html", "zip"])
    params:
        k8s_node_selector={"diskvol": "8x", "ec2": "4x"},
        label=lambda wildcards: f"fastqc.{wildcards.sample}",
    threads: 3 #lambda wildcards, input: calc_threads(input.bam)
    resources:
        mem_mb=lambda wildcards, threads: threads * 1800,
        time=4320,
        time_min=120
    conda: "conda.fastqc.yml"
    script: "75.xenograft.py"

def get_full_fastq(sample_name):
    prefix = "data/xenograft-20201201/fastq"
    sample_map = {
        "cfEW1": "cfEW1_CKDL200167178-1a-AK13490-AK27563_HFC22CCX2_L8",
        "cfEW2": "cfEW2_CKDL200167178-1a-AK13590-AK32183_HFC22CCX2_L8",
        "cfEW3": "cfEW3_CKDL200167178-1a-7UDI387-AK32184_HFC22CCX2_L8",
        "EW1": "EW1_CKDL200167177-1a-AK7082-AK8483_HCHKHCCX2_L6",
        "EW2": "EW2_CKDL200167177-1a-AK6759-AK9863_HCHKHCCX2_L6",
        "EW3": "EW3_CKDL200167177-1a-AK7085-AK985_HCHKHCCX2_L6",
        "test": "test",
    }
    return [f"{prefix}/{sample_map[sample_name]}_{rg}.fq.gz" for rg in [1, 2]]

# CONFIG: TRIMMOMATIC
rule trimmomatic:
    input: 
        fastq=lambda wildcards: get_full_fastq(wildcards.sample),
        adapter=HTTP.remote("https://raw.githubusercontent.com/timflutre/trimmomatic/master/adapters/TruSeq3-PE-2.fa"),
    output:
        trim=expand("results/20201130-week-49/xenograft/trimmomatic/{{sample}}.R{rg}.{pairing}.fq.gz", rg=[1, 2], pairing=["paired", "unpaired"]),
        log="results/20201130-week-49/xenograft/trimmomatic/{sample}.trimmomatic.summary.txt",
    params:
        k8s_node_selector={"diskvol": "8x", "ec2": "4x"},
        label=lambda wildcards: f"trimmomatic.{wildcards.sample}",
    threads: 2 #lambda wildcards, input: calc_threads(input.bam)
    resources:
        mem_mb=lambda wildcards, threads: threads * 1800,
        time=4320,
        time_min=120
    conda: "conda.trimmomatic.yml"
    script: "75.xenograft.py"

# Generate post-trim FastQC reports
rule fastqc_post_trim:
    input: expand("results/20201130-week-49/xenograft/trimmomatic/{{sample}}.R{rg}.paired.fq.gz", rg=[1, 2])
    output: expand("results/20201130-week-49/xenograft/fastqc-trim/{{sample}}.R{rg}.paired_fastqc.{ext}", rg=[1, 2], ext=["html", "zip"])
    params:
        k8s_node_selector={"diskvol": "8x", "ec2": "4x"},
        label=lambda wildcards: f"fastqc.{wildcards.sample}",
    threads: lambda wildcards, input: min(len(input), 15)
    resources:
        mem_mb=lambda wildcards, threads: threads * 1800,
        time=4320,
        time_min=120
    conda: "conda.fastqc.yml"
    script: "75.xenograft.py"


def get_ref_genome(ref_genome):
    ref_map = {
        "hg19": expand("data/ref_genome/hs37-1kg/human_g1k_v37.fasta.gz{ext}", ext=["", ".amb", ".ann", ".bwt", ".fai", ".gzi", ".pac", ".sa"]),
        "mm10": expand("data/ref_genome/mm10/mm10.fa{ext}", ext=["", ".amb", ".ann", ".bwt", ".fai", ".pac", ".sa"]),
        "hg19_mm10": expand("data/ref_genome/hg19_mm10/human_g1k_v37_mm10_merged.fa{ext}", ext=["", ".amb", ".ann", ".bwt", ".fai", ".pac", ".sa"]),
    }
    return ref_map[ref_genome]


# Align raw reads using bwa-mem
rule bwa_raw:
    input: 
        ref=lambda wildcards: get_ref_genome(wildcards.ref_genome),
        fastq=expand("results/20201130-week-49/xenograft/trimmomatic/{{sample}}.R{rg}.paired.fq.gz", rg=[1, 2]),
    output: 
        bam="results/20201130-week-49/xenograft/temp/{sample,[^\\.]+}.{ref_genome,[^\\.]+}.raw.bam",
        log="results/20201130-week-49/xenograft/bam/{sample}.{ref_genome}.samblaster.log",
    params:
        k8s_node_selector={"diskvol": "8x", "ec2": "4x"},
        label=lambda wildcards: f"bwa_raw.{wildcards.ref_genome}.{wildcards.sample}",
    threads: 15 #lambda wildcards, input: calc_threads(input.bam)
    resources:
        mem_mb=lambda wildcards, threads: threads * 1800,
        time=4320,
        time_min=120
    conda: "conda.bwa.yml"
    script: "75.xenograft.py"

# Sort the BAM file with duplicates marked
rule bam_sort:
    input: 
        bam="results/20201130-week-49/xenograft/temp/{sample}.{ref_genome}.raw.bam",
    output:
        bam="results/20201130-week-49/xenograft/bam/{sample,[^\\.]+}.{ref_genome,[^\\.]+}.mdups.bam",
        bai="results/20201130-week-49/xenograft/bam/{sample}.{ref_genome}.mdups.bam.bai",
    params:
        k8s_node_selector={"diskvol": "8x", "ec2": "4x"},
        label=lambda wildcards: f"bam_sort.{wildcards.ref_genome}.{wildcards.sample}",
    threads: 5
    resources:
        mem_mb=lambda wildcards, threads: threads * 1800,
        time=4320,
        time_min=120
    conda: "conda.bwa.yml"
    script: "75.xenograft.py"

rule infer_fragments:
    input:
        bam="results/20201130-week-49/xenograft/temp/{sample}.{ref_genome}.raw.bam",
    output:
        frag="results/20201130-week-49/xenograft/frag/{sample,[^\\.]+}.{ref_genome,[^\\.]+}.frag.bed.gz",
        frag_idx="results/20201130-week-49/xenograft/frag/{sample}.{ref_genome}.frag.bed.gz.tbi",
        frag_hg19="results/20201130-week-49/xenograft/frag/{sample}.{ref_genome}.human.frag.bed.gz",
        frag_idx_hg19="results/20201130-week-49/xenograft/frag/{sample}.{ref_genome}.human.frag.bed.gz.tbi",
        frag_mm10="results/20201130-week-49/xenograft/frag/{sample}.{ref_genome}.mouse.frag.bed.gz",
        frag_idx_mm10="results/20201130-week-49/xenograft/frag/{sample}.{ref_genome}.mouse.frag.bed.gz.tbi",
    params:
        k8s_node_selector={"diskvol": "8x", "ec2": "4x"},
        label=lambda wildcards: f"infer_fragments.{wildcards.ref_genome}.{wildcards.sample}",
    threads: 5 
    resources:
        mem_mb=lambda wildcards, threads: threads * 1800,
        time=4320,
        time_min=240,
    conda: "conda.infer_fragments.yml"
    script: "75.xenograft.py"


rule bam_stats:
    input:
        bam="results/20201130-week-49/xenograft/bam/{sample}.{ref_genome}.mdups.bam",
        bai="results/20201130-week-49/xenograft/bam/{sample}.{ref_genome}.mdups.bam.bai",
    output:
        stats="results/20201130-week-49/xenograft/bam-stats/{sample,[^\\.]+}.{ref_genome,[^\\.]+}.stats.txt",
        idxstats="results/20201130-week-49/xenograft/bam-stats/{sample}.{ref_genome}.idxstats.txt",
        flagstats="results/20201130-week-49/xenograft/bam-stats/{sample}.{ref_genome}.flagstats.txt",
    params:
        k8s_node_selector={"diskvol": "8x", "ec2": "4x"},
        label=lambda wildcards: f"bam_stats.{wildcards.ref_genome}.{wildcards.sample}",
    threads: 1 
    resources:
        mem_mb=lambda wildcards, threads: threads * 1800,
        time=4320,
        time_min=240,
    conda: "conda.bwa.yml"
    script: "75.xenograft.py"