# MIT License
#
# Copyright (c) 2020 Haizi Zheng
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Author: Haizi Zheng
# Copyright: Copyright 2020, Haizi Zheng
# Email: haizi.zh@gmail.com
# License: MIT
# Date: Dec 1, 2020
#
# This script evaluates error rates

## CLI ----

suppressMessages({
  library(here)
  library(tidyverse)
  library(magrittr)
})

## Script ----

### Body ----

results <-
  list(c("M11189W", "H11189W"), c("M12148W", "H12148W")) %>% walk(function(samples) {
    prefix <-"results/20201130-week-49/trio-variant-calling/mask-analysis-0.05"
    # c(1:22, "X") %>% map_dfr(function(chrom) {
    c("21") %>% map_dfr(function(chrom) {
      source("scripts/20201123-week-48/57.vcf.utils.R", local = TRUE)
      args <-
        list(
          masked = str_interp("${prefix}/masked.chr${chrom}.vcf.gz"),
          imputed = str_interp("${prefix}/mis/data/chr${chrom}.dose.vcf.gz"),
          samples = samples
        )
      
      logging::loginfo(str_interp("Argument summary:"))
      logging::loginfo(str_interp("--masked: ${args$masked}"))
      logging::loginfo(str_interp("--imputed: ${args$imputed}"))
      logging::loginfo(str_interp("--imputed: ${args$samples}"))
      
      tempdir_masked <- paste0(tempdir(), "/", digest::sha1(runif(1)))
      dir.create(tempdir_masked)
      
      system(
        str_interp(
          "bcftools isec ${args$imputed} ${args$masked} -p ${tempdir_masked} -n =2"
        )
      )
      
      vcf_imputed <-
        vcf2df(vcfR::read.vcfR(paste0(tempdir_masked, "/0000.vcf")), fields = c("GT", "GP")) %>%
        calc_dgp() %>%
        identify_critical_sites(sample_1 = args$samples[1],
                                sample_2 = args$samples[2])
      
      vcf_masked <-
        vcf2df(vcfR::read.vcfR(paste0(tempdir_masked, "/0001.vcf")), fields = c("GT")) %>%
        identify_critical_sites(
          sample_1 = args$samples[1],
          sample_2 = args$samples[2],
          # Keep all sites, including the non-critical ones
          na.rm = FALSE
        )
      
      left_join(
        vcf_masked %>% select(CHROM, POS, REF, ALT, delta),
        vcf_imputed %>% select(CHROM, POS, REF, ALT, delta, ends_with(".DGP")),
        by = c("CHROM", "POS", "REF", "ALT"),
        suffix = c(".masked", ".imputed")
      )
    }) %>%
      write_tsv(str_interp("${prefix}/results.${samples[1]}.${samples[2]}.tsv.gz"))
  })
