from os.path import splitext, dirname

OUTPUT_DIR = "results/20200406-week-15/bam_filter"
CORES = 8
MEM_REQ = CORES * 1000
MIN_MAPQ = 30

envvars:
    "AWS_ACCESS_KEY_ID",
    "AWS_SECRET_ACCESS_KEY"

rule foo:
    shell:
        """
        pwd
        find .
        Rscript src/bam_filter.R
        """

# rule samtools_filter:
#     input:    "data/bh01/bh01.chr{chr_name}.sorted.bam"
#     output:
#         bam="%s/bh01.chr{chr_name}.filtered.bam" % OUTPUT_DIR,
#         bai="%s/bh01.chr{chr_name}.filtered.bam.bai" % OUTPUT_DIR
#     conda:  "conda_env.yaml"
#     threads:    CORES
#     resources:
#         mem_mb=MEM_REQ
#     shell:
#         """
#         bam_flag83="bh01.chr{wildcards.chr_name}.filtered.flag_83.bam"
#         bam_flag99="bh01.chr{wildcards.chr_name}.filtered.flag_99.bam"
#         samtools view -b -@ {threads} -q {MIN_MAPQ} -f 83 {input} > "$bam_flag83"
#         samtools view -b -@ {threads} -q {MIN_MAPQ} -f 99 {input} > "$bam_flag99"
#         samtools merge -@ {threads} {output.bam} "$bam_flag83" "$bam_flag99"
#         samtools index -@ {threads} {output.bam} {output.bai}
#         rm -rf "$bam_flag83" "$bam_flag99"
#         """
#     
# rule samtools_merge:
#     input:
#         "works/samtools_filter/bh01.chr{chr_name}.filtered.flag_83.bam",
#         "works/samtools_filter/bh01.chr{chr_name}.filtered.flag_99.bam"
#     output:
#         bam="works/samtools_filter/bh01.chr{chr_name}.filtered.bam",
#         bai="works/samtools_filter/bh01.chr{chr_name}.filtered.bam.bai"
#     conda:    "conda_env.yaml"
#     threads:    config["threads"]
#     shell:
#         """
#         samtools merge -@ {threads} {output.bam} {input}
#         samtools index -@ {threads} {output.bam} {output.bai}
#         """
