Filter the BAM files based on the following criteria:

- Flag: 83 or 99
- MAPQ >= 30
- Mappability >= 0.75
