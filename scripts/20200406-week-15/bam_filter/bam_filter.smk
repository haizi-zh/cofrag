from os.path import splitext, dirname

OUTPUT_DIR = "results/20200406-week-15/bam_filter"
CORES = 5
MIN_MAPQ = 30
MIN_MAPS = 0.75

envvars:
    "AWS_ACCESS_KEY_ID",
    "AWS_SECRET_ACCESS_KEY"

rule samtools_filter:
    input:    "data/bh01/bh01.chr{chr_name}.sorted.bam"
    output:
        bam=temp("%s/bh01.chr{chr_name}.samtools.filtered.bam" % OUTPUT_DIR),
        bai=temp("%s/bh01.chr{chr_name}.samtools.filtered.bam.bai" % OUTPUT_DIR)
    conda:  "conda_env.yaml"
    threads:    CORES
    resources:
        mem_mb=lambda wildcards, attempt, threads: attempt * threads * 500
    log:
        "%s/bh01.chr{chr_name}.samtools.filtered.bam.log" % OUTPUT_DIR
    shell:
        """
        bam_flag83="bh01.chr{wildcards.chr_name}.samtools.filtered.flag_83.bam"
        bam_flag99="bh01.chr{wildcards.chr_name}.samtools.filtered.flag_99.bam"
        samtools view -b -@ {threads} -q {MIN_MAPQ} -f 83 {input} > "$bam_flag83"
        samtools view -b -@ {threads} -q {MIN_MAPQ} -f 99 {input} > "$bam_flag99"
        samtools merge -@ {threads} {output.bam} "$bam_flag83" "$bam_flag99"
        samtools index -@ {threads} {output.bam} {output.bai}
        rm -rf "$bam_flag83" "$bam_flag99"
        """

rule gem_filter:
    input:  
        bam="%s/bh01.chr{chr_name}.samtools.filtered.bam" % OUTPUT_DIR,
        bai="%s/bh01.chr{chr_name}.samtools.filtered.bam.bai" % OUTPUT_DIR,
        maptrack="results/20200330-week-14/mappability/hs37d5.chr{chr_name}.45mer.bw"
    output:
        bam="%s/bh01.chr{chr_name}.filtered.bam" % OUTPUT_DIR,
        bai="%s/bh01.chr{chr_name}.filtered.bam.bai" % OUTPUT_DIR
    conda:  "conda_env.yaml"
    threads:    CORES
    resources:
        mem_mb=lambda wildcards, attempt, threads: attempt * threads * 500
    log:
        "%s/bh01.chr{chr_name}.filtered.bam.log" % OUTPUT_DIR
    shell:
        """
        Rscript --vanilla src/bam_filter.R --mapq {MIN_MAPQ} --maps {MIN_MAPS} --map-track {input.maptrack} -n {threads} -o {output.bam} {input.bam}
        samtools index -@ {threads} {output.bam} {output.bai}
        """
