Setup the data analysis pipeline based on snakemake and kubernetes

Example:

snakemake --verbose -d /Users/haizi/CCHMC/cofrag --kubernetes --container-image zephyre/comp-bio:v0.3.1 --default-remote-provider S3 --default-remote-prefix cofrag.epifluidlab.cchmc.org -j 1 -s mappability.smk -p --force cofrag.epifluidlab.cchmc.org/results/20200406-week-15/pipeline/sacCer3/sacCer3.fa
