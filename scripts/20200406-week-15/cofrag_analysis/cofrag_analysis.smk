from os.path import splitext, dirname

BAM_FILTER_DIR = "results/20200406-week-15/bam_filter"
COFRAG_DIR = "results/20200406-week-15/cofrag_analysis"
MIN_MAPQ = 30
MIN_MAPS = 0.75

envvars:
    "AWS_ACCESS_KEY_ID",
    "AWS_SECRET_ACCESS_KEY"

rule samtools_filter:
    input:    "data/bh01/bh01.chr{chr_name}.sorted.bam"
    output:
        bam=temp("%s/bh01.chr{chr_name}.samtools.filtered.bam" % BAM_FILTER_DIR),
        bai=temp("%s/bh01.chr{chr_name}.samtools.filtered.bam.bai" % BAM_FILTER_DIR)
    conda:  "conda_env.yaml"
    threads:    5
    resources:
        mem_mb=lambda wildcards, attempt, threads: attempt * threads * 500
    shell:
        """
        bam_flag83="bh01.chr{wildcards.chr_name}.samtools.filtered.flag_83.bam"
        bam_flag99="bh01.chr{wildcards.chr_name}.samtools.filtered.flag_99.bam"
        samtools view -b -@ {threads} -q {MIN_MAPQ} -f 83 {input} > "$bam_flag83"
        samtools view -b -@ {threads} -q {MIN_MAPQ} -f 99 {input} > "$bam_flag99"
        samtools merge -@ {threads} {output.bam} "$bam_flag83" "$bam_flag99"
        samtools index -@ {threads} {output.bam} {output.bai}
        rm -rf "$bam_flag83" "$bam_flag99"
        """

rule gem_filter:
    input:  
        bam="%s/bh01.chr{chr_name}.samtools.filtered.bam" % BAM_FILTER_DIR,
        bai="%s/bh01.chr{chr_name}.samtools.filtered.bam.bai" % BAM_FILTER_DIR,
        maptrack="results/20200330-week-14/mappability/hs37d5.chr{chr_name}.45mer.bw"
    output:
        bam="%s/bh01.chr{chr_name}.filtered.bam" % BAM_FILTER_DIR,
        bai="%s/bh01.chr{chr_name}.filtered.bam.bai" % BAM_FILTER_DIR
    conda:  "conda_env.yaml"
    threads:    5
    resources:
        mem_mb=lambda wildcards, attempt, threads: attempt * threads * 500
    shell:
        """
        Rscript --vanilla src/bam_filter.R --mapq {MIN_MAPQ} --maps {MIN_MAPS} --map-track {input.maptrack} -n {threads} -o {output.bam} {input.bam}
        samtools index -@ {threads} {output.bam} {output.bai}
        """

rule calc_distance:
    input:
        bam="%s/bh01.chr{chr_name}.filtered.bam" % BAM_FILTER_DIR,
        bai="%s/bh01.chr{chr_name}.filtered.bam.bai" % BAM_FILTER_DIR
    output:
        "%s/bh01.chr{chr_name}.{bin_size}kb.{metric}.distance.txt" % COFRAG_DIR
    threads:    15
    resources:
        mem_mb=lambda wildcards, attempt, threads: attempt * threads * 1000
    log:
        "%s/bh01.chr{chr_name}.{bin_size}kb.{metric}.distance.log" % COFRAG_DIR
    conda:  "conda_env.yaml"
    shell:
        """
        set +o pipefail
        start_pos=$(samtools view {input.bam} | head -n 1 | cut -f4)
        end_pos=$(samtools view {input.bam} | tail -n 1 | cut -f4)
        set -o pipefail

        fi=$(readlink -f {input.bam})
        fo=$(readlink -f {output})
        fl=$(readlink -f {log})
        cd src/
        Rscript --vanilla cfhic.R distance --range {wildcards.chr_name}:"$start_pos"-"$end_pos" -s $(({wildcards.bin_size}*1000)) -b 1000000 -m {wildcards.metric} -o "$fo" --max-frag-size 1000 --min-samples 1000 -n {threads} "$fi" 2>&1 | tee "$fl"
        """
        
