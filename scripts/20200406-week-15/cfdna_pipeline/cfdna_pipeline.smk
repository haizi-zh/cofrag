
envvars:
    "AWS_ACCESS_KEY_ID",
    "AWS_SECRET_ACCESS_KEY"


# from os.path import splitext
# 
# REF_DIR = "data/ref_genome"
# OUTPUT_DIR = "results/20200406-week-15/pipeline"
# CORES = 8
# MEM_REQ = CORES * 1000
# 
# envvars:
#     "AWS_ACCESS_KEY_ID",
#     "AWS_SECRET_ACCESS_KEY"
# 
# rule unpack_fa:
#     input:    "%s/{ref_name}/{ref_name}.fa.gz" % REF_DIR
#     output:   "%s/{ref_name}/{ref_name}.fa" % REF_DIR
#     shell:
#         """
#         set +e
#         gunzip -k -c {input} > {output}
#         exitcode=$?
#         if [ $exitcode -eq 2 ]
#         then
#                 exit 0
#         fi
#         """
# 
# rule postproc_fa:
#     input:
#         "%s/{ref_name}/{ref_name}.fa" % REF_DIR
#     output:
#         temp("%s/mappability/{ref_name}.postproc.fa" % OUTPUT_DIR)
#     shell:
#         "sed 's/^\\(>[^ ]\\+\\).*\\?/\\1/g' {input} > {output}"
# 
# rule gem_indexer:
#     input:
#         temp("%s/mappability/{ref_name}.postproc.fa" % OUTPUT_DIR)
#     output:
#         "%s/mappability/{ref_name}.gem" % OUTPUT_DIR
#     threads: CORES
#     resources:
#         mem_mb=MEM_REQ
#     run:
#         output_base = splitext(str(output))[0]
#         shell("gem-indexer -T {threads} -c dna -i {input} -o {output_base}")
# 
# rule gem_mappability:
#     input:
#         "%s/mappability/{ref_name}.gem" % OUTPUT_DIR
#     output:
#         protected("%s/mappability/{ref_name}.{kmer}mer.mappability" % OUTPUT_DIR)
#     threads: CORES
#     resources:
#         mem_mb=MEM_REQ
#     run:
#         output_base = splitext(str(output))[0]
#         shell("gem-mappability -T {threads} -I {input} -l {wildcards.kmer} -o {output_base}")
# 
# rule gem_wig:
#     input:
#         idx="%s/mappability/{ref_name}.gem" % OUTPUT_DIR,
#         mapb="%s/mappability/{ref_name}.{kmer}mer.mappability" % OUTPUT_DIR
#     output:
#         wig="%s/mappability/{ref_name}.{kmer}mer.wig" % OUTPUT_DIR,
#         sizes="%s/mappability/{ref_name}.{kmer}mer.sizes" % OUTPUT_DIR
#     threads: 1
#     resources:
#         mem_mb=2000
#     run:
#         output_base = splitext(str(output.wig))[0]
#         shell("gem-2-wig -I {input.idx} -i {input.mapb} -o {output_base}")
# 
# rule wig_bw:
#     input:
#         "%s/mappability/{ref_name}.{kmer}mer.wig" % OUTPUT_DIR,
#         "%s/mappability/{ref_name}.{kmer}mer.sizes" % OUTPUT_DIR
#     output:
#         "%s/mappability/{ref_name}.{kmer}mer.bw" % OUTPUT_DIR
#     conda:  "conda_env.yaml"
#     threads: 1
#     resources:
#         mem_mb=2000
#     shell:
#         "wigToBigWig {input} {output}"
# 
# rule wig2begdgraph:
#     input:
#         "%s/mappability/{ref_name}.{kmer}mer.wig" % OUTPUT_DIR
#     output:
#         "%s/mappability/{ref_name}.{kmer}mer.bedgraph" % OUTPUT_DIR
#     conda:  "conda_env.yaml"
#     threads: 1
#     resources:
#         mem_mb=2000
#     shell:
#         "wig2bed < {input} | cut -f1-3,5 > {output}"
# 
# rule split_bedgraph:
#     input:
#         "%s/mappability/{ref_name}.{kmer}mer.bedgraph" % OUTPUT_DIR
#     output:
#         temp("%s/mappability/{ref_name}.{chr}.{kmer}mer.bedgraph" % OUTPUT_DIR)
#     conda:  "conda_env.yaml"
#     shell:
#         "cat {input} | sed -n '/^{wildcards.chr}/p' > {output}"
# 
# # rule split_chr_bw:
# #         input:
# #                 "works/mappability/{ref_name}.{chr}.{kmer}mer.bedgraph",
# #                 "works/mappability/{ref_name}.{kmer}mer.sizes"
# #         output: "works/mappability/{ref_name}.{chr}.{kmer}mer.bw"
# #         conda:  "conda_env.yaml"
# #         shell:
# #                 "bedGraphToBigWig {input} {output}"
