library(tidyverse)
library(logging)
library(magrittr)
library(logging)

setwd("./dev/")
library(here)
setwd("..")

source(here("src/contact_matrix.R"))
# load(here("../lab_notebook/journal-20200912-week-37.RData"))

args <- commandArgs(trailingOnly = TRUE)
entry_name <- args[1]
chr_name_short <- args[2]
bin_size <- as.integer(args[3])
frag_file <- args[4]
output <- args[5]

# entry_name <- "EE88155"
# chr_name_short <- "14"
# bin_size <- 500e3L
chr_name <- paste0("chr", chr_name_short)

frags <- read_tsv(
  gzfile(frag_file),
  col_names = c("chr", "start", "end"),
  col_types = cols(col_factor(), col_integer(), col_integer())
) %>%
  mutate(length = end - start)

bfp <- calc_bfp(frags,
                infer_genomic_range(frags, bin_size),
                bin_size = bin_size) %>%
  mutate(cov_raw = frag %>% map_int( ~ {
    length(.[[1]])
  })) %>%
  select(-frag)

load(here(str_interp(
  "../results/20200907-week-37/gc_contents.${chr_name}.RData")
))

gc_bias_file <-
  str_interp(
    here(
      "../results/20200907-week-37/${entry_name}/${entry_name}.hg19.gc_bias.txt"
    )
  )

gc_bias <- read_tsv(gc_bias_file,
                    skip = 6) %>%
  transmute(
    gc = GC,
    coverage = NORMALIZED_COVERAGE,
    error = ERROR_BAR_WIDTH,
    entry = entry_name
  ) %>% select(entry, everything())

gc_approx_fun <- approxfun(x = gc_bias$gc, y = gc_bias$coverage)

corrected <- left_join(
  bfp %>% rename(start = bin_start) %>% mutate(chr = chr_name_short, end = start + bin_size),
  gc_content,
  by = c("chr", "start", "end")
) %>%
  select(chr, start, end, cov_raw, avg.gc) %>%
  mutate(
    normalized_cov = gc_approx_fun(avg.gc),
    cov_score_corr = (cov_raw / normalized_cov) / sum(cov_raw / normalized_cov, na.rm = TRUE) / (1 / nrow(bfp)),
    cov_score_raw = cov_raw / sum(cov_raw) / (1 / nrow(bfp))
  )

save(corrected, file = output)# str_interp("./results/20200907-week-37/${entry_name}/{$entry_name}.gc_corrected_cov.${chr_name}.RData"))
