rule get_mappability:
    input: "data/mappability/wgEncodeDukeMapabilityUniqueness35bp.bigWig"
    output: "data/mappability/wgEncodeDukeMapabilityUniqueness35bp.{chr,chr.+}.lt0_5.bed.gz"
    shell:
        """
        bigWigToBedGraph -chrom={wildcards.chr} {input} /dev/stdout |
            awk '$4<=0.5 {{print substr($0, 4)}}' |
            bgzip > {output}
        """

rule get_frag:
    singularity: "docker://zephyre/comp-bio:v0.3.7"
    input: 
        frag="/pylon5/mc5plcp/haizizh/finaledb_data/frag/{entry_id}.hg19.frag.gz",
        mappability="data/mappability/wgEncodeDukeMapabilityUniqueness35bp.chr{chr}.lt0_5.bed.gz"
    output:
        frag="results/{results_subdir}/{entry_id,EE[0-9]+}/{entry_id}.hg19.chr{chr,.+}.filtered.frag.len100_350.tsv.gz",
        index="results/{results_subdir}/{entry_id,EE[0-9]+}/{entry_id}.hg19.chr{chr,.+}.filtered.frag.len100_350.tsv.gz.tbi",
    threads: 1
    resources:
        cpus=lambda wildcards, threads: threads,
        mem_mb=lambda wildcards, threads: threads * 4200,
        time_min=90
    params:
        label=lambda wildcards: f"{wildcards.entry_id}.chr{wildcards.chr}",
        partition="RM-shared"
    shell:
        """
        LOCAL=/local
        
        echo `date +"%F %T %Z"` "Extracting fragments..."
        tabix {input.frag} {wildcards.chr} |
            awk '$4>=30 && $3-$2<=350 && $3-$2>=100' | 
            bedtools subtract -A -a - -b data/duke_excluded_regions/wgEncodeDacMapabilityConsensusExcludable.chr{wildcards.chr}.bed |
            bedtools subtract -A -a - -b {input.mappability} -f 0.1 |
            bgzip > $LOCAL/frag.gz
        echo `date +"%F %T %Z"` "Indexing fragments..."
        tabix -0 -p bed $LOCAL/frag.gz
        
        echo `date +"%F %T %Z"` "Copying results to target directory..."
        cp -a $LOCAL/frag.gz {output.frag}.tmp
        cp -a $LOCAL/frag.gz.tbi {output.index}.tmp
        mv {output.frag}.tmp {output.frag}
        mv {output.index}.tmp {output.index}
        """ 

rule calc_cm:
    input: 
        "results/{results_subdir}/{entry_id}/{entry_id}.hg19.chr{chr}.filtered.frag.len100_350.tsv.gz"
    output: 
        "results/{results_subdir}/{entry_id,EE[0-9]+}/{entry_id}.cm.{metrics}.ss{ss,[0-9]+}k.chr{chr,.+}.seed{seed}.bed.gz"
    threads: 28
    resources:
        cpus=lambda wildcards, threads: threads,
        mem_mb=lambda wildcards, threads: threads * 4200,
        time_min=90
    params:
        label=lambda wildcards: f"{wildcards.entry_id}.chr{wildcards.chr}.ss{wildcards.ss}k",
        partition="RM"
    shell:
        """
        R_LIBS=/home/haizizh/Rpackages/4.0/ /opt/packages/R/4.0.0-mkl/bin/Rscript --vanilla \
            scripts/20200907-week-37/01.calc.contact_matrix.R {wildcards.ss} {wildcards.metrics} 14 5 {wildcards.seed} {input} {output}.tmp

        mv {output}.tmp {output}
        """