library(tidyverse)
library(logging)
library(magrittr)
library(logging)

load("./lab_notebook/journal-20200912-week-37.RData")

# args <- commandArgs(trailingOnly = TRUE)
# chr_name_short <- args[1]
chr_name_short <- 21
chr_name <- paste0("chr", chr_name_short)

# bin_layout <- env_base$bin_layout %>% 
#   pmap_dfr(function(chr, start, end, ...) { 
#     start <- seq.int(start, end - 1, 100e3L)
#     end <- start + 100e3L
#     chr <- rep_along(start, chr)
#     list(chr = chr, start = as.integer(start), end = as.integer(end))
#   })

bin_layout <- env_base$bin_layout

loginfo(str_interp("Analyzing chr${chr_name_short}"))
gc_content <- seq(0, 250e6L - 1, by = 25e6L) %>%
  as.integer() %>%
  map_dfr(~ {
    region_start <- .
    
    region <-
      str_interp("${chr_name}:$[d]{region_start+1}-$[d]{region_start+25e6L}")
    loginfo(str_interp("Region: ${region}"))
    loginfo("Loading GC content dataset...")
    
    gc5bp <- bedr::tabix(
      region = region,
      check.chr = FALSE,
      check.valid = FALSE,
      check.merge = FALSE,
      check.sort = FALSE,
      check.zero.based = FALSE,
      file.name = "./data/gc/hg19.gc5bp.bed.gz"
    )
    if (is.null(gc5bp))
      return(NULL)
    
    gc5bp %<>%
      as_tibble() %>%
      set_colnames(c("chr", "start", "end", "gc")) %>%
      mutate(
        chr = str_sub(chr, 4),
        start = as.integer(start),
        end = as.integer(end),
        gc = as.double(gc)
      )
    
    loginfo("Perform bedtools intersect...")
    gc_content <- bedr::bedr(
      input = list(
        a = gc5bp,
        b = bin_layout %>%
          filter(
            chr == chr_name_short &
              start >= region_start & end <= region_start + 25e6L
          ) %>%
          select(chr, start, end) %>%
          mutate(start = as.integer(start), end = as.integer(end))
      ),
      method = "intersect",
      params = "-loj -sorted",
      check.merge = FALSE,
      check.chr = FALSE,
      check.valid = FALSE,
      check.sort = FALSE,
      check.zero.based = FALSE
    ) %>%
      as_tibble() %>%
      mutate(
        start.b = as.integer(start.b),
        end.b = as.integer(end.b),
        gc = as.double(gc)
      ) %>%
      rename(
        start.a = start,
        end.a = end,
        start = start.b,
        end = end.b
      ) %>%
      select(chr, start, end, gc, start.a, end.a) %>%
      arrange(start) %>%
      group_by(chr, start, end) %>%
      summarize(.groups = "drop",
                avg.gc = mean(gc))
    
    gc_content
  })

# save(gc_content, file = str_interp("./results/20200907-week-37/gc_contents.${chr_name}.RData"))