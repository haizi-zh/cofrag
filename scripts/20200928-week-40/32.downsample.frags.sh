#!/usr/bin/bash

# This script generate downsampled fragment datasets

echo "Downsample $1 ..."
frag_cnt=`zcat $1 | wc -l`

for rate in 0.01 0.1 1 5 10 50 100
do
    target_cnt=`python -c "print(int($frag_cnt / 100 * $rate))"`
    echo -e "$rate%\toriginal\t$frag_cnt\tdownsampled\t$target_cnt"

    zcat $1 | sort --
done