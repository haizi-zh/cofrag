import os
import math

def eprint(*args, **kwargs):
    import sys

    sys.stderr.write('\x1b[33m')
    print(*args, file=sys.stderr, **kwargs)
    sys.stderr.write('\x1b[0m')
 

def format_time_str(total_minutes):
    hour = total_minutes / 60
    minutes = total_minutes % 60
    return "%02d:%02d:00" % (hour, minutes)

def get_file_size(file_path):
    import os

    return os.path.getsize(file_path)


def nlogn(size_bytes):
    import math

    size_gb = max(size_bytes / 1024**3, 1)
    return (math.log10(size_gb) + 1) * size_gb


rule calc_cm_downsample:
    input:
        "results/20200914-week-38/training/frags/{entry_id}.hg19.chr{chr}.filtered.frag.len100_350.tsv.gz"
    output:
        "results/20200928-week-40/cm.downsample/{entry_id,EE[0-9]+}.cm.{metrics}.ds{ds_rate}.ss{ss,[0-9]+}k.chr{chr,.+}.seed{seed}.bed.gz"
        # expand("results/20200928-week-40/cm.downsample/{{entry_id,EE[0-9]+}}.cm.{{metrics}}.ds{ds_rate}.ss{{ss,[0-9]+}}k.chr{{chr,.+}}.seed{{seed}}.bed.gz", ds_rate=[0.001, 0.01, 0.05, 0.1, 0.5])
    threads: 18
    resources:
        cpus=lambda wildcards, threads: threads,
        mem_mb=lambda wildcards, threads: threads * 4200,
        time_min=360
    params:
        label=lambda wildcards: f"{wildcards.entry_id}.chr{wildcards.chr}",
        partition="RM"
    shell:
        """
        echo `date +"%F %T %Z"` "Job started: {wildcards.entry_id}"


        R_LIBS=/home/haizizh/Rpackages/4.0/ /opt/packages/R/4.0.0-mkl/bin/Rscript --vanilla \
            scripts/20200928-week-40/33.calc.contact_matrix.downsample.R 20 ks {resources.cpus} 5 11 {input} {wildcards.ds_rate}
        """
