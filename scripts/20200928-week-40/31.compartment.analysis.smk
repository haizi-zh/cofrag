import os
import math

def eprint(*args, **kwargs):
    import sys

    sys.stderr.write('\x1b[33m')
    print(*args, file=sys.stderr, **kwargs)
    sys.stderr.write('\x1b[0m')
 

def format_time_str(total_minutes):
    hour = total_minutes / 60
    minutes = total_minutes % 60
    return "%02d:%02d:00" % (hour, minutes)

def get_file_size(file_path):
    import os

    return os.path.getsize(file_path)


def nlogn(size_bytes):
    import math

    size_gb = max(size_bytes / 1024**3, 1)
    return (math.log10(size_gb) + 1) * size_gb


rule call_compartments:
    # singularity: "docker://zephyre/comp-bio:v0.3.7"
    input:
        lambda wildcards: [f"results/20200914-week-38/training/cm/{wildcards.sample_id}.cm.ks.ss20k.chr{chr}.seed11.bed.gz" for chr in range(1, 23)]
    output:
        "stash/cristiano/{sample_id}.compartment.bed"
    threads: 1
    resources:
        cpus=lambda wildcards, threads: threads,
        mem_mb=lambda wildcards, threads: threads * 4200,
        time_min=60
    params:
        mem_mb_per_thread=lambda wildcards, threads, resources: int((resources.mem_mb - 1000) * 0.8 / threads),
        mem_mb_sortbed=lambda wildcards, threads, resources: int((resources.mem_mb - 2000) * 0.4),
        sort_threads=lambda wildcards, threads, resources: resources.cpus - 1,
        label=lambda wildcards: f"{wildcards.sample_id}",
        partition="RM-shared"
    shell:
        """
        echo `date +"%F %T %Z"` "Job started: {wildcards.sample_id}"

        R_LIBS=/home/haizizh/Rpackages/4.0/ /opt/packages/R/4.0.0-mkl/bin/Rscript --vanilla \
            scripts/20200928-week-40/30.compartment.analysis.R {wildcards.sample_id}
        """
