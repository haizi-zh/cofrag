# chrG1: chr2-chr6, chr8, chr13, chr18
# chrG2: chr1, chr7, chr9-12, chr14, chr15, chr20, chr21
# chrG3: chr16, chr17
# chrG4: chr19, chr22

chr_groups = {
    "G1": [str(v) for v in list(range(2, 7)) + [8, 13, 18]],
    "G2": [str(v) for v in list(range(9, 13)) + [1, 7, 14, 15, 20, 21]],
    "G3": ["16", "17"],
    "G4": ["19", "22"]
}

model_ids = {
    "G1": "rf_model.v6.v4.1",
    "G2": "rf_model.v6.v3.1",
    "G3": "rf_model.v6.v1.1",
    "G4": "rf_model.v6.v2.1"
}

def build_gc_corrected(entry, chr_grp):
    chrs = chr_groups[chr_grp]
    return [f"results/20200914-week-38/training/gc/{entry}.chr{chr}.gc_corrected.bed.gz" for chr in chrs]

def build_cm(entry, chr_grp):
    chrs = chr_groups[chr_grp]
    return [f"results/20200914-week-38/training/cm/{entry}.cm.ks.ss20k.chr{chr}.seed11.bed.gz" for chr in chrs]

rule training_dataset:
    input: 
        gc_corrected=lambda wildcards: build_gc_corrected(wildcards.entry_id, wildcards.chr_group),
        mappability="data/mappability/mappability.500kb.bed",
        cm=lambda wildcards: build_cm(wildcards.entry_id, wildcards.chr_group)
    output: "results/20200921-week-39/training/cristiano/training_dataset.{entry_id,EE[0-9]+}.chr{chr_group}.tsv.gz"
    threads: 1
    resources:
        cpus=lambda wildcards, threads: threads,
        mem_mb=lambda wildcards, threads: threads * 4200,
        time_min=90
    params:
        label=lambda wildcards: f"{wildcards.entry_id}.chr{wildcards.chr_group}",
        chr_names=lambda wildcards: ":".join(chr_groups[wildcards.chr_group]),
        partition="RM-shared"
    shell:
        """
        R_LIBS=/home/haizizh/Rpackages/4.0/ /opt/packages/R/4.0.0-mkl/bin/Rscript --vanilla \
            scripts/20200921-week-39/22.prepare.dataset.R {wildcards.entry_id} {params.chr_names} wbc.rep2 $LOCAL/output.{params.label}.tsv.gz

        mkdir -p results/20200921-week-39/training/cristiano/
        cp -a $LOCAL/output.{params.label}.tsv.gz {output}.tmp
        mv {output}.tmp {output}
        """
        
        
rule test_dataset:
    input: 
        gc_corrected=lambda wildcards: build_gc_corrected(wildcards.entry_id, wildcards.chr_group),
        mappability="data/mappability/mappability.500kb.bed",
        cm=lambda wildcards: build_cm(wildcards.entry_id, wildcards.chr_group)
    output: "results/20200921-week-39/test/test_dataset.{entry_id,EE[0-9]+}.chr{chr_group}.tsv.gz"
    threads: 1
    resources:
        cpus=lambda wildcards, threads: threads,
        mem_mb=lambda wildcards, threads: threads * 4200,
        time_min=90
    params:
        label=lambda wildcards: f"{wildcards.entry_id}.chr{wildcards.chr_group}",
        chr_names=lambda wildcards: ":".join(chr_groups[wildcards.chr_group]),
        partition="RM-shared"
    shell:
        """
        R_LIBS=/home/haizizh/Rpackages/4.0/ /opt/packages/R/4.0.0-mkl/bin/Rscript --vanilla \
            scripts/20200921-week-39/22.prepare.dataset.R {wildcards.entry_id} {params.chr_names} wbc.rep2 $LOCAL/output.{params.label}.tsv.gz

        mkdir -p results/20200921-week-39/training/cristiano/
        cp -a $LOCAL/output.{params.label}.tsv.gz {output}.tmp
        mv {output}.tmp {output}
        """
        
        
rule rf_prediction:
    input:
        "results/20200921-week-39/test/test_dataset.{entry_id,EE[0-9]+}.chr{chr_group}.tsv.gz"
    output: 
        "results/20200921-week-39/test/test_dataset.{entry_id,EE[0-9]+}.chr{chr_group}.predicted.tsv.gz"
    threads: 1
    resources:
        cpus=lambda wildcards, threads: threads,
        mem_mb=lambda wildcards, threads: threads * 4200,
        time_min=30
    params:
        label=lambda wildcards: f"{wildcards.entry_id}.chr{wildcards.chr_group}",
        model_id=lambda wildcards: model_ids[wildcards.chr_group],
        partition="RM-shared"
    shell:
        """
        R_LIBS=/home/haizizh/Rpackages/4.0/ /opt/packages/R/4.0.0-mkl/bin/Rscript --vanilla \
            scripts/20200921-week-39/24.predict.rf.R test_dataset.{wildcards.entry_id}.chr{wildcards.chr_group} {params.model_id} \
            $LOCAL/output.{params.label}.tsv.gz \
            169.228.63.120 53383

        mkdir -p results/20200921-week-39/test/
        cp -a $LOCAL/output.{params.label}.tsv.gz {output}.tmp
        mv {output}.tmp {output}
        """
