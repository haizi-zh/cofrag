# Sep 23, 2020
# Prepare training/testing datasets

library(here)
library(tidyverse)
library(magrittr)
library(logging)


rstudio_job_mode <- FALSE
args <- get0("job_args")
if (is.null(args)) {
  if (!interactive()) {
    args <- commandArgs(trailingOnly = TRUE)
  } else{
    stop()
  }
} else {
  rstudio_job_mode <- TRUE
}

entries <- str_split(args[1], ":")[[1]]
chrs <- str_split(args[2], ":")[[1]]
response_type <- args[3]  # allowed values: full / wbc.rep1 / wbc.rep2
output_file <- args[4]

source(here("src/genomic_matrix.R"), local = TRUE)

loginfo(str_interp("Building training dataset for ${paste(entries, collapse = \", \")} / ${chrs %>% map_chr(~ paste0(\"chr\", .)) %>% paste(collapse = \", \")}"))


calc_neighborhood_mean <- function(m) {
  shift_grid <-
    expand_grid(x = -1:1, y = -1:1) %>% filter(!(x == 0 & y == 0))
  
  # Subset of m
  m1 <- matrix(0, nrow = nrow(m) - 2, ncol = ncol(m) - 2) 
  count_m1 <- matrix(0, nrow = nrow(m1), ncol = ncol(m1))
  
  shift_grid %>% pwalk(function(x, y) {
    m2 <- m[(2 + x):(nrow(m) - 1 + x), (2 + y):(ncol(m) - 1 + y)]
    na_idx <- c(is.na(m2))
    # If the position is NA, then we set it as 0
    m2[na_idx] <- 0
    # If the position is not NA, then we count it as 1
    count_m2 <- matrix(1, nrow = nrow(m2), ncol = ncol(m2))
    count_m2[na_idx] <- 0
    
    m1 <<- m1 + m2
    count_m1 <<- count_m1 + count_m2
  })
  rownames(m1) <- rownames(m)[2:(nrow(m) - 1)]
  colnames(m1) <- colnames(m)[2:(ncol(m) - 1)]
  
  m1 <- m1 / count_m1
  
  expand_grid(start1 = 1:nrow(m1), start2 = 1:ncol(m1)) %>%
    # filter(start1 <= start2) %>%
    arrange(start2, start1) %>%
    mutate(neighbor_mean = c(m1),
           start1 = rownames(m1)[start1] %>% as.integer(),
           start2 = colnames(m1)[start2] %>% as.integer()) %>%
    filter(start1 <= start2) %>% arrange(start2, start1)
}

calc_neighborhood <- function(cm) {
  mat <- convert_to_matrix(cm)
  neighbor_mean <- calc_neighborhood_mean(mat)
}


prepare_predictor_dataset <- function(entries, chrs, keep_diag = FALSE, bin_size = 500e3L, full_features = FALSE) {
  training_grid <- expand_grid(entry = entries, chr = chrs)
  
  # Load the GC-corrected coverage data
  gc_corrected <- training_grid %>%
    pmap_dfr(function(entry, chr) {
      read_tsv(here(
        str_interp(
          "results/20200914-week-38/training/gc/${entry}.chr${chr}.gc_corrected.bed.gz"
        )
      )) %>%
        mutate(
          chr = factor(as.character(chr), levels = 1:22 %>% map_chr(as.character)),
          start = as.integer(start),
          end = as.integer(end),
          entry = entry
        )
    }) %>% arrange(chr, start)
  
  # Mappability
  mappability <- read_tsv(here("data/mappability/mappability.500kb.bed"), skip = 1, 
                          col_names = c("chr", "start", "end", "maps"),
                          col_types = cols(col_character(), col_integer(), col_integer(), col_double())) %>% 
    mutate(chr = factor(as.character(chr), levels = 1:22 %>% map_chr(as.character)))
  
  
  cm <- training_grid %>%
    pmap_dfr(function(entry, chr) {
      # cm_fleet <- c("ks", "cvm.stat", "cucconi.stat") %>% map(~ {
      # metrics <- .
      loginfo(paste(entry, chr, collapse = "/"))
      
      metrics <- "ks"
      cm <- load_genomic_matrix(
        here(
          str_interp(
            "results/20200914-week-38/training/cm/${entry}.cm.${metrics}.ss20k.chr${chr}.seed11.bed.gz"
          )
        ), 
        # additional_col_names = c("score2", "bootstrap", "frag1", "frag2", "pvalue")
        additional_col_names = c("score2", "bootstrap")) %>% #, "bootstrap", "frag1", "frag2", "pvalue")) %>%
          # as_tibble() %>%
          # select(-frag1, -frag2) %>%
          mutate(
            chr = factor(as.character(chr), levels = 1:22 %>% map_chr(as.character)),
            bootstrap = as.integer(bootstrap),
            entry = entry,
            metrics = metrics
          ) %>%
          # Remove diagonal entries and zero-clipped entries
          # filter(start1 != start2 & pvalue > 2e-16 & !is.na(score2)) %>%
          filter(!is.na(score2))
      
      if (!keep_diag)
        cm %<>% filter(start1 != start2)
      
      # Only keep score2 as scores
      cm %<>%
        select(chr, start1, start2, score2, bootstrap, entry, metrics) %>% #, everything(), -score, -bootstrap, -pvalue) %>%
        rename(score = score2)
      
      # Calculate the normalized distance
      bin_starts <- c(cm$start1, cm$start2)
      ep_left <- min(bin_starts)
      ep_right <- max(bin_starts) + bin_size
      roi_length <- ep_right - ep_left
      # cm %<>% mutate(dist = abs(start1 - start2) / roi_length)
      # For now, use the bin-difference as the distance
      cm %<>% mutate(dist = abs(start1 - start2) %/% bin_size)
      
      
      # Get neighborhood
      neighborhood <- cm$bootstrap %>% unique() %>% map_dfr(~ {
        bootstrap <- .
        calc_neighborhood(cm %>% filter(bootstrap == !!bootstrap)) %>%
          mutate(bootstrap = bootstrap, entry = entry, chr = chr)
      })
      
      cm %>% inner_join(
        x = .,
        y = neighborhood,
        by = c("entry", "chr", "start1", "start2", "bootstrap")
      )
    }) 
  
  
  
  cm %<>% inner_join(
    x = .,
    y = gc_corrected %>% select(-end, -corrected_cov, -cov_raw),
    by = c(chr = "chr", start1 = "start", entry = "entry")
  ) %>%
    rename(gc.1=gc, mean.1 = mean, median.1 = median, sd.1 = sd, skewness.1 = skewness, kurtosis.1 = kurtosis, short2long.1 = short2long, normalized_cov.1 = normalized_cov, peak.1 = peak) %>%
    inner_join(
      x = .,
      y = gc_corrected %>% select(-end, -corrected_cov, -cov_raw),
      by = c(chr = "chr", start2 = "start", entry = "entry")
    ) %>%
    rename(gc.2 = gc, mean.2 = mean, median.2 = median, sd.2 = sd, skewness.2 = skewness, kurtosis.2 = kurtosis, short2long.2 = short2long, normalized_cov.2 = normalized_cov, peak.2 = peak) %>%
    inner_join(
      x = .,
      y = mappability %>% select(-end),
      by = c(chr = "chr", start1 = "start")
    ) %>%
    rename(maps.1 = maps) %>%
    inner_join(
      x = .,
      y = mappability %>% select(-end),
      by = c(chr = "chr", start2 = "start")
    ) %>%
    rename(maps.2 = maps)
  
  if (full_features) {
    cm
  }
  else {
    # Remove colinear features
    cm %>% 
      select(-median.1, -median.2, -short2long.1, -short2long.2, -maps.1, -maps.2)#, -n_large_neighbor)
  }
}


prepare_training_dataset <- function(entries, chrs, response_type = "full", keep_diag = FALSE) {
  predictor_dataset <- prepare_predictor_dataset(entries, chrs, keep_diag = keep_diag)
  
  wbc_type <- if (response_type == "full")
    c("wbc.rep1", "wbc.rep2")
  else if (response_type %in% c("wbc.rep1", "wbc.rep2"))
    response_type
  else
    stop()
  
  loginfo(str_interp("Using wbc data: ${wbc_type}"))
  
  # Load wbc data
  wbc_data <- expand_grid(wbc = wbc_type, chr = chrs) %>% pmap_dfr(function(wbc, chr) {
    read_tsv(
      here(
        str_interp("data/${wbc}/${wbc}.chr${chr}.500kb.oe.none.txt")
      ),
      col_names = c("start1", "start2", "score"),
      col_types = cols(col_integer(), col_integer(), col_double())
    ) %>%
      mutate(chr = factor(as.character(chr), levels = 1:22 %>% map_chr(as.character)),
             wbc = wbc) %>%
      rename(response = score)
  })
  
  
  inner_join(predictor_dataset, wbc_data, by = c("chr", "start1", "start2"))
}


training_dataset <- prepare_training_dataset(entries, chrs, response_type, keep_diag = TRUE)

loginfo(str_interp("Writing to ${output_file} ..."))
write_tsv(training_dataset, path = output_file)