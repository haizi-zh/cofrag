__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
import os
import sys

# smk file home location
smk_path = path.normpath(path.dirname(__file__))
sys.path = [smk_path] + sys.path

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell
from snakemake.logging import logger


def intersect_critical_sites():
    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell(
            "bedtools intersect "
            "-a <(zcat {snakemake.input.frag}) "
            "-b <(zcat {snakemake.input.crit}) "
            "-wa -wb | "
            "bgzip > {tempdir}/temp.bed.gz"
        )

        logger.info("Indexing fragments...")
        shell(
            "tabix -p bed {tempdir}/temp.bed.gz"
        )

        logger.info("Copying to destination...")
        shell(
            "cp -a {tempdir}/temp.bed.gz {snakemake.output.bed}.tmp"
        )
        shell(
            "mv {snakemake.output.bed}.tmp {snakemake.output.bed}"
        )
        shell(
            "mv {tempdir}/temp.bed.gz.tbi {snakemake.output.bed_idx}"
        )
        shell(
            "tabix -p bed {tempdir}/temp.bed.gz"
        )


def separate_fragments():
    batch_size = snakemake.config.get("BATCH_SIZE", "50000")
    min_dgp = snakemake.config.get("MIN_DGP", "0.5")

    if "recipient" in snakemake.wildcards.sepdir:
        logger.info("Run in recipient mode: extract recipient cfDNA fragments")
        negate = True
        negate_arg = "--negate"
        min_dgp = "0"
    else:
        negate_arg = ""

    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell(
            "Rscript scripts/20201123-week-48/65.separate.cfDNA.fragments.R "
            # "--nrows 50000 "
            "--min-DGP {min_dgp} "
            "--batch-size {batch_size} "
            "--separation {snakemake.input.crit} "
            "--bam {snakemake.input.bam} "
            "{negate_arg} "
            "--output {tempdir}/temp0.bed.gz "
        )

        logger.info("Deduplicating fragments...")
        shell(
            "zcat {tempdir}/temp0.bed.gz | uniq | bgzip > {tempdir}/temp.bed.gz"
        )
        shell("rm {tempdir}/temp0.bed.gz")

        logger.info("Indexing...")
        shell(
            "tabix -p bed {tempdir}/temp.bed.gz"
        )

        logger.info("Copying to destination...")
        shell(
            "cp -a {tempdir}/temp.bed.gz {snakemake.output.frag}.tmp"
        )
        shell(
            "mv {snakemake.output.frag}.tmp {snakemake.output.frag}"
        )
        shell(
            "mv {tempdir}/temp.bed.gz.tbi {snakemake.output.frag_idx}"
        )


def calc_gm():
    if "maternal" in snakemake.wildcards.sepdir:
        raise

        # # Run in maternal mode: extract maternal cfDNA fragments
        # logger.info("Run in maternal mode: extract maternal cfDNA fragments")
        # downsample_target = "results/20201123-week-48/trio-variant-calling/separation-DGP0.1/" \
        # f"{snakemake.wildcards.s1}W.{snakemake.wildcards.s2}W/" \
        # f"{snakemake.wildcards.s1}_plasma.separated.frags." \
        # f"chr{snakemake.wildcards.chrom}.bed.gz"
        
        # logger.info(f"Downsample to {downsample_target}")
        # downsample_arg = f"--downsample {downsample_target}"
    else:
        downsample_arg = ""

    assert "maternal" not in snakemake.wildcards.sepdir
    assert snakemake.wildcards.sample_type == "donor"

    res = snakemake.wildcards.res
    if res.endswith("mb"):
        res = int(int(res[:-2]) * 1e6)
    elif res.endswith("kb"):
        res = int(int(res[:-2]) * 1e3)
    else:
        logger.error(f"Invalid resolution: {res}")
        raise

    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        logger.info("Merging fragment files ...")

        sample_list = [f"LVT0{v}R.LVT0{v}D" for v in list(range(1,8)) + [9]] + \
            [f"LVT{v}R.LVT{v}D" for v in [11, 12]] + ["T99W.T100W"]
        
        frag_files = [f"results/20201123-week-48/liver/{snakemake.wildcards.sepdir}/{v}/{v}.separated.frags.chr{snakemake.wildcards.chrom}.bed.gz" for v in sample_list]
        logger.info(f"Fragment files: {' '.join(frag_files)}")
        for f in frag_files:
            shell(
                "zcat {f} | tail -n +2 | bgzip >> {tempdir}/merged0.bed.gz"
            )

        logger.info("Sorting...")
        shell(
            "zcat {tempdir}/merged0.bed.gz | sort -k 1,1 -k2,2n | bgzip > {tempdir}/merged.sorted.bed.gz"
        )
        shell(
            "tabix -p bed {tempdir}/merged.sorted.bed.gz"
        )
        shell(
            "zcat {tempdir}/merged.sorted.bed.gz | wc -l"
        )

        subsample = int(snakemake.wildcards.ss) * 1000

        shell(
            "Rscript scripts/20201123-week-48/66.calc.contact.matrix.R "
            "--input {tempdir}/merged.sorted.bed.gz "
            "--res {res} "
            "--metrics {snakemake.wildcards.metrics} "
            "--ncores {snakemake.threads} "
            "--bootstrap 10 "
            "--subsample {subsample} "
            "--seed {snakemake.wildcards.seed} "
            "{downsample_arg} "
            "--output {tempdir}/temp.bed.gz "            
        )

        logger.info("Copying to destination...")
        shell(
            "cp -a {tempdir}/temp.bed.gz {snakemake.output}.tmp"
        )
        shell(
            "mv {snakemake.output}.tmp {snakemake.output}"
        )


if snakemake.rule == "intersect_critical_sites":
    intersect_critical_sites()
elif snakemake.rule == "separate_fragments":
    separate_fragments()
elif snakemake.rule == "calc_gm":
    calc_gm()
else:
    raise RuntimeError(f"Invalid rule: {snakemake.rule}")