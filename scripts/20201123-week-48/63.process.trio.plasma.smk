import os
import math

def calc_time_min(file_path):
    size_gb = math.ceil(os.path.getsize(file_path) / 1024**3 )
    hours = min(round(12 + 0.6 * size_gb), 72)
    return hours * 60

def calc_threads(file_path):
    size_gb = math.ceil(os.path.getsize(file_path) / 1024**3 )
    threads = min(round(4 + 0.1 * size_gb), 14)
    return threads

rule bam_query_sort:
    input:
        bam="results/20201116-week-47/trio-variant-calling/bam-bqsr/{sample}.chr{chrom}.bqsr.bam",
        bai="results/20201116-week-47/trio-variant-calling/bam-bqsr/{sample}.chr{chrom}.bqsr.bam.bai",
    output:
        temp("results/20201123-week-48/trio-variant-calling/temp/{sample}.chr{chrom}.bqsr.qsorted.bam")
    params:
        label=lambda wildcards: f"bam_query_sort.{wildcards.sample}.chr{wildcards.chrom}",
        mem_mb_per_thread=lambda wildcards, threads, resources: int((resources.mem_mb - 2000) * 0.8 / threads),
        input_size=lambda wildcards, input: f"{round(os.path.getsize(input.bam) / 1024**2)}mb",
    threads: lambda wildcards, input: calc_threads(input.bam)
    resources:
        mem_mb=lambda wildcards, threads: threads * 4200,
        time=4320,
        time_min=lambda wildcards, input: calc_time_min(input.bam),
    script: "63.process.trio.plasma.py"


rule infer_fragments:
    input:
        "results/20201123-week-48/trio-variant-calling/temp/{sample}.chr{chrom}.bqsr.qsorted.bam"
    output:
        frag="results/20201123-week-48/trio-variant-calling/frag/{sample}.chr{chrom}.frag.tsv.gz",
        frag_idx="results/20201123-week-48/trio-variant-calling/frag/{sample}.chr{chrom}.frag.tsv.gz.tbi",
    params:
        label=lambda wildcards: f"infer_fragments.{wildcards.sample}.chr{wildcards.chrom}",
        mem_mb_sortbed=lambda wildcards, threads, resources: int((resources.mem_mb - 2000) * 0.8),
        input_size=lambda wildcards, input: f"{round(os.path.getsize(input[0]) / 1024**2)}mb",
    threads: lambda wildcards, input: calc_threads(input[0])
    resources:
        mem_mb=lambda wildcards, threads: threads * 4200,
        time=4320,
        time_min=lambda wildcards, input: calc_time_min(input[0]),
    script: "63.process.trio.plasma.py"


# Apply bedtools intersect on frag.tsv.gz at critical sites
# The result will be used for separating donor(paternal) cfDNA fragments
rule intersect_critical_sites:
    input:
    #frag/M12148_plasma.chr22.frag.tsv.gz
        frag="results/20201123-week-48/trio-variant-calling/frag/{s1}_plasma.chr{chrom}.frag.tsv.gz",
        crit="results/20201123-week-48/trio-variant-calling/mis/{s1}W.{s2}W/{s1}W.{s2}W.chr{chrom}.crit.bed.gz",
    output:
        bed="results/20201123-week-48/trio-variant-calling/separation/{s1}W.{s2}W/{s1}_plasma.intersect.chr{chrom}.crit.bed.gz",
        bed_idx="results/20201123-week-48/trio-variant-calling/separation/{s1}W.{s2}W/{s1}_plasma.intersect.chr{chrom}.crit.bed.gz.tbi",
    params:
        label=lambda wildcards: f"intersect_critical_sites.{wildcards.s1}_plasma.chr{wildcards.chrom}",
    threads: 2
    resources:
        time=4320,
        time_min=120
    script: "63.process.trio.plasma.py"


# Subset the bam file around critical sites
rule bam_critical_sites:
    input:
        bam="results/20201116-week-47/trio-variant-calling/bam-bqsr/{s1}_plasma.chr{chrom}.bqsr.bam",
        bai="results/20201116-week-47/trio-variant-calling/bam-bqsr/{s1}_plasma.chr{chrom}.bqsr.bam.bai",
        crit="results/20201123-week-48/trio-variant-calling/separation/{s1}W.{s2}W/{s1}_plasma.intersect.chr{chrom}.crit.bed.gz",
        crit_idx="results/20201123-week-48/trio-variant-calling/separation/{s1}W.{s2}W/{s1}_plasma.intersect.chr{chrom}.crit.bed.gz.tbi",
    output:
        bam="results/20201123-week-48/trio-variant-calling/separation/{s1}W.{s2}W/{s1}_plasma.intersect.chr{chrom}.crit.bam",
        bai="results/20201123-week-48/trio-variant-calling/separation/{s1}W.{s2}W/{s1}_plasma.intersect.chr{chrom}.crit.bam.bai",
    params:
        label=lambda wildcards: f"bam_critical_sites.{wildcards.s1}_plasma.chr{wildcards.chrom}",
    threads: 4
    resources:
        time=4320,
        time_min=1440,
    script: "63.process.trio.plasma.py"


# Configuration:
# NTHREADS:
# NROWS:
# BATCH_SIZE:
# MIN_DGP:
# NEGATE
rule separate_fragments:
    input:
        bam="results/20201116-week-47/trio-variant-calling/bam-bqsr/{s1}_plasma.chr{chrom}.bqsr.bam",
        bai="results/20201116-week-47/trio-variant-calling/bam-bqsr/{s1}_plasma.chr{chrom}.bqsr.bam.bai",
        crit="results/20201123-week-48/trio-variant-calling/separation/{s1}W.{s2}W/{s1}_plasma.intersect.chr{chrom}.crit.bed.gz",
        crit_idx="results/20201123-week-48/trio-variant-calling/separation/{s1}W.{s2}W/{s1}_plasma.intersect.chr{chrom}.crit.bed.gz.tbi",
    output:
        # frag="results/20201123-week-48/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.frags.chr{chrom}.bed.gz",
        # frag_idx="results/20201123-week-48/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.frags.chr{chrom}.bed.gz.tbi",
        frag="{prefix}/{sepdir,[^/]+}/{s1}W.{s2}W/{s1}_plasma.separated.frags.chr{chrom}.bed.gz",
        frag_idx="{prefix}/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.frags.chr{chrom}.bed.gz.tbi",
    params:
        label=lambda wildcards: f"separate_fragments.{wildcards.s1}_plasma.chr{wildcards.chrom}",
    threads: config.get("NTHREADS", 4)
    resources:
        time=4320,
        time_min=1440,
    script: "63.process.trio.plasma.py" 


rule calc_gm:
    input: 
        "results/20201123-week-48/trio-variant-calling/{sepdir}/{s1}W.{s2}W/{s1}_plasma.separated.frags.chr{chrom}.bed.gz",
    output: 
        "results/20201123-week-48/trio-variant-calling/{sepdir}/gm/{s1}W.{s2}W/{s1}_plasma.gm.{metrics,[^\\.]+}.{res,[^\\.]+}.seed{seed}.ss{ss}k.chr{chrom}.bed.gz",
    params:
        label=lambda wildcards: f"calc_gm.{wildcards.s1}_plasma.chr{wildcards.chrom}",
    threads: config.get("NTHREADS", 4)
    resources:
        time=4320,
        time_min=60,
    script: "63.process.trio.plasma.py"