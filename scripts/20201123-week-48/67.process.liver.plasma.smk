# In October I prepared cfDNA fragment data for liver transplantation
# However, the data format is incompatible with current scripts.
# Need to convert
rule convert_frag:
    input:
        "results/20201019-week-43/genotype/sun2019gr.unclipped/frags/{sample}/{sample}.chr{chrom}.bed.gz"
    output:
        frag="results/20201123-week-48/liver/frag/{sample}.frag.chr{chrom}.bed.gz",
        frag_idx="results/20201123-week-48/liver/frag/{sample}.frag.chr{chrom}.bed.gz.tbi",
    params:
        label=lambda wildcards: f"convert_frag.{wildcards.sample}.chr{wildcards.chrom}"
    threads: 1
    resources:
        time=4320,
        time_min=120
    shell:
        """
        tempdir=$(mktemp -d)

        zcat {input} |
        awk -F '\\t' 'BEGIN{{OFS=FS}} {{if ($2<$6) {{print $1,$2,$6,$8,$9,$7}} else {{print $1,$5,$3,$8,$9,$7}} }}' |
        sort -k 1,1 -k2,2n |
        bgzip > $tempdir/temp.bed.gz

        echo "Indexing..."
        tabix -p bed $tempdir/temp.bed.gz
        cp -a $tempdir/temp.bed.gz {output.frag}.tmp
        mv {output.frag}.tmp {output.frag}
        mv $tempdir/temp.bed.gz.tbi {output.frag_idx}
        """


sample_sheet = {
    "LVT01R": "EE85748",
    "LVT02R": "EE85751",
    "LVT03R": "EE85744",
    "LVT04R": "EE85738",
    "LVT05R": "EE85728",
    "LVT06R": "EE85742",
    "LVT07R": "EE85735",
    "LVT09R": "EE85725",
    "LVT11R": "EE85740",
    "LVT12R": "EE85736",
    "T99W": "EE85747"
}

def infer_entry_id(recipient):
    return sample_sheet[recipient]


# Apply bedtools intersect on frag.tsv.gz at critical sites
# The result will be used for separating donor(paternal) cfDNA fragments
rule intersect_critical_sites:
    input:
        frag=lambda wildcards: f"results/20201123-week-48/liver/frag/{infer_entry_id(wildcards.s1)}.frag.chr{wildcards.chrom}.bed.gz",
        crit="results/20201123-week-48/liver/mis/{s1}.{s2}/{s1}.{s2}.chr{chrom}.crit.bed.gz",
    output:
        bed="results/20201123-week-48/liver/separation/{s1}.{s2}/{s1}.{s2}.intersect.chr{chrom}.crit.bed.gz",
        bed_idx="results/20201123-week-48/liver/separation/{s1}.{s2}/{s1}.{s2}.intersect.chr{chrom}.crit.bed.gz.tbi",
    params:
        label=lambda wildcards: f"intersect_critical_sites.{wildcards.s1}.chr{wildcards.chrom}",
    threads: 2
    resources:
        time=4320,
        time_min=120
    script: "67.process.liver.plasma.py"



# Configuration:
# NTHREADS:
# NROWS:
# BATCH_SIZE:
# MIN_DGP:
rule separate_fragments:
    input:
        bam=lambda wildcards: f"results/20201019-week-43/genotype/sun2019gr.unclipped/bam/{infer_entry_id(wildcards.s1)}.hg19.mdups.bam",
        bai=lambda wildcards: f"results/20201019-week-43/genotype/sun2019gr.unclipped/bam/{infer_entry_id(wildcards.s1)}.hg19.mdups.bam.bai",
        crit="results/20201123-week-48/liver/separation/{s1}.{s2}/{s1}.{s2}.intersect.chr{chrom}.crit.bed.gz",
        crit_idx="results/20201123-week-48/liver/separation/{s1}.{s2}/{s1}.{s2}.intersect.chr{chrom}.crit.bed.gz.tbi",
    output:
        frag="results/20201123-week-48/liver/{sepdir}/{s1}.{s2}/{s1}.{s2}.separated.frags.chr{chrom}.bed.gz",
        frag_idx="results/20201123-week-48/liver/{sepdir}/{s1}.{s2}/{s1}.{s2}.separated.frags.chr{chrom}.bed.gz.tbi",
    params:
        label=lambda wildcards: f"separate_fragments.{wildcards.s1}_plasma.chr{wildcards.chrom}",
    threads: config.get("NTHREADS", 3)
    resources:
        time=4320,
        time_min=1440,
    script: "67.process.liver.plasma.py" 


rule calc_gm:
    output: 
        "results/20201123-week-48/liver/{sepdir}/{sample_type}.gm.{metrics,[^\\.]+}.{res,[^\\.]+}.seed{seed}.ss{ss}k.chr{chrom}.bed.gz",
    params:
        label=lambda wildcards: f"calc_gm.{wildcards.sample_type}.chr{wildcards.chrom}",
    threads: config.get("NTHREADS", 4)
    resources:
        time=4320,
        time_min=60,
    script: "67.process.liver.plasma.py"