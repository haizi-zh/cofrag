# Generate QC reports

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
import os
import sys

# smk file home location
smk_path = path.normpath(path.dirname(__file__))
sys.path = [smk_path] + sys.path

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell
from snakemake.logging import logger


def bam_query_sort():
    def func(tempdir):
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")
        shell(
            "samtools view -f 3 -F 3852 -q 30 -h {snakemake.input.bam} | "
            "samtools sort -@ {snakemake.threads} -n "
            "-m {snakemake.params.mem_mb_per_thread}M "
            "-T {tempdir} -o {tempdir}/temp.qsorted.bam - "
        )

        shell("cp -a {tempdir}/temp.qsorted.bam {snakemake.output}.tmp")
        shell("mv {snakemake.output}.tmp {snakemake.output}")

    if "LOCAL" not in os.environ:
        with TemporaryDirectory() as tempdir:
            func(tempdir)
    else:
        func(os.environ["LOCAL"])


def infer_fragments():
    def func(tempdir):
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell(
            "bamToBed -bedpe -mate1 -i {snakemake.input} | "
            """perl -ne 'chomp;@f=split " ";if($f[0] ne $f[3]){{next;}}$s=$f[1];$e=$f[5];if($f[8] eq "-"){{$s=$f[4];$e=$f[2];}}if($e>$s){{print "$f[0]\\t$s\\t$e\\t$f[7]\\t$f[8]\\t$f[6]\\n";}}' | """
            "sort-bed --max-mem {snakemake.params.mem_mb_sortbed}M --tmpdir {tempdir} - | "
            "bgzip > {tempdir}/temp.tsv.gz"
        )

        logger.info("Indexing fragments...")
        shell("tabix -p bed {tempdir}/temp.tsv.gz")

        shell("cp -a {tempdir}/temp.tsv.gz {snakemake.output.frag}.tmp")
        shell("mv {snakemake.output.frag}.tmp {snakemake.output.frag}")
        shell("mv {tempdir}/temp.tsv.gz.tbi {snakemake.output.frag_idx}")

    if "LOCAL" not in os.environ:
        with TemporaryDirectory() as tempdir:
            func(tempdir)
    else:
        func(os.environ["LOCAL"])


def intersect_critical_sites():
    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell(
            "bedtools intersect "
            "-a <(zcat {snakemake.input.frag}) "
            "-b <(zcat {snakemake.input.crit}) "
            "-wa -wb | "
            "bgzip > {tempdir}/temp.bed.gz"
        )

        logger.info("Indexing fragments...")
        shell("tabix -p bed {tempdir}/temp.bed.gz")

        logger.info("Copying to destination...")
        shell("cp -a {tempdir}/temp.bed.gz {snakemake.output.bed}.tmp")
        shell("mv {snakemake.output.bed}.tmp {snakemake.output.bed}")
        shell("mv {tempdir}/temp.bed.gz.tbi {snakemake.output.bed_idx}")
        shell("tabix -p bed {tempdir}/temp.bed.gz")


def bam_critical_sites():
    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell(
            "samtools view -f 3 -F 3852 -q 1 -b {snakemake.input.bam} | "
            "bedtools intersect "
            "-a stdin -b {snakemake.input.crit} "
            "-wa > {tempdir}/temp.bam "
        )

        logger.info("Indexing the BAM file...")
        shell("samtools index {tempdir}/temp.bam")

        logger.info("Copying to destination...")
        shell("cp -a {tempdir}/temp.bam {snakemake.output.bam}.tmp")
        shell("mv {snakemake.output.bam}.tmp {snakemake.output.bam}")
        shell("mv {tempdir}/temp.bam.bai {snakemake.output.bai}")


def separate_fragments():
    batch_size = snakemake.config.get("BATCH_SIZE", "50000")
    min_dgp = snakemake.config.get("MIN_DGP", "0.5")
    negate = snakemake.config.get("NEGATE", False)

    if negate:
        # Run in negate mode, useful for extracting maternal cfDNA fragments
        logger.info("Running in negate mode")
        negate_arg = "--negate"
    else:
        negate_arg = ""

    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        shell(
            "Rscript scripts/20201123-week-48/65.separate.cfDNA.fragments.R "
            # "--nrows 50000 "
            "--min-DGP {min_dgp} "
            "--batch-size {batch_size} "
            "--separation {snakemake.input.crit} "
            "--bam {snakemake.input.bam} "
            "{negate_arg} "
            "--output {tempdir}/temp0.bed.gz "
        )

        logger.info("Deduplicating fragments...")
        shell("zcat {tempdir}/temp0.bed.gz | uniq | bgzip > {tempdir}/temp.bed.gz")
        shell("rm {tempdir}/temp0.bed.gz")

        logger.info("Indexing...")
        shell("tabix -p bed {tempdir}/temp.bed.gz")

        logger.info("Copying to destination...")
        shell("cp -a {tempdir}/temp.bed.gz {snakemake.output.frag}.tmp")
        shell("mv {snakemake.output.frag}.tmp {snakemake.output.frag}")
        shell("mv {tempdir}/temp.bed.gz.tbi {snakemake.output.frag_idx}")


def calc_gm():
    if "maternal" in snakemake.wildcards.sepdir:
        # Run in maternal mode: extract maternal cfDNA fragments
        logger.info("Run in maternal mode: extract maternal cfDNA fragments")
        downsample_target = (
            "results/20201123-week-48/trio-variant-calling/separation-DGP0.1/"
            f"{snakemake.wildcards.s1}W.{snakemake.wildcards.s2}W/"
            f"{snakemake.wildcards.s1}_plasma.separated.frags."
            f"chr{snakemake.wildcards.chrom}.bed.gz"
        )

        logger.info(f"Downsample to {downsample_target}")
        downsample_arg = f"--downsample {downsample_target}"
    else:
        downsample_arg = ""

    res = snakemake.wildcards.res
    if res.endswith("mb"):
        res = int(int(res[:-2]) * 1e6)
    elif res.endswith("kb"):
        res = int(int(res[:-2]) * 1e3)
    else:
        logger.error(f"Invalid resolution: {res}")
        raise

    with TemporaryDirectory() as tempdir:
        logger.info(f"Job started: {snakemake.params.label}")
        logger.info(f"Using temporary directory: {tempdir}")

        subsample = int(snakemake.wildcards.ss) * 1000

        shell(
            "Rscript scripts/20201123-week-48/66.calc.contact.matrix.R "
            "--input {snakemake.input} "
            "--res {res} "
            "--metrics {snakemake.wildcards.metrics} "
            "--ncores {snakemake.threads} "
            "--bootstrap 10 "
            "--subsample {subsample} "
            "--seed {snakemake.wildcards.seed} "
            "{downsample_arg} "
            "--output {tempdir}/temp.bed.gz "
        )

        logger.info("Copying to destination...")
        shell("cp -a {tempdir}/temp.bed.gz {snakemake.output}.tmp")
        shell("mv {snakemake.output}.tmp {snakemake.output}")


if snakemake.rule == "bam_query_sort":
    bam_query_sort()
elif snakemake.rule == "infer_fragments":
    infer_fragments()
elif snakemake.rule == "intersect_critical_sites":
    intersect_critical_sites()
elif snakemake.rule == "bam_critical_sites":
    bam_critical_sites()
elif snakemake.rule == "separate_fragments":
    separate_fragments()
elif snakemake.rule == "calc_gm":
    calc_gm()
else:
    raise RuntimeError(f"Invalid rule: {snakemake.rule}")
