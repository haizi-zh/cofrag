rule masked_vcf2df:
    input: "results/20201019-week-43/genotype/masked_analysis.split0.98/{filename}.vcf.gz"
    output: "results/20201123-week-48/liver/masked_analysis.split0.98/{filename}.tsv.gz"
    params:
        label=lambda wildcards: f"imputed_vcf2df.{wildcards.filename}",
    threads: 1
    resources:
        time=720,
        time_min=240
    shell:
        """
        Rscript $COFRAG/scripts/20201123-week-48/58.vcf2df.R --fields 1 \
            --input {input} --output {output}
        """


# rule imputed_vcf2df:
#     input: "results/20201019-week-43/genotype/mis/data/{filename}.vcf.gz"
#     output: "results/20201123-week-48/liver/mis/{filename}.tsv.gz"
#     params:
#         label=lambda wildcards: f"imputed_vcf2df.{wildcards.filename}",
#     threads: 4
#     resources:
#         time=1440,
#         time_min=360
#     shell:
#         """
#         Rscript $COFRAG/scripts/20201123-week-48/58.vcf2df.R --fields 1,4 \
#             --input {input} --output {output}
#         """


# rule split_imputed_vcf:
#     input: "results/20201123-week-48/liver/mis/{filename}.tsv.gz"
#     output: "results/20201123-week-48/liver/mis/{s1}.{s2}/{s1}.{s2}.{filename}.tsv.gz"
#     params:
#         label=lambda wildcards: f"split_imputed.{wildcards.s1}.{wildcards.s2}.{wildcards.filename}"
#     threads: 2
#     resources:
#         time=1440,
#         time_min=360
#     shell:
#         """
#         zcat {input} | awk '/{wildcards.s1}|{wildcards.s2}/' | bgzip > {output}
#         """

rule imputed_vcf2df:
    input: "{dir}/data/chr{chrom}.dose.vcf.gz"
    output: temp("{dir}/{s1}.{s2}/{s1}.{s2}.chr{chrom}.dose.tsv.gz")
    params:
        label=lambda wildcards: f"imputed_vcf2df.{wildcards.s1}.{wildcards.s2}.chr{wildcards.chrom}",
    threads: 4
    resources:
        time=1440,
        time_min=360
    shell:
        """
        Rscript scripts/20201123-week-48/58.vcf2df.R \
            --input {input} \
            --output {output} \
            --samples {wildcards.s1},{wildcards.s2} \
            --fields GT,GP
        """


rule imputed_bed:
    input: "{dir}/{s1}.{s2}/{s1}.{s2}.chr{chrom}.dose.tsv.gz"
    output: "{dir}/{s1}.{s2}/{s1}.{s2}.chr{chrom}.bed.gz"
    params:
        label=lambda wildcards: f"imputed_bed.{wildcards.s1}.{wildcards.s2}.chr{wildcards.chrom}",
    threads: 2
    resources:
        time=720,
        time_min=120
    shell:
        """
        Rscript scripts/20201123-week-48/60.genotype.postprocess.R \
            --input {input} --output {output}
        """


rule mask_analysis:
    input: 
        imputed="results/20201123-week-48/liver/mis/{s1}.{s2}/{s1}.{s2}.chr{chrom}.dose.tsv.gz",
        masked="results/20201123-week-48/liver/masked_analysis.split0.98/masked.tsv.gz"
    output: 
        "results/20201123-week-48/liver/masked_analysis.split0.98/{s1}.{s2}/{s1}.{s2}.chr{chrom}.mask_analysis.tsv.gz"
    shell:
        """
        Rscript scripts/20201123-week-48/61.imputation.mask.analysis.R \
            --input {input.imputed} --mask {input.masked} --samples {wildcards.s1},{wildcards.s2} --chrom {wildcards.chrom} --output {output}
        """ 


rule infer_critical_sites:
    input: "{dir}/{s1}.{s2}/{s1}.{s2}.chr{chrom}.bed.gz"
    output: "{dir}/{s1}.{s2}/{s1}.{s2}.chr{chrom}.crit.bed.gz"
    params:
        label=lambda wildcards: f"infer_crits.{wildcards.s1}.{wildcards.s2}.chr{wildcards.chrom}"
    threads: 2
    resources:
        time=1440,
        time_min=360
    shell:
        """
        Rscript scripts/20201123-week-48/62.infer.critical.sites.R \
            --input {input} --samples {wildcards.s1},{wildcards.s2} --output {output}
        """
