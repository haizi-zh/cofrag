# MIT License
#
# Copyright (c) 2020 Haizi Zheng
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Author: Haizi Zheng
# Copyright: Copyright 2020, Haizi Zheng
# Email: haizi.zh@gmail.com
# License: MIT
#
# This script separates cfDNA fragments based on genotype data

suppressMessages({
  library(here)
  library(tidyverse)
  library(magrittr)
  source(here("scripts/common/utils.R"))
})

args <- get0("script_args")
if (is.null(args) && !interactive()) {
  # Run in CLI script mode
  parser <- optparse::OptionParser(
    option_list = list(
      optparse::make_option(c("--separation")),
      optparse::make_option(c("--bam")),
      optparse::make_option(c("--nrows"), type = "integer", default = -1),
      optparse::make_option(c("--batch-size"), type = "integer", default = 100e3L),
      optparse::make_option(c("--min-DGP"), type = "double", default = 0.5),
      optparse::make_option(c("--min-MAPQ"), type = "integer", default = 10),
      # When negate is TRUE, the script will extract fragments where extractable is FALSE
      # This is useful when extracting maternal fragments
      optparse::make_option(c("--negate"), action = "store_true", default = FALSE),
      optparse::make_option(c("--output"))
    )
  )
  args <-
    optparse::parse_args(
      parser,
      args = commandArgs(trailingOnly = TRUE),
      convert_hyphens_to_underscores = TRUE
    )
  if (args$nrows == -1)
    args$nrows = Inf
}

logging::loginfo(str_interp("Argument summary:"))
logging::loginfo(str_interp("--separation: ${args$separation}"))
logging::loginfo(str_interp("--bam: ${args$bam}"))
logging::loginfo(str_interp("--nrows: ${args$nrows}"))
logging::loginfo(str_interp("--batch-size: ${args$batch_size}"))
logging::loginfo(str_interp("--min-DGP: ${args$min_DGP}"))
logging::loginfo(str_interp("--min-MAPQ: ${args$min_MAPQ}"))
logging::loginfo(str_interp("--negate: ${args$negate}"))
logging::loginfo(str_interp("--output: ${args$output}"))

separation_table <-
  read_tsv(
    args$separation,
    n_max = args$nrows,
    col_names = FALSE,
    col_types = cols()
  ) %>%
  select(X1:X4, X6, X8, X10:X11, X14:X16)
colnames(separation_table) <- c("CHROM", "START", "END", "MAPQ", "READ_ID", "POS", "REF", "ALT", "DGP.1", "DGP.2", "DELTA")
separation_table %<>%
  modify_at(c(1, 11), as.character) %>%
  modify_at(c(2:4, 6), as.integer) %>%
  filter(DGP.1 >= args$min_DGP & DGP.2 >= args$min_DGP & MAPQ >= args$min_MAPQ)

chrom <- separation_table$CHROM %>% unique()
stopifnot(length(chrom) == 1)

interval <- with(separation_table, {
  c(min(c(START, END)) - 1e3L, max(c(START, END)) + 1e3L)
})
interval <- GenomicRanges::GRanges(str_interp("${chrom}:${interval[1]}-${interval[2]}"))
logging::loginfo(str_interp("Separation table loaded. Found ${nrow(separation_table)} fragments. Genomic range: ${interval}"))

batch_size <- args$batch_size
n_extracted <- 0

# Devided into blocks
seq(1, nrow(separation_table), by = batch_size) %>% walk(function(row_idx) {
  separation_subtable <-
    separation_table %>% slice(row_idx:(row_idx + batch_size - 1))
  
  interval <- with(separation_subtable, {
    c(min(c(START, END)) - 1e3L, max(c(START, END)) + 1e3L)
  })
  interval <- GenomicRanges::GRanges(str_interp("${chrom}:${interval[1]}-${interval[2]}"))
  batch_idx <- (row_idx - 1) %/% batch_size + 1
  logging::loginfo(str_interp("Processing batch #${batch_idx}. Genomic range: ${interval}"))
  
  param <-
    Rsamtools::ScanBamParam(
      what = c("flag", "qname", "seq", "mapq"),
      flag = Rsamtools::scanBamFlag(
        isPaired = TRUE,
        isProperPair = TRUE,
        isUnmappedQuery = FALSE,
        hasUnmappedMate = FALSE,
        isMinusStrand = NA,
        isMateMinusStrand = NA,
        isFirstMateRead = NA,
        isSecondMateRead = NA,
        isSecondaryAlignment = FALSE,
        isNotPassingQualityControls = FALSE,
        isDuplicate = FALSE,
        isSupplementaryAlignment = FALSE
      ),
      which = interval
    )
  bam_results <-
    Rsamtools::scanBam(args$bam, param = param)
  
  # qname                                        flag seq
  # <chr>                                       <int> <chr>
  # 1 SN1049:513:C8P7FACXX:1:1116:3947:163753083     99 AGGAGGTGAAGCAGGGCTGAAGGGCCTCCCTCAGAGCCTTCTCCCACTCTGTGGTGTCCACATCCCCTTGGTCGT
  # 2 HISEQ:508:C8P84ACXX:6:1211:8654:96553         147 GAAGCAGGGCTGAAGGGCCTCCCTCAGAGCCTTCTCCCACTCTGTGGTGTCCACATCCCCTTGGTCGTCCTTGTG
  # 3 HISEQ:508:C8P84ACXX:4:1106:10609:36632        163 GCCTCCCTCAGAGCCTTCTCCCACTCTGTGGTGTCCACATCCCCTTGGTCGTCCTTGTGGGAGGCACTCACCTTT
  # 4 HISEQ:507:C8P7VACXX:1:2210:18085:50013        147 CTTCTCCCACTCTGTGGTGTCCACATCCCCTTGGTCGTCCTTGTGGGAGGCACTCACCTTTTGCTCAGCCTATTG
  bam_results <- tibble(
    qname = bam_results %>% map(~ .$qname) %>% unlist(),
    flag = bam_results %>% map(~ .$flag) %>% unlist(),
    seq = bam_results %>% map(~ .$seq %>% as.character) %>% unlist(),
    mapq = bam_results %>% map(~ .$mapq) %>% unlist()
  )
  logging::loginfo(str_interp("BAM loaded. Found ${nrow(bam_results)} reads"))
  
  # CHROM    START      END  MAPQ READ_ID                                         POS REF   ALT   DGP.1 DGP.2 DELTA  flag seq
  # <chr>    <int>    <int> <int> <chr>                                         <int> <chr> <chr> <dbl> <dbl> <chr> <int> <chr>
  # 1 22    16053613 16053805    51 SN1049:513:C8P7FACXX:8:2311:3097:104166319 16053791 C     A     0.934 0.448 1       163 CCCCTTGGTCGTCCTTGTGGGAGGCACTCACCTTTTGCTCAGCCTATTGTGGCTACAGCCCAGCAGGTCCGAGGT
  # 2 22    16053613 16053805    51 SN1049:513:C8P7FACXX:8:2311:3097:104166319 16053791 C     A     0.934 0.448 1        83 CCTCACCATGAAGGGATGATGTATAGTGGGTGGGGCCTCAGGAGGAAGAGGGCCACCAACCCTACCTGGCCCCTA
  # 3 22    16053619 16053815    50 HISEQ:507:C8P7VACXX:3:2111:6043:36651      16053791 C     A     0.934 0.448 1        99 GGTCGTCCTTGTGGGAGGCACTCACCTTTTGCTCAGCCTATGGTGGCTACAGCCCAGCAGGTCCCAGGTGGCACC
  # 4 22    16053619 16053815    50 HISEQ:507:C8P7VACXX:3:2111:6043:36651      16053791 C     A     0.934 0.448 1       147 AAGGGATGATGTATAGTGGGTGGGGCCTCAGGAGGAAGAGGGCCACCAACCCTTTCTGGCCCCTAACCTGCTGCC
  separation_table_seq <- inner_join(x = separation_subtable,
                                     y = bam_results,
                                     by = c("READ_ID" = "qname"))
  
  # pivot to wider form
  # R1 and R2 side-by-side
  resolve_endpoint_type <- function(flag) {
    if (is.null(flag)) {
      browser()
    }
    # 163/83, 99/147: start/end
    if (bitwAnd(flag, 0x40) > 0) {
      # First in pair
      if (bitwAnd(flag, 0x20) > 0)
        return("S")
      else
        return("E")
    } else {
      # Second in pair
      if (bitwAnd(flag, 0x10) > 0)
        return("E")
      else
        return("S")
    }
  }
  separation_table_seq$ENDPOINT <-
    sapply(separation_table_seq$flag, resolve_endpoint_type)
  
  # CHROM    START      END  MAPQ READ_ID                              POS REF   ALT   DGP.1 DGP.2 DELTA S                                                       E
  # <chr>    <int>    <int> <int> <chr>                              <int> <chr> <chr> <dbl> <dbl> <chr> <chr>                                                   <chr>
  # 1 22    16053613 16053805    51 SN1049:513:C8P7FACXX:8:2311:309…  1.61e7 C     A     0.934 0.448 1     CCCCTTGGTCGTCCTTGTGGGAGGCACTCACCTTTTGCTCAGCCTATTGTGGCT… CCTCACCATGAAGGGATGATGTATAGTGGGTGGGGCCTCAGGAGGAAGAGGGCC…
  # 2 22    16053619 16053815    50 HISEQ:507:C8P7VACXX:3:2111:6043…  1.61e7 C     A     0.934 0.448 1     GGTCGTCCTTGTGGGAGGCACTCACCTTTTGCTCAGCCTATGGTGGCTACAGCC… AAGGGATGATGTATAGTGGGTGGGGCCTCAGGAGGAAGAGGGCCACCAACCCTT…
  separation_table_seq %<>% select(-flag) %>%
    pivot_wider(names_from = "ENDPOINT", values_from = c(seq, mapq)) %>%
    filter(mapq_S >= args$min_MAPQ &
             mapq_E >= args$min_MAPQ &
             !is.na(seq_S) & !is.na(seq_E)) %>%
    select(-mapq_S, -mapq_E)
  
  logging::loginfo(
    str_interp(
      "Successfully joined the BAM and the separation table. Found ${nrow(separation_table_seq)} fragments"
    )
  )
  
  # subset the sequence at the variant loci
  subset_seq_variant <-
    function(frag_start,
             frag_end,
             seq1,
             seq2,
             loci,
             variant) {
      variant_len <- str_length(variant)
      seq1_len <- str_length(seq1)
      # Determine whether seq1/2 covers the loci
      if (loci >= frag_start &&
          loci + variant_len <= frag_start + seq1_len) {
        return(str_sub(seq1, loci - frag_start + 1, loci - frag_start + variant_len))
      }
      
      seq2_len <- str_length(seq2)
      if (loci >= frag_end - seq2_len &&
          loci + variant_len <= frag_end) {
        return(str_sub(
          seq2,
          loci - (frag_end - seq2_len) + 1,
          loci - (frag_end - seq2_len) + variant_len
        ))
      }
      
      return(NULL)
    }
  
  # Determine whether to extract
  is_extractable <-
    function(seq1,
             seq2,
             frag_start,
             frag_end,
             variant_pos,
             ref,
             alt,
             delta) {
      if (str_length(ref) == str_length(alt)) {
        # Compare the sequence subset with REF/ALT
        if (delta == "1") {
          seq_variant_alt <-
            subset_seq_variant(frag_start, frag_end, seq1, seq2, variant_pos, alt)
          return(identical(seq_variant_alt, alt))
        } else if (delta == "0") {
          seq_variant_ref <-
            subset_seq_variant(frag_start, frag_end, seq1, seq2, variant_pos, ref)
          return(identical(seq_variant_ref, ref))
        } else
          return(FALSE)
      } else {
        # When REF and ALT have different lengths, we need to compare the sequence with both of them
        # to determine whether to extract
        seq_variant_ref <-
          subset_seq_variant(frag_start, frag_end, seq1, seq2, variant_pos, ref)
        seq_variant_alt <-
          subset_seq_variant(frag_start, frag_end, seq1, seq2, variant_pos, alt)
        # Compare the sequence subset with REF/ALT
        if (delta == "1")
          return(identical(seq_variant_alt, alt) &&
                   !identical(seq_variant_ref, ref))
        else if (delta == "0")
          return(identical(seq_variant_ref, ref) &&
                   !identical(seq_variant_alt, alt))
        else
          return(FALSE)
      }
    }
  
  # apply(separation_table_seq %>% slice_head(n = 10), MARGIN = 1, is_extractable)
  separation_table_seq$extractable <- with(separation_table_seq, {
    mapply(is_extractable,
           seq_S,
           seq_E,
           START,
           END,
           POS - 1,
           REF,
           ALT,
           DELTA)
  })
  
  if (args$negate)
    separation_table_seq$extractable <- !separation_table_seq$extractable
  
  extracted <- separation_table_seq %>%
    filter(extractable == TRUE) %>%
    select(CHROM:READ_ID)
  n_extracted <<- n_extracted + nrow(extracted)
  logging::loginfo(str_interp("Extracted ${nrow(extracted)} fragments. Total extracted: ${n_extracted}"))
  
  if (row_idx == 1) {
    write_bed_bgzip(extracted, args$output)
  } else {
    write_bed_bgzip(extracted, args$output, append = TRUE, col_names = FALSE)
  }
})


