# Load the fragment bedGraph file
import csv
import sys

def split_format(vcf_format):
    return vcf_format.split(":")

def retrieve_field(vcf_format, field, data):
    vcf_format = split_format(vcf_format)
    idx = vcf_format.index(field)
    try:
        return data.split(":")[idx]
    except IndexError:
        print(f"{vcf_format}\t{field}\t{data}")
        return "."
    

reader = csv.reader(sys.stdin, delimiter="\t")
total_snps = 0
processed_snps = 0
inconsistent_snps = 0
for row in reader:
    total_snps += 1
    vcf_format = row[8]

    sample_1 = row[12]
    genotype_1 = retrieve_field(vcf_format, "GT", sample_1)
    gq_1 = retrieve_field(vcf_format, "GQ", sample_1)
    sample_2 = row[14]
    genotype_2 = retrieve_field(vcf_format, "GT", sample_2)
    gq_2 = retrieve_field(vcf_format, "GQ", sample_2)

    if gq_1 == "." or gq_2 == "." or min(int(gq_1), int(gq_2)) < 90:
        continue

    processed_snps += 1
    if gq_1 != gq_2:
        inconsistent_snps += 1

    # print(f"genotype:\t{genotype_1}\t{genotype_2}\tGQ:\t{gq_1}\t{gq_2}\tformat:\t{vcf_format}\tsample:\t{sample_1}\t{sample_2}")

print("==============")
print(f"Total: {total_snps}, processed: {processed_snps}, inconsistent: {inconsistent_snps}, percentage: {inconsistent_snps / processed_snps}")