# Snakemake script which annotate the VCF with dbsnp IDs

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"


from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

# Run fastqc, since there can be race conditions if multiple jobs
# use the same fastqc dir, we create a temp dir.
with TemporaryDirectory() as tempdir:
    memory = ""
    if "mem_mb" in snakemake.resources.keys():
        # Only allocate 80% of the total memory
        memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.8)))

    temp_vcf = path.join(tempdir, "output.vcf.gz")
    temp_log = path.join(tempdir, "output.log")

#   VariantAnnotator \
#    -R reference.fasta \
#    -I input.bam \
#    -V input.vcf \
#    -o output.vcf \
#    -A Coverage \
#    --dbsnp dbsnp.vcf

    # Generate the index
    shell(
        "gatk --java-options \"{memory}\" "
        "VariantAnnotator "
        "-O {temp_vcf} "
        "-V {snakemake.input.vcf} "
        "-R {snakemake.input.ref} "
        "--dbsnp {snakemake.input.dbsnp} "
        "2> >(tee -a {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_vcf} {snakemake.output.vcf}")
    shell("mv {temp_vcf}.tbi {snakemake.output.vcf_tbi}")
    shell("mv {temp_log} {snakemake.log}")