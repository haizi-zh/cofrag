# This snakemake script apply the BQSR report

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

memory = ""
if "mem_mb" in snakemake.resources.keys():
    # Only allocate 90% of the total memory
    memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.9)))

java_opts = snakemake.params.get("java_opts", "")
if "-Xmx" not in java_opts:
    java_opts = f"{java_opts} {memory} "

extra = snakemake.params.get("extra", "")


with TemporaryDirectory() as tempdir:
    temp_bam = path.join(tempdir, "output.bam")
    temp_bai = path.join(tempdir, "output.bai")
    temp_log = path.join(tempdir, "output.log")

#     gatk --java-options "-XX:+PrintFlagsFinal -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps \
#       -XX:+PrintGCDetails -Xloggc:gc_log.log \
#       -XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10 -Dsamjdk.compression_level=~{compression_level} -Xms3000m" \
#       ApplyBQSR \
#       --create-output-bam-md5 \
#       --add-output-sam-program-record \
#       -R ~{ref_fasta} \
#       -I ~{input_bam} \
#       --use-original-qualities \
#       -O ~{output_bam_basename}.bam \
#       -bqsr ~{recalibration_report} \
#       ~{true='--static-quantized-quals 10' false='' bin_base_qualities} \
#       ~{true='--static-quantized-quals 20' false='' bin_base_qualities} \
#       ~{true='--static-quantized-quals 30' false='' bin_base_qualities} \
#       ~{true='--static-quantized-quals 40' false='' bin_somatic_base_qualities} \
#       ~{true='--static-quantized-quals 50' false='' bin_somatic_base_qualities} \
#       -L ~{sep=" -L " sequence_group_interval}

    shell(
        "gatk --java-options '{java_opts}' "
        "ApplyBQSR "
        "{extra} "
        "-O {temp_bam} "
        "-R {snakemake.input.ref} "
        "-I {snakemake.input.bam} "
        "-bqsr {snakemake.input.bqsr} "
        "-L {snakemake.wildcards.chrom} "
        "--add-output-sam-program-record "
        "--use-original-qualities "
        "2> >(tee {temp_log} >&2) "
    )

    shell(
        "gatk --java-options '{java_opts}' "
        "BuildBamIndex "
        "-I {temp_bam} "
    ) 

    # Move outputs into proper position.
    shell("mv {temp_bam} {snakemake.output.bam}")
    shell("mv {temp_bai} {snakemake.output.bai}")
    shell("mv {temp_log} {snakemake.log}")