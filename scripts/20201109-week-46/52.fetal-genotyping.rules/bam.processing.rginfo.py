# This snakemake script adds RG information (platform info) to the BAM file

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

memory = ""
if "mem_mb" in snakemake.resources.keys():
    # Only allocate 90% of the total memory
    memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.9)))

java_opts = snakemake.params.get("java_opts", "")
if "-Xmx" not in java_opts:
    java_opts = f"{java_opts} {memory} "

extra = snakemake.params.get("extra", "")

with TemporaryDirectory() as tempdir:
    temp_bam = path.join(tempdir, "output.bam")
    temp_bai = path.join(tempdir, "output.bai")

    shell(
        "gatk --java-options '{java_opts}' "
        "AddOrReplaceReadGroups "
        "-O {temp_bam} "
        "-I {snakemake.input.bam} "
        "-RGPL ILLUMINA "
        "-RGLB lib1 "
        "-RGPU unit1 "
        "-RGSM {snakemake.wildcards.sample_name} "
    )

    shell(
        "gatk --java-options '{java_opts}' "
        "BuildBamIndex "
        "-I {temp_bam} "
    ) 

    # Move outputs into proper position.
    shell("mv {temp_bam} {snakemake.output.bam}")
    shell("mv {temp_bai} {snakemake.output.bai}")