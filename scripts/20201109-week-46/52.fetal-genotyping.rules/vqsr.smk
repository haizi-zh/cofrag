rule genotype_index:
    input:
        "results/20201109-week-46/fetal-genome/genotype/joint.chr{chrom}.vcf.gz"
    output:
        "results/20201109-week-46/fetal-genome/genotype/joint.chr{chrom,[0-9XY]+}.vcf.gz.tbi"
    threads: 1
    resources:
        mem_mb=2048
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "vqsr.genotype_index.py"
    
rule sites_only_vcf:
    input:
        vcfs=expand("results/20201109-week-46/fetal-genome/genotype/joint.chr{chrom}.vcf.gz", chrom=CHROMS),
        vcfs_tbi=expand("results/20201109-week-46/fetal-genome/genotype/joint.chr{chrom}.vcf.gz.tbi", chrom=CHROMS)
    output:
        vcf="results/20201109-week-46/fetal-genome/genotype/merged.sites_only.vcf.gz",
        vcf_tbi="results/20201109-week-46/fetal-genome/genotype/merged.sites_only.vcf.gz.tbi"
    log:
        "results/20201109-week-46/fetal-genome/genotype/merged.sites_only.log"
    threads: 4
    resources:
        mem_mb=8192
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "vqsr.sites_only_vcf.py"


rule vqsr_model_indels:
    input: 
        vcf="results/20201109-week-46/fetal-genome/genotype/merged.sites_only.vcf.gz",
        vcf_tbi="results/20201109-week-46/fetal-genome/genotype/merged.sites_only.vcf.gz.tbi",
        mills="data/broad/hg19/hg19_v0_Mills_and_1000G_gold_standard.indels.b37.sites.vcf",
        mills_idx="data/broad/hg19/hg19_v0_Mills_and_1000G_gold_standard.indels.b37.sites.vcf.idx",
        axiom_poly="data/broad/hg19/hg19_v0_Axiom_Exome_Plus.genotypes.all_populations.poly.vcf.gz",
        axiom_poly_tbi="data/broad/hg19/hg19_v0_Axiom_Exome_Plus.genotypes.all_populations.poly.vcf.gz.tbi",
        dbsnp=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz"),
        dbsnp_tbi=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz.tbi")
    output: 
        tranches="results/20201109-week-46/fetal-genome/vqsr/model.indels.tranches",
        recal="results/20201109-week-46/fetal-genome/vqsr/model.indels.recal"
    log:
        "results/20201109-week-46/fetal-genome/vqsr/model.indels.log"
    threads: 7
    resources:
        mem_mb=15360
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "vqsr.train_model_indels.py"


rule vqsr_model_snps:
    input: 
        vcf="results/20201109-week-46/fetal-genome/genotype/merged.sites_only.vcf.gz",
        vcf_tbi="results/20201109-week-46/fetal-genome/genotype/merged.sites_only.vcf.gz.tbi",
        hapmap="data/broad/hg19/hg19_v0_hapmap_3.3.b37.vcf.gz",
        hapmap_tbi="data/broad/hg19/hg19_v0_hapmap_3.3.b37.vcf.gz.tbi",
        omni="data/broad/hg19/hg19_v0_1000G_omni2.5.b37.vcf.gz",
        omni_tbi="data/broad/hg19/hg19_v0_1000G_omni2.5.b37.vcf.gz.tbi",
        g1k="data/broad/hg19/hg19_v0_1000G_phase1.snps.high_confidence.b37.vcf.gz",
        g1k_tbi="data/broad/hg19/hg19_v0_1000G_phase1.snps.high_confidence.b37.vcf.gz.tbi",
        dbsnp=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz"),
        dbsnp_tbi=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz.tbi")
    output: 
        tranches="results/20201109-week-46/fetal-genome/vqsr/model.snps.tranches",
        recal="results/20201109-week-46/fetal-genome/vqsr/model.snps.recal"
    log:
        "results/20201109-week-46/fetal-genome/vqsr/model.snps.log"
    threads: 7
    resources:
        mem_mb=15360
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "vqsr.train_model_snps.py"


rule index_recal:
    input:
        indels="results/20201109-week-46/fetal-genome/vqsr/model.indels.recal",
        snps="results/20201109-week-46/fetal-genome/vqsr/model.snps.recal"
    output:
        indels="results/20201109-week-46/fetal-genome/vqsr/model.indels.recal.idx",
        snps="results/20201109-week-46/fetal-genome/vqsr/model.snps.recal.idx"
    threads: 1
    resources:
        mem_mb=2048
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "vqsr.recal_index.py"


rule vqsr_apply_model:
    input: 
        vcf="results/20201109-week-46/fetal-genome/genotype/joint.chr{chrom}.vcf.gz",
        vcf_tbi="results/20201109-week-46/fetal-genome/genotype/joint.chr{chrom}.vcf.gz.tbi",
        indels_tranches="results/20201109-week-46/fetal-genome/vqsr/model.indels.tranches",
        indels_recal="results/20201109-week-46/fetal-genome/vqsr/model.indels.recal",
        indels_recal_idx="results/20201109-week-46/fetal-genome/vqsr/model.indels.recal.idx",
        snps_tranches="results/20201109-week-46/fetal-genome/vqsr/model.snps.tranches",
        snps_recal="results/20201109-week-46/fetal-genome/vqsr/model.snps.recal",
        snps_recal_idx="results/20201109-week-46/fetal-genome/vqsr/model.snps.recal.idx"
    output: 
        vcf="results/20201109-week-46/fetal-genome/genotype/final_genotype.chr{chrom,[0-9XY]+}.vcf.gz",
        vcf_tbi="results/20201109-week-46/fetal-genome/genotype/final_genotype.chr{chrom}.vcf.gz.tbi",
    log:
        "results/20201109-week-46/fetal-genome/vqsr/final_genotype.chr{chrom}.log"
    threads: 5
    resources:
        mem_mb=10240
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "vqsr.apply_model.py"


rule gather_final_vcf:
    input:
        vcfs=expand("results/20201109-week-46/fetal-genome/genotype/final_genotype.chr{chrom}.vcf.gz", chrom=CHROMS),
        vcfs_tbi=expand("results/20201109-week-46/fetal-genome/genotype/final_genotype.chr{chrom}.vcf.gz.tbi", chrom=CHROMS)
    output:
        vcf="results/20201109-week-46/fetal-genome/genotype/final_genotype.vcf.gz",
        vcf_tbi="results/20201109-week-46/fetal-genome/genotype/final_genotype.vcf.gz.tbi"
    log:
        "results/20201109-week-46/fetal-genome/vqsr/final_genotype.log"
    threads: 4
    resources:
        mem_mb=8192
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "vqsr.gather_final_vcf.py"


rule annotate_vcf:
    input:
        vcf="results/20201109-week-46/fetal-genome/genotype/final_genotype.vcf.gz",
        vcf_tbi="results/20201109-week-46/fetal-genome/genotype/final_genotype.vcf.gz.tbi",
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        dbsnp=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-All.vcf.gz"),
        dbsnp_tbi=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-All.vcf.gz.tbi")
    output:
        vcf="results/20201109-week-46/fetal-genome/genotype/final_genotype.annotated.vcf.gz",
        vcf_tbi="results/20201109-week-46/fetal-genome/genotype/final_genotype.annotated.vcf.gz.tbi"
    log:
        "results/20201109-week-46/fetal-genome/genotype/final_genotype.annotated.log"
    threads: 4
    resources:
        mem_mb=8192
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "vqsr.annotate_vcf.py"


rule final_vcf_metrics:
    input:
        vcf="results/20201109-week-46/fetal-genome/genotype/final_genotype.annotated.vcf.gz",
        vcf_tbi="results/20201109-week-46/fetal-genome/genotype/final_genotype.annotated.vcf.gz.tbi",
        dbsnp=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz"),
        dbsnp_tbi=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz.tbi")
    output:
        details="results/20201109-week-46/fetal-genome/genotype/final_genotype.variant_calling_detail_metrics",
        summary="results/20201109-week-46/fetal-genome/genotype/final_genotype.variant_calling_summary_metrics"
    log:
        "results/20201109-week-46/fetal-genome/genotype/final_genotype.variant_calling_metrics.log"
    threads: 6
    resources:
        mem_mb=12288
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "vqsr.final_vcf_metrics.py"