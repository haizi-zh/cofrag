# This snakemake script applies the VQSR model

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

memory = ""
if "mem_mb" in snakemake.resources.keys():
    # Only allocate 80% of the total memory
    memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.9)))

java_opts = snakemake.params.get("java_opts", "")
if "-Xmx" not in java_opts:
    java_opts = f"{java_opts} {memory} "

extra = snakemake.params.get("extra", "")

with TemporaryDirectory() as tempdir:
    temp_vcf1 = path.join(tempdir, "output.1.vcf.gz")
    temp_vcf2 = path.join(tempdir, "output.2.vcf.gz")
    temp_log = path.join(tempdir, "output.log")

    tranche_values = ["100.0", "99.95", "99.9", "99.5", "99.0", "97.0", "96.0", "95.0", "94.0", "93.5", "93.0", "92.0", "91.0", "90.0"]
    tranche_args = " ".join([f"-tranche {v}" for v in tranche_values])
    annotation_values = ["FS", "ReadPosRankSum", "MQRankSum", "QD", "SOR", "DP"]
    annotation_args = " ".join([f"-an {v}" for v in annotation_values])

    # Apply the indels model
    shell(
        "gatk --java-options '{java_opts}' "
        "ApplyVQSR "
        "{extra} "
        "-O {temp_vcf1} "
        "-V {snakemake.input.vcf} "
        "--recal-file {snakemake.input.indels_recal} "
        "--tranches-file {snakemake.input.indels_tranches} "
        "--use-allele-specific-annotations "
        "--truth-sensitivity-filter-level 99.0 "
        "--create-output-variant-index true "
        "-mode INDEL "
        "2> >(tee -a {temp_log} >&2) "
    )

    # Apply the SNPs model
    shell(
        "gatk --java-options '{java_opts}' "
        "ApplyVQSR "
        "{extra} "
        "-O {temp_vcf2} "
        "-V {temp_vcf1} "
        "--recal-file {snakemake.input.snps_recal} "
        "--tranches-file {snakemake.input.snps_tranches} "
        "--use-allele-specific-annotations "
        "--truth-sensitivity-filter-level 99.7 "
        "--create-output-variant-index true "
        "-mode SNP "
        "2> >(tee -a {temp_log} >&2) "
    )

    # Generate the index
    shell(
        "gatk --java-options \"{memory}\" "
        "IndexFeatureFile "
        "-I {temp_vcf2} "
        "2> >(tee -a {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_vcf2} {snakemake.output.vcf}")
    shell("mv {temp_vcf2}.tbi {snakemake.output.vcf_tbi}")


    # gatk --java-options -Xms5g \
    #   ApplyVQSR \
    #   -O tmp.indel.recalibrated.vcf \
    #   -V ~{input_vcf} \
    #   --recal-file ~{indels_recalibration} \
    #   ~{true='--use-allele-specific-annotations' false='' use_allele_specific_annotations} \
    #   --tranches-file ~{indels_tranches} \
    #   --truth-sensitivity-filter-level ~{indel_filter_level} \
    #   --create-output-variant-index true \
    #   -mode INDEL

    # gatk --java-options -Xms5g \
    #   ApplyVQSR \
    #   -O ~{recalibrated_vcf_filename} \
    #   -V tmp.indel.recalibrated.vcf \
    #   --recal-file ~{snps_recalibration} \
    #   ~{true='--use-allele-specific-annotations' false='' use_allele_specific_annotations} \
    #   --tranches-file ~{snps_tranches} \
    #   --truth-sensitivity-filter-level ~{snp_filter_level} \
    #   --create-output-variant-index true \
    #   -mode SNP
