# This snakemake script jointly generate BQSR reports

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

memory = ""
if "mem_mb" in snakemake.resources.keys():
    # Only allocate 90% of the total memory
    memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.9)))

java_opts = snakemake.params.get("java_opts", "")
if "-Xmx" not in java_opts:
    java_opts = f"{java_opts} {memory} "

extra = snakemake.params.get("extra", "")


with TemporaryDirectory() as tempdir:
    temp_out = path.join(tempdir, "output.table")
    temp_log = path.join(tempdir, "output.log")


#     gatk --java-options "-XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10 -XX:+PrintFlagsFinal \
#       -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -XX:+PrintGCDetails \
#       -Xloggc:gc_log.log -Xms5g" \
#       BaseRecalibrator \
#       -R ~{ref_fasta} \
#       -I ~{input_bam} \
#       --use-original-qualities \
#       -O ~{recalibration_report_filename} \
#       --known-sites ~{dbsnp_vcf} \
#       --known-sites ~{sep=" -known-sites " known_indels_sites_vcfs} \
#       -L ~{sep=" -L " sequence_group_interval}
#   }

    shell(
        "gatk --java-options '{java_opts}' "
        "BaseRecalibrator "
        "{extra} "
        "-O {temp_out} "
        "-R {snakemake.input.ref} "
        "-I {snakemake.input.bam} "
        "-L {snakemake.wildcards.chrom} "
        "-known-sites {snakemake.input.dbsnp} "
        "--use-original-qualities "
        "2> >(tee {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_out} {snakemake.output}")
    shell("mv {temp_log} {snakemake.log}")