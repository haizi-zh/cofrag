# This snakemake script creates an index for the genotype VCF

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

memory = ""
if "mem_mb" in snakemake.resources.keys():
    # Only allocate 80% of the total memory
    memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.9)))

java_opts = snakemake.params.get("java_opts", "")
if "-Xmx" not in java_opts:
    java_opts = f"{java_opts} {memory} "

extra = snakemake.params.get("extra", "")

with TemporaryDirectory() as tempdir:
    temp_snps_idx = path.join(tempdir, "output.snps.idx")
    temp_indels_idx = path.join(tempdir, "output.indels.idx")
    temp_log = path.join(tempdir, "output.log")

    # Generate the index
    shell(
        "gatk --java-options \"{memory}\" "
        "IndexFeatureFile "
        "-I {snakemake.input.snps} "
        "-O {temp_snps_idx} "
        "2> >(tee -a {temp_log} >&2) "
    )
    shell(
        "gatk --java-options \"{memory}\" "
        "IndexFeatureFile "
        "-I {snakemake.input.indels} "
        "-O {temp_indels_idx} "
        "2> >(tee -a {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_indels_idx} {snakemake.output.indels}")
    shell("mv {temp_snps_idx} {snakemake.output.snps}")