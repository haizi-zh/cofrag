# Snakemake script which generates report about the final VCF

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"


from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

# Run fastqc, since there can be race conditions if multiple jobs
# use the same fastqc dir, we create a temp dir.
with TemporaryDirectory() as tempdir:
    memory = ""
    if "mem_mb" in snakemake.resources.keys():
        # Only allocate 80% of the total memory
        memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.8)))

    temp_log = path.join(tempdir, "output.log")

    # Generate the report
    prefix = snakemake.output.details.split("/")[-1].split(".")[0]
    shell(
        "gatk --java-options \"{memory}\" "
        "CollectVariantCallingMetrics "
        "--INPUT {snakemake.input.vcf} "
        "--DBSNP {snakemake.input.dbsnp} "
        "--OUTPUT {prefix} "
        "--THREAD_COUNT {snakemake.threads} "
        "2> >(tee -a {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {prefix}.variant_calling_detail_metrics {snakemake.output.details}")
    shell("mv {prefix}.variant_calling_summary_metrics {snakemake.output.summary}")
    shell("mv {temp_log} {snakemake.log}")