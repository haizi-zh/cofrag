rule add_platform_info:
    input:
        bam="results/20201109-week-46/fetal-genome/bam-chrom/{sample_name}.chr{chrom}.bam",
        bai="results/20201109-week-46/fetal-genome/bam-chrom/{sample_name}.chr{chrom}.bam.bai"
    output:
        bam="results/20201109-week-46/fetal-genome/bam-ready/{sample_name}.chr{chrom}.bam",
        bai="results/20201109-week-46/fetal-genome/bam-ready/{sample_name}.chr{chrom}.bam.bai"
    threads: 2
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/baserecalibrator/environment.yaml"
    script: "bam.processing.rginfo.py"
