# Snakemake script which calls raw variants for each sample using HaplotypeCaller

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"


from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

# Run fastqc, since there can be race conditions if multiple jobs
# use the same fastqc dir, we create a temp dir.
with TemporaryDirectory() as tempdir:
    memory = ""
    if "mem_mb" in snakemake.resources.keys():
        # Only allocate 80% of the total memory
        memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.8)))

    ref = snakemake.input.ref
    temp_gvcf = path.join(tempdir, "output.g.vcf.gz")
    temp_gvcf_tbi = path.join(tempdir, "output.g.vcf.gz.tbi")
    temp_log = path.join(tempdir, "output.log")

    shell(
        "gatk --java-options \"{memory}\" "
        "HaplotypeCaller "
        "--pcr-indel-model NONE "
        "-ERC GVCF "
        "-DF NotDuplicateReadFilter "
        "-G StandardAnnotation -G StandardHCAnnotation -G AS_StandardAnnotation "
        "-GQB 10 -GQB 20 -GQB 30 -GQB 40 -GQB 50 -GQB 60 -GQB 70 -GQB 80 -GQB 90 "
        "-L {snakemake.wildcards.chrom}:{snakemake.wildcards.start}-{snakemake.wildcards.end} "
        "-R {snakemake.input.ref} "
        "-I {snakemake.input.bam} "
        "-O {temp_gvcf} "
        "2> >(tee {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_gvcf} {snakemake.output.gvcf}")
    shell("mv {temp_gvcf_tbi} {snakemake.output.gvcf_tbi}")
    shell("mv {temp_log} {snakemake.output.log}")