def infer_gvcf_aggregates(chrom):
    pattern = f"{BUCKET}/results/20201109-week-46/fetal-genome/haplotype/{{sample_name}}.chr{chrom}:{{region}}.g.vcf.gz"
    sample_name, region = S3.glob_wildcards(pattern)
    n = len(sample_name)
    return [ancient(f"results/20201109-week-46/fetal-genome/haplotype/{sample_name[idx]}.chr{chrom}:{region[idx]}.g.vcf.gz") for idx in range(n)]


# def infer_multisample_gvcf_aggregates(chrom):
#     pattern = f"{BUCKET}/results/20201109-week-46/fetal-genome/genotype/{{sample_name}}.chr{chrom}.g.vcf.gz"
#     sample_name, = S3.glob_wildcards(pattern)
#     # Don't want to check/trigger re-creation once the multisample GVCF file is ready
#     return [ancient(f"results/20201109-week-46/fetal-genome/genotype/{s}.chr{chrom}.g.vcf.gz") for s in sample_name]


rule merge_gvcfs:
    input:
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        gvcfs=lambda wildcards: infer_gvcf_aggregates(wildcards.chrom)
    output:
        "results/20201109-week-46/fetal-genome/haplotype-merged/multi-sample.chr{chrom,[0-9XY]+}.g.vcf.gz",
    log:
        "results/20201109-week-46/fetal-genome/haplotype-merged/multi-sample.chr{chrom}.log"
    resources:
        mem_mb=6000
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/combinegvcfs/environment.yaml"
    script:
        "merge_gvcfs.py"


# rule merge_gvcfs:
#     input:
#         ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
#         ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
#         ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
#         gvcfs=lambda wildcards: infer_gvcf_aggregates(wildcards.sample_name, wildcards.chrom)
#     output:
#         "results/20201109-week-46/fetal-genome/genotype/{sample_name,[^\\.]+}.chr{chrom,[0-9XY]+}.g.vcf.gz",
#     log:
#         "results/20201109-week-46/fetal-genome/genotype/{sample_name}.chr{chrom}.log"
#     resources:
#         mem_mb=4096
#     conda:
#         "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/combinegvcfs/environment.yaml"
#     script:
#         "merge_gvcfs.py"


# rule merge_multisample_gvcfs:
#     input:
#         ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
#         ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
#         ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
#         gvcfs=lambda wildcards: infer_multisample_gvcf_aggregates(wildcards.chrom)
#     output:
#         "results/20201109-week-46/fetal-genome/haplotype-multisample/chr{chrom,[0-9XY]+}.multisample.g.vcf.gz",
#     log:
#         "results/20201109-week-46/fetal-genome/log/genotype/chr{chrom}.multisample.log"
#     resources:
#         mem_mb=4096
#     conda:
#         "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/combinegvcfs/environment.yaml"
#     script:
#         "merge_gvcfs.py" 