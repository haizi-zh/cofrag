rule variant_calling:
    input:
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        bam="results/20201109-week-46/fetal-genome/bqsr/{sample_name}.chr{chrom}.bqsr.bam",
        bai="results/20201109-week-46/fetal-genome/bqsr/{sample_name}.chr{chrom}.bqsr.bam.bai"
    output:
        gvcf="results/20201109-week-46/fetal-genome/haplotype-scatter/{sample_name,[^\\.]+}.chr{chrom,[0-9XY]+}:{start,[0-9]+}-{end,[0-9]+}.g.vcf.gz",
        gvcf_tbi="results/20201109-week-46/fetal-genome/haplotype-scatter/{sample_name}.chr{chrom}:{start}-{end}.g.vcf.gz.tbi",
        log="results/20201109-week-46/fetal-genome/haplotype-scatter/{sample_name}.chr{chrom}.{start}-{end}.log"
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/7f98c5/bio/gatk/haplotypecaller/environment.yaml"
    resources:
        mem_mb=4096
    script:
        "genotype_calling.haplotype.py"


def aggregate_haplotype_scatters(chrom):
    pattern = f"{BUCKET}/results/20201109-week-46/fetal-genome/haplotype-scatter/{{sample_name}}.chr{chrom}:{{region}}.g.vcf.gz"
    sample_name, region = S3.glob_wildcards(pattern)
    n = len(sample_name)
    gvcfs = [f"results/20201109-week-46/fetal-genome/haplotype-scatter/{sample_name[idx]}.chr{chrom}:{region[idx]}.g.vcf.gz" for idx in range(n)]
    gvcfs_tbi = [f"{gvcf}.tbi" for gvcf in gvcfs]
    # Return GVCF files and the corresponding tbi files
    return gvcfs + gvcfs_tbi


rule gather_haplotypes:
    input:
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        gvcfs_all=lambda wildcards: aggregate_haplotype_scatters(wildcards.chrom)
    output:
        gvcf="results/20201109-week-46/fetal-genome/haplotype-gathered/multi-sample.chr{chrom,[0-9XY]+}.g.vcf.gz",
        gvcf_tbi="results/20201109-week-46/fetal-genome/haplotype-gathered/multi-sample.chr{chrom}.g.vcf.gz.tbi",
    log:
        "results/20201109-week-46/fetal-genome/haplotype-gathered/multi-sample.chr{chrom}.log"
    threads: 4
    resources:
        mem_mb=6000
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/combinegvcfs/environment.yaml"
    script:
        "genotype_calling.gather_haplotypes.py"        


rule genotype_gvcfs:
    input:
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        dbsnp=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz"),
        dbsnp_tbi=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz.tbi"),
        gvcf="results/20201109-week-46/fetal-genome/haplotype-gathered/multi-sample.chr{chrom}.g.vcf.gz",
        gvcf_tbi="results/20201109-week-46/fetal-genome/haplotype-gathered/multi-sample.chr{chrom}.g.vcf.gz.tbi"
    output:
        "results/20201109-week-46/fetal-genome/genotype/joint.chr{chrom,[0-9XY]+}.vcf.gz"
    log:
        "results/20201109-week-46/fetal-genome/genotype/joint.chr{chrom}.log"
    threads: 3
    resources:
        mem_mb=6144
    # params:
        # java_opts="-Xmx16G"
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "genotype_calling.genotypegvcfs.py" 


