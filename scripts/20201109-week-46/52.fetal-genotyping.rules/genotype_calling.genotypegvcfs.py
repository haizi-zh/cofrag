# This snakemake script jointly call genotypes from a multi-sample GVCF file

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

memory = ""
if "mem_mb" in snakemake.resources.keys():
    # Only allocate 80% of the total memory
    memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.9)))

java_opts = snakemake.params.get("java_opts", "")
if "-Xmx" not in java_opts:
    java_opts = f"{java_opts} {memory} "

extra = snakemake.params.get("extra", "")

with TemporaryDirectory() as tempdir:
    temp_vcf = path.join(tempdir, "output.vcf.gz")
    temp_log = path.join(tempdir, "output.log")

    shell(
        "gatk --java-options '{java_opts}' "
        "GenotypeGVCFs "
        "{extra} "
        "-R {snakemake.input.ref} "
        "-V {snakemake.input.gvcf} "
        "-O {temp_vcf} "
        "-D {snakemake.input.dbsnp} "
        "-L {snakemake.wildcards.chrom} "
        "--only-output-calls-starting-in-intervals "
        "-G StandardAnnotation -G AS_StandardAnnotation "
        # "--use-new-qual-calculator "  # No needed in GATK 4.1.9.0
        "--merge-input-intervals "
        "2> >(tee {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_vcf} {snakemake.output}")
    shell("mv {temp_log} {snakemake.log}")