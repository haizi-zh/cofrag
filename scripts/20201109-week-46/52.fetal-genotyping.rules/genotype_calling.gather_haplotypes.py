# This snakemake script combines GVCF files for each chromosome togeter

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

memory = ""
if "mem_mb" in snakemake.resources.keys():
    # Only allocate 80% of the total memory
    memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.9)))

java_opts = snakemake.params.get("java_opts", "")
if "-Xmx" not in java_opts:
    java_opts = f"{java_opts} {memory} "

extra = snakemake.params.get("extra", "")

# Separate .tbi files from GVCF files
gvcfs = [item for item in snakemake.input.gvcfs_all if item.endswith(".g.vcf.gz")]
gvcfs = list(map("-V {}".format, gvcfs))


with TemporaryDirectory() as tempdir:
    temp_gvcf = path.join(tempdir, "output.g.vcf.gz")
    temp_log = path.join(tempdir, "output.log")

    shell(
        "gatk --java-options '{java_opts}' "
        "CombineGVCFs "
        "{extra} "
        "{gvcfs} "
        "-R {snakemake.input.ref} "
        "-O {temp_gvcf} "
        "-L {snakemake.wildcards.chrom} "
        "-G StandardAnnotation -G AS_StandardAnnotation "
        "2> >(tee {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_gvcf} {snakemake.output.gvcf}")
    shell("mv {temp_gvcf}.tbi {snakemake.output.gvcf_tbi}")
    shell("mv {temp_log} {snakemake.log}")