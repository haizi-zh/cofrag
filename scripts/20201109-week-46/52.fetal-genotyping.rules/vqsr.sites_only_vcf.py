# Snakemake script which generates a gathered sites-only VCF

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"


from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

# Run fastqc, since there can be race conditions if multiple jobs
# use the same fastqc dir, we create a temp dir.
with TemporaryDirectory() as tempdir:
    memory = ""
    if "mem_mb" in snakemake.resources.keys():
        # Only allocate 80% of the total memory
        memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.8)))

    temp_log = path.join(tempdir, "output.log")

    # Make sites-only VCFs
    for idx in range(len(snakemake.input.vcfs)):
        input_vcf = snakemake.input.vcfs[idx]
        temp_vcf = path.join(tempdir, f"output.{idx}.vcf")

    # for chrom in [str(v) for v in range(1, 23)] + ["X", "Y"]:
        # temp_vcf = path.join(tempdir, f"output.chr{chrom}.vcf")

    #     gatk --java-options -Xms3g \
    #   MakeSitesOnlyVcf \
    #   -I ~{variant_filtered_vcf_filename} \
    #   -O ~{sites_only_vcf_filename}

        shell(
            "gatk --java-options \"{memory}\" "
            "MakeSitesOnlyVcf "
            "-I {input_vcf} "
            "-O {temp_vcf} "
            "2> >(tee -a {temp_log} >&2) "
        )
    
    # Gather them all
    temp_vcfs = [path.join(tempdir, f"output.{idx}.vcf") for idx in range(len(snakemake.input.vcfs))]
    temp_vcfs = " ".join([f"-I {v}" for v in temp_vcfs])
    temp_gathered = path.join(tempdir, "output_gathered.vcf.gz")
    shell(
        "gatk --java-options \"{memory}\" "
        "GatherVcfs "
        # "-I {temp_vcf} "
        "{temp_vcfs} "
        "-O {temp_gathered} "
        "2> >(tee -a {temp_log} >&2) "
    )

    # Generate the index
    shell(
        "gatk --java-options \"{memory}\" "
        "IndexFeatureFile "
        "-I {temp_gathered} "
        "2> >(tee -a {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_gathered} {snakemake.output.vcf}")
    shell("mv {temp_gathered}.tbi {snakemake.output.vcf_tbi}")
    shell("mv {temp_log} {snakemake.log}")