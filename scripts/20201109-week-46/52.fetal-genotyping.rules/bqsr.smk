rule base_recalibrator:
    input:
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        dbsnp=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz"),
        dbsnp_tbi=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz.tbi"),
        bam="results/20201109-week-46/fetal-genome/bam-ready/{sample_name}.chr{chrom}.bam",
        bai="results/20201109-week-46/fetal-genome/bam-ready/{sample_name}.chr{chrom}.bam.bai"
    output:
        "results/20201109-week-46/fetal-genome/bqsr/{sample_name,[^\\.]+}.chr{chrom,[0-9XY]+}.bqsr.table"
    log:
        "results/20201109-week-46/fetal-genome/bqsr/{sample_name}.chr{chrom}.bqsr.table.log"
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/baserecalibrator/environment.yaml"
    threads: 2
    resources:
        mem_mb=lambda wildcards, threads: math.ceil(threads * 2048 * 0.95)
        # mem_mb=lambda wildcards, input: math.ceil(S3.remote(input).size() / 1024**3 * 380)
    params:
        label=lambda wildcards: f"{wildcards.sample_name}.{wildcards.chrom}"
    script:
        "bqsr.base_recalibrator.py"


rule apply_bqsr:
    input:
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        bam="results/20201109-week-46/fetal-genome/bam-ready/{sample_name}.chr{chrom}.bam",
        bai="results/20201109-week-46/fetal-genome/bam-ready/{sample_name}.chr{chrom}.bam.bai",
        bqsr="results/20201109-week-46/fetal-genome/bqsr/{sample_name}.chr{chrom}.bqsr.table"
    output:
        bam="results/20201109-week-46/fetal-genome/bqsr/{sample_name,[^\\.]+}.chr{chrom,[0-9XY]+}.bqsr.bam",
        bai="results/20201109-week-46/fetal-genome/bqsr/{sample_name,[^\\.]+}.chr{chrom,[0-9XY]+}.bqsr.bam.bai"
    log:
        "results/20201109-week-46/fetal-genome/bqsr/{sample_name}.chr{chrom}.bqsr.bam.log"
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/baserecalibrator/environment.yaml"
    threads: 2
    resources:
        mem_mb=lambda wildcards, threads: math.ceil(threads * 2048 * 0.95)
    # resources:
    #     # mem_mb=lambda wildcards, threads: math.ceil(threads * 2048 * 0.95)
    #     mem_mb=lambda wildcards, input: max(math.ceil(S3.remote(input).size() / 1024**3 * 380), 2000)
    params:
        label=lambda wildcards: f"{wildcards.sample_name}.{wildcards.chrom}"
    script:
        "bqsr.apply_bqsr.py"