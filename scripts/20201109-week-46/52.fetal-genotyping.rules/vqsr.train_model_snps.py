# This snakemake script trains the VQSR model for SNPs

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

memory = ""
if "mem_mb" in snakemake.resources.keys():
    # Only allocate 80% of the total memory
    memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.9)))

java_opts = snakemake.params.get("java_opts", "")
if "-Xmx" not in java_opts:
    java_opts = f"{java_opts} {memory} "

extra = snakemake.params.get("extra", "")

with TemporaryDirectory() as tempdir:
    temp_tranches = path.join(tempdir, "snps.tranches")
    temp_recal = path.join(tempdir, "snps.recal")
    temp_log = path.join(tempdir, "output.log")

    tranche_values = ["100.0", "99.95", "99.9", "99.8", "99.6", "99.5", "99.4", "99.3", "99.0", "98.0", "97.0", "90.0" ]
    tranche_args = " ".join([f"-tranche {v}" for v in tranche_values])
    annotation_values = ["QD", "MQRankSum", "ReadPosRankSum", "FS", "MQ", "SOR", "DP"]
    annotation_args = " ".join([f"-an {v}" for v in annotation_values])

    shell(
        "gatk --java-options '{java_opts}' "
        "VariantRecalibrator "
        "{extra} "
        "-O {temp_recal} "
        "-V {snakemake.input.vcf} "
        "--tranches-file {temp_tranches} "
        "--trust-all-polymorphic "
        "{tranche_args} "
        "{annotation_args} "
        "--use-allele-specific-annotations "
        "-mode SNP "
        "--max-gaussians 6 "
        "-resource:hapmap,known=false,training=true,truth=true,prior=15 {snakemake.input.hapmap} "
        "-resource:omni,known=false,training=true,truth=true,prior=12 {snakemake.input.omni} "
        "-resource:1000G,known=false,training=true,truth=false,prior=10 {snakemake.input.g1k} "
        "-resource:dbsnp,known=true,training=false,truth=false,prior=7 {snakemake.input.dbsnp} "
        "2> >(tee {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_tranches} {snakemake.output.tranches}")
    shell("mv {temp_recal} {snakemake.output.recal}")
    shell("mv {temp_log} {snakemake.log}")


    # gatk --java-options -Xms~{java_mem}g \
    #   VariantRecalibrator \
    #   -V ~{sites_only_variant_filtered_vcf} \
    #   -O ~{recalibration_filename} \
    #   --tranches-file ~{tranches_filename} \
    #   --trust-all-polymorphic \
    #   -tranche ~{sep=' -tranche ' recalibration_tranche_values} \
    #   -an ~{sep=' -an ' recalibration_annotation_values} \
    #   ~{true='--use-allele-specific-annotations' false='' use_allele_specific_annotations} \
    #   -mode SNP \
    #    ~{model_report_arg} \
    #   --max-gaussians ~{max_gaussians} \
    #   -resource:hapmap,known=false,training=true,truth=true,prior=15 ~{hapmap_resource_vcf} \
    #   -resource:omni,known=false,training=true,truth=true,prior=12 ~{omni_resource_vcf} \
    #   -resource:1000G,known=false,training=true,truth=false,prior=10 ~{one_thousand_genomes_resource_vcf} \
    #   -resource:dbsnp,known=true,training=false,truth=false,prior=7 ~{dbsnp_resource_vcf}