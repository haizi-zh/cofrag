# This snakemake script trains the VQSR model for indels

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

memory = ""
if "mem_mb" in snakemake.resources.keys():
    # Only allocate 80% of the total memory
    memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.9)))

java_opts = snakemake.params.get("java_opts", "")
if "-Xmx" not in java_opts:
    java_opts = f"{java_opts} {memory} "

extra = snakemake.params.get("extra", "")

with TemporaryDirectory() as tempdir:
    temp_tranches = path.join(tempdir, "indels.tranches")
    temp_recal = path.join(tempdir, "indels.recal")
    temp_log = path.join(tempdir, "output.log")

    tranche_values = ["100.0", "99.95", "99.9", "99.5", "99.0", "97.0", "96.0", "95.0", "94.0", "93.5", "93.0", "92.0", "91.0", "90.0"]
    tranche_args = " ".join([f"-tranche {v}" for v in tranche_values])
    annotation_values = ["FS", "ReadPosRankSum", "MQRankSum", "QD", "SOR", "DP"]
    annotation_args = " ".join([f"-an {v}" for v in annotation_values])

    shell(
        "gatk --java-options '{java_opts}' "
        "VariantRecalibrator "
        "{extra} "
        "-O {temp_recal} "
        "-V {snakemake.input.vcf} "
        "--tranches-file {temp_tranches} "
        "--trust-all-polymorphic "
        "{tranche_args} "
        "{annotation_args} "
        "--use-allele-specific-annotations "
        "-mode INDEL "
        "--max-gaussians 4 "
        "-resource:mills,known=false,training=true,truth=true,prior=12 {snakemake.input.mills} "
        "-resource:axiomPoly,known=false,training=true,truth=false,prior=10 {snakemake.input.axiom_poly} "
        "-resource:dbsnp,known=true,training=false,truth=false,prior=2 {snakemake.input.dbsnp} "
        "2> >(tee {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_tranches} {snakemake.output.tranches}")
    shell("mv {temp_recal} {snakemake.output.recal}")
    shell("mv {temp_log} {snakemake.log}")

    # gatk --java-options -Xms24g \
    #   VariantRecalibrator \
    #   -V ~{sites_only_variant_filtered_vcf} \
    #   -O ~{recalibration_filename} \
    #   --tranches-file ~{tranches_filename} \
    #   --trust-all-polymorphic \
    #   -tranche ~{sep=' -tranche ' recalibration_tranche_values} \
    #   -an ~{sep=' -an ' recalibration_annotation_values} \
    #   ~{true='--use-allele-specific-annotations' false='' use_allele_specific_annotations} \
    #   -mode INDEL \
    #   --max-gaussians ~{max_gaussians} \
    #   -resource:mills,known=false,training=true,truth=true,prior=12 ~{mills_resource_vcf} \
    #   -resource:axiomPoly,known=false,training=true,truth=false,prior=10 ~{axiomPoly_resource_vcf} \
    #   -resource:dbsnp,known=true,training=false,truth=false,prior=2 ~{dbsnp_resource_vcf}