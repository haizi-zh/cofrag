# Snakemake script which infers insert sizes by Picard

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"


from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

# Run fastqc, since there can be race conditions if multiple jobs
# use the same fastqc dir, we create a temp dir.
with TemporaryDirectory() as tempdir:
    memory = ""
    if "mem_mb" in snakemake.resources.keys():
        memory = "-Xmx{}M".format(str(snakemake.resources["mem_mb"]))

    temp_txt = path.join(tempdir, "insert_size.txt")
    temp_pdf = path.join(tempdir, "insert_size.pdf")
    temp_log = path.join(tempdir, "insert_size.log")

    shell(
        "picard CollectInsertSizeMetrics "  # Tool and its subcommand
        "-Dpicard.useLegacyParser=false "
        "{memory} "  # Automatic Xmx java option
        "{snakemake.params} "  # User defined parmeters
        "-I {snakemake.input.bam} "  # Input file
        "-O {temp_txt} "  # Output metrics
        "-H {temp_pdf} "  # Output metrics
        "2> >(tee {temp_log} >&2)"
    )

    # Move outputs into proper position.
    shell("mv {temp_txt} {snakemake.output.insert_size}")
    shell("mv {temp_pdf} {snakemake.output.insert_size_pdf}")
    shell("mv {temp_log} {snakemake.output.insert_size_log}")