# Snakemake script which splits BAM files by chroms

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"


from os import path
from tempfile import TemporaryDirectory

from snakemake.shell import shell

log = snakemake.log_fmt_shell(stdout=False, stderr=True)


# Run fastqc, since there can be race conditions if multiple jobs
# use the same fastqc dir, we create a temp dir.
with TemporaryDirectory() as tempdir:
    chroms = [str(v) for v in range(1, 23)] + ["X", "Y"]
    bam_input = snakemake.input.bam
    # breakpoint()
    for chr_idx in range(len(chroms)):
        bam_output = path.join(tempdir, f"temp.chr{chroms[chr_idx]}.bam")
        shell(f"samtools view -b {bam_input} {chroms[chr_idx]} > {bam_output}")

    # Move outputs into proper position.
    for chr_idx in range(len(chroms)):
        bam_output = path.join(tempdir, f"temp.chr{chroms[chr_idx]}.bam")
        shell(f"mv {bam_output} {snakemake.output.bam[chr_idx]}")
