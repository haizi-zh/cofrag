# Snakemake script which infers GC bias by Picard

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"


from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

# Run fastqc, since there can be race conditions if multiple jobs
# use the same fastqc dir, we create a temp dir.
with TemporaryDirectory() as tempdir:
    memory = ""
    if "mem_mb" in snakemake.resources.keys():
        memory = "-Xmx{}M".format(str(snakemake.resources["mem_mb"]))

    temp_txt = path.join(tempdir, "gc_bias.txt")
    temp_chart = path.join(tempdir, "gc_bias.pdf")
    temp_summary = path.join(tempdir, "gc_bias.summary.txt")
    temp_log = path.join(tempdir, "gc_bias.log")

    shell(
        "picard CollectGcBiasMetrics "  # Tool and its subcommand
        "-Dpicard.useLegacyParser=false "
        "{memory} "  # Automatic Xmx java option
        "{snakemake.params} "  # User defined parmeters
        "-I {snakemake.input.bam} "  # Input file
        "-R {snakemake.input.ref} "  # ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/technical/reference/human_g1k_v37.fasta.gz
        "-O {temp_txt} "  # Output metrics
        "-CHART {temp_chart} "  # Output metrics
        "-S {temp_summary} "  # Output metrics
        "2> >(tee {temp_log} >&2)"
    )

    # Move outputs into proper position.
    shell("mv {temp_txt} {snakemake.output.gc_bias}")
    shell("mv {temp_chart} {snakemake.output.gc_bias_chart}")
    shell("mv {temp_summary} {snakemake.output.gc_bias_summary}")
    shell("mv {temp_log} {snakemake.output.gc_bias_log}")