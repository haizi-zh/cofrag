# This snakemake script combines GVCF files for each chromosome togeter

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"


from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

# Run fastqc, since there can be race conditions if multiple jobs
# use the same fastqc dir, we create a temp dir.
with TemporaryDirectory() as tempdir:
    memory = ""
    if "mem_mb" in snakemake.resources.keys():
        # Only allocate 80% of the total memory
        memory = "-Xmx{}M".format(str(int(int(snakemake.resources["mem_mb"]) * 0.9)))

    temp_gvcf = path.join(tempdir, "output.g.vcf.gz")
    temp_log = path.join(tempdir, "output.log")

    shell(
        "gatk --java-options \"{memory}\" "
        "MergeVcfs "
        "-I {snakemake.input} "
        "-O {temp_gvcf} "
        "2> >(tee {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_gvcf} {snakemake.output.gvcf}")
    shell("mv {temp_log} {snakemake.output.log}")