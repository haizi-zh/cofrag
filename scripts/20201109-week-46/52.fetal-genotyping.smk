from snakemake.remote.S3 import RemoteProvider as S3RemoteProvider
S3 = S3RemoteProvider(enable_cache=True, cache_ttl=120)
from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider
FTP = FTPRemoteProvider()
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
HTTP = HTTPRemoteProvider(enable_cache=True, cache_ttl=90)

import math

CHROMS = [str(v) for v in range(1, 23)] + ["X", "Y"]

BUCKET = "cofrag2"

rule fastqc:
    input:
        "data/chan2016pnas/{sample}.hg19.mdups.bam"
    output:
        html="results/20201109-week-46/fetal-genome/fastqc/{sample}.html",
        zip="results/20201109-week-46/fetal-genome/fastqc/{sample}_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc to find the file. If not using multiqc, you are free to choose an arbitrary filename
    params: ""
    log:
        "logs/fastqc/{sample}.log"
    threads: 5
    resources:
        mem_mb=2048,
        disk_mb=61440
    wrapper:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/c8ce87/bio/fastqc"


rule raw_bam_index:
    input:
        "data/chan2016pnas/{sample}.hg19.mdups.bam"
    output:
        "data/chan2016pnas/{sample}.hg19.mdups.bam.bai"
    params: ""
    log:
        "logs/raw_bam_index/{sample}.log"
    threads: 1
    resources:
        mem_mb=2048,
        disk_mb=61440
    wrapper:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/c8ce87/bio/samtools/index"


rule raw_bam_split:
    input:
        bam="data/chan2016pnas/{sample}.hg19.mdups.bam",
        bai="data/chan2016pnas/{sample}.hg19.mdups.bam.bai"
    output:
        bam=expand("results/20201109-week-46/fetal-genome/bam/{{sample}}.chr{chr}.bam", chr=CHROMS)
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/c8ce87/bio/samtools/view/environment.yaml"
    script:
        "52.fetal-genotyping.bam_chr.py"


def infer_sample_aggregates(sample_name, chrom):
    pattern = f"results/20201109-week-46/fetal-genome/bam/{sample_name}_{{lane_info}}.chr{chrom}.bam"
    lane_info, = S3.glob_wildcards(f"{BUCKET}/{pattern}")
    return [ancient(f"results/20201109-week-46/fetal-genome/bam/{sample_name}_{l}.chr{chrom}.bam") for l in lane_info]


rule raw_bam_rearrange:
    input:
        lambda wildcards: infer_sample_aggregates(wildcards.sample_name, wildcards.chrom)
    output:
        bam="results/20201109-week-46/fetal-genome/bam-chrom/{sample_name}.chr{chrom}.bam",
        bai="results/20201109-week-46/fetal-genome/bam-chrom/{sample_name}.chr{chrom}.bam.bai"
    threads: 2
    params:
        k8s_node_selector={"diskvol": "low"},
    resources:
        mem_mb=2048,
        disk_mb=61440
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/c8ce87/bio/samtools/view/environment.yaml"
    shell:
        """
        exit 1
        TEMPDIR=$(mktemp -d)

        samtools merge -rcp --threads 2 $TEMPDIR/output.bam {input}
        samtools index $TEMPDIR/output.bam

        mv $TEMPDIR/output.bam {output.bam}
        mv $TEMPDIR/output.bam.bai {output.bai}
        """


rule picard_insert_size:
    input:
        bam="results/20201109-week-46/fetal-genome/bam-filtered/{sample_name}.chr{chrom}.bam",
        bai="results/20201109-week-46/fetal-genome/bam-filtered/{sample_name}.chr{chrom}.bam.bai"
    output: 
        insert_size="results/20201109-week-46/fetal-genome/picard-metrics/insert_size/{sample_name}.chr{chrom}.insert_size.txt",
        insert_size_pdf="results/20201109-week-46/fetal-genome/picard-metrics/insert_size/{sample_name}.chr{chrom}.insert_size.pdf",
        insert_size_log="results/20201109-week-46/fetal-genome/picard-metrics/insert_size/{sample_name}.chr{chrom}.insert_size.log",
    threads: 2
    resources:
        mem_mb=2048,
        disk_mb=61440
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/c8ce87/bio/picard/markduplicates/environment.yaml"
    script:
        "52.fetal-genotyping.picard_insert_size.py"


rule picard_gc_bias:
    input:
        bam="results/20201109-week-46/fetal-genome/bam-filtered/{sample_name}.chr{chrom}.bam",
        bai="results/20201109-week-46/fetal-genome/bam-filtered/{sample_name}.chr{chrom}.bam.bai",
        ref=FTP.remote("ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/technical/reference/human_g1k_v37.fasta.gz")
    output: 
        gc_bias="results/20201109-week-46/fetal-genome/picard-metrics/gc_bias/{sample_name}.chr{chrom}.gc_bias.txt",
        gc_bias_chart="results/20201109-week-46/fetal-genome/picard-metrics/gc_bias/{sample_name}.chr{chrom}.gc_bias.pdf",
        gc_bias_summary="results/20201109-week-46/fetal-genome/picard-metrics/gc_bias/{sample_name}.chr{chrom}.gc_bias.summary.txt",
        gc_bias_log="results/20201109-week-46/fetal-genome/picard-metrics/gc_bias/{sample_name}.chr{chrom}.gc_bias.log",
    threads: 2
    resources:
        mem_mb=2048,
        disk_mb=61440
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/c8ce87/bio/picard/markduplicates/environment.yaml"
    script:
        "52.fetal-genotyping.picard_gc_bias.py"


rule filter_bam:
    input:
        "results/20201109-week-46/fetal-genome/bam-chrom/{sample_name}.chr{chrom}.bam",
    output:
        bam="results/20201109-week-46/fetal-genome/bam-filtered/{sample_name}.chr{chrom}.bam",
        bai="results/20201109-week-46/fetal-genome/bam-filtered/{sample_name}.chr{chrom}.bam.bai",
    threads: 2
    resources:
        mem_mb=2048,
        disk_mb=61440
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/c8ce87/bio/samtools/view/environment.yaml"
    shell:
        """
        TEMPDIR=$(mktemp -d)

        samtools view -q 30 -f 3 -F 3852 -b --threads 2 {input} > $TEMPDIR/output.bam
        samtools index $TEMPDIR/output.bam

        mv $TEMPDIR/output.bam {output.bam}
        mv $TEMPDIR/output.bam.bai {output.bai}
        """



## Modules
include: "52.fetal-genotyping.rules/bam.processing.smk"
include: "52.fetal-genotyping.rules/bqsr.smk"
include: "52.fetal-genotyping.rules/genotype_calling.smk"
include: "52.fetal-genotyping.rules/vqsr.smk"
