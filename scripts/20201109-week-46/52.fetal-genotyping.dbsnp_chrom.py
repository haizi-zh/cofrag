# This snakemake script splits the dbsnp vcf file into pieces, one for each chromosome

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"


from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell

# Run fastqc, since there can be race conditions if multiple jobs
# use the same fastqc dir, we create a temp dir.
with TemporaryDirectory() as tempdir:
    temp_vcf = path.join(tempdir, "output.vcf.gz")

    shell("tabix ")

    shell(
        "gatk --java-options \"{memory}\" "
        "HaplotypeCaller "
        "--pcr-indel-model NONE -ERC GVCF "
        "-DF NotDuplicateReadFilter "
        "-L {snakemake.wildcards.chrom}:{snakemake.wildcards.start}-{snakemake.wildcards.end} "
        "-R {snakemake.input.ref} "
        "-I {snakemake.input.bam} "
        "-O {temp_gvcf} "
        "2> >(tee {temp_log} >&2) "
    )

    # Move outputs into proper position.
    shell("mv {temp_gvcf} {snakemake.output.gvcf}")
    shell("mv {temp_log} {snakemake.output.log}")