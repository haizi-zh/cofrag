library(tidyverse)
library(logging)

args <- commandArgs(trailingOnly = TRUE)
distribution <- args[1]
n_trees <- as.integer(args[2])
loginfo(str_interp("Distribution: ${distribution}, # of iterations: ${n_trees}k"))


working_dir <- getwd()
setwd("./dev/")
library(here)
setwd(working_dir)

source(here("src/contact_matrix.R"))
source(here("src/utils.R"))

# frags_ih02 <- read_tsv(
#   gzfile(
#     here(
#       "../results/20200824-week-35/ih02.hg19.chr14.filtered.frag.len100_350.tsv.gz"
#     )
#   ),
#   col_names = c("chr", "start", "end", "mapq"),
#   col_types = cols(col_factor(), col_integer(), col_integer(), col_integer())
# ) %>%
#   mutate(length = end - start)

frags <- read_tsv(
  gzfile(
    here(
      "../results/20200824-week-35/bh01.hg19.chr14.filtered.frag.len100_350.tsv.gz"
    )
  ),
  col_names = c("chr", "start", "end", "mapq"),
  col_types = cols(col_factor(), col_integer(), col_integer(), col_integer())
) %>%
  mutate(length = end - start)

gene_density <- read_tsv(
  gzfile(here("../data/gene_density/gencode.v30.b37.gene_density.bedgraph.gz")),
  col_names = c("chr", "start", "end", "count"),
  col_types = cols(col_character(), col_integer(), col_integer(), col_integer()))

bin_size <- 500e3L
gr <- infer_genomic_range(frags, bin_size = bin_size)
bfp <- calc_bfp(frags, gr = gr, bin_size = bin_size) %>%
  mutate(
    bin_idx = bin_start %/% bin_size,
    cov = frag %>% map_int(~ length(.[[1]]))
  )

wbc_rep1 <- load_hic_obs(here(
  "../results/20200824-week-35/wbc.rep1.chr14.500kb.oe.none.txt"
), chr = "14") %>%
  mutate(bin_idx1 = start1 %/% bin_size, bin_idx2 = start2 %/% bin_size)

# comp_rep1 <- wbc_rep1 %>% call_compartments(gene_density = gene_density)

cm_cucconi <- load_genomic_matrix(
  gzfile("./results/20200824-week-35/cm.cucconi.ss15k.chr14.bed.gz"), 
  additional_col_names = c("score2", "pvalue", "bootstrap")
) %>%
  filter(bootstrap == 1) %>%
  select(-bootstrap, -score2) %>%
  mutate(
    bin_idx1 = start1 %/% bin_size,
    bin_idx2 = start2 %/% bin_size,
    bin_dist = abs(bin_idx1 - bin_idx2),
    cov1 = bfp$cov[bin_idx1 - bfp$bin_idx[1] + 1],
    cov2 = bfp$cov[bin_idx2 - bfp$bin_idx[1] + 1]
  )

y_scores <- 1:nrow(cm_cucconi) %>% map_dbl(~ {
  row_idx <- .x
  i <- cm_cucconi[row_idx,]$bin_idx1
  j <- cm_cucconi[row_idx,]$bin_idx2
  wbc_rep1 %>% filter(bin_idx1 == i & bin_idx2 == j) %>% .$score %>% .[1]
})

cm_cucconi <- cm_cucconi %>% 
  mutate(y = y_scores) %>%
  filter(!is.na(y))

train_dataset <- cm_cucconi %>% 
  as_tibble() %>%
  select(y, bin_dist, cov1, cov2, score)

model_gbm <- gbm::gbm(
  y ~ .,
  data = train_dataset,
  verbose = TRUE,
  distribution = distribution,
  shrinkage = 0.001,
  n.trees = n_trees * 1000,
  interaction.depth = 2,
  cv.folds = 10,
  n.cores = 28,
  keep.data = TRUE
)

save(
  model_gbm,
  file = str_interp(
    "./results/20200824-week-35/gbm/chr14.cucconi.ss15k.gbm.${distribution}.${n_trees}k.RData"))