
working_dir <- getwd()
setwd("./dev/")
library(here)
setwd(working_dir)

library(tidyverse)
library(logging)

args <- get0("script_args")
if (is.null(args)) {
  args <- commandArgs(trailingOnly = TRUE)
}
ss <- as.integer(args[1])
metrics <- args[2]
ncores <- as.integer(args[3])
bootstrap <- as.integer(args[4])
seed <- as.integer(args[5])
chr <- args[6]
output_file <- args[7]

# load(here("../lab_notebook/journal-20200826-week-35.RData"))

# loginfo(args)

data_file <- switch(chr,
                    # chr14 = here("../results/20200824-week-35/bh01.hg19.chr14.filtered.frag.len100_350.tsv.gz"),
                    chr14 = here("../results/20200824-week-35/ih02.hg19.chr14.filtered.frag.len100_350.tsv.gz"),
                    # chr21 = here("../results/20200817-week-34/bh01.hg19.chr21.len100-350.frag.tsv.gz"),
                    # chr22 = here("../results/20200817-week-34/bh01.hg19.chr22.len100-350.frag.tsv.gz"),
                    stop(str_interp("Invalid ${chr}")))

tmp <- read_tsv(
  gzfile(data_file),
  col_names = c("chr", "start", "end", "mapq"),
  col_types = cols(col_factor(), col_integer(), col_integer(), col_integer())
) 
frags <- list(
  chr = tmp$chr, start = tmp$start, end = tmp$end, length = tmp$end - tmp$start) %>% 
  as_tibble()
rm(tmp)

source(here("src/contact_matrix.R"), local = TRUE)
cm <- call_contact_matrix(
  frags,
  rng_seed = seed,
  ncores = ncores, 
  metrics = metrics, 
  bootstrap = bootstrap, 
  subsample = ss)

dump_genomic_matrix(cm, conn = gzfile(output_file))