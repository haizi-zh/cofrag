rule vcf_conform:
    input: 
        gt="results/20201012-week-42/genotype/{sample}/{sample}.chr{chr}.vcf.gz",
        ref="data/genotype/1kg.phase3.v5a.20130502.genotypes/dedup/ALL.chr{chr}.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.dedup.vcf.gz"
    output: "results/20201019-week-43/genotype/{sample}/{sample}.chr{chr}.mod.vcf.gz"
    shell:
        """
        java -jar /home/ubuntu/tools/conform-gt.24May16.cee.jar match=POS \
            ref={input.ref} \
            gt={input.gt} \
            chrom={wildcards.chr} \
            out=results/20201019-week-43/genotype/{wildcards.sample}/{wildcards.sample}.chr{wildcards.chr}.mod.vcf.gz \
            excludesamples=data/genotype/integrated_call_samples_v3.20130502.EAS.excluded
        """

rule vcf_index:
    input: "results/20201019-week-43/genotype/{sample}/{sample}.chr{chr}.mod.vcf.gz"
    output: "results/20201019-week-43/genotype/{sample}/{sample}.chr{chr}.mod.vcf.gz.csi"
    shell:
        """
        bcftools index {input}
        """
        
        
rule vcf_check:
    input: "results/20201019-week-43/genotype/{sample}/{sample}.chr{chr}.mod.vcf.gz"
    output: "results/20201019-week-43/genotype/{sample}/vcf_check/{sample}.chr{chr}.check.log"
    params:
        label=lambda wildcards: f"{wildcards.sample}.chr{wildcards.chr}",
        output_prefix=lambda wildcards, output: str(output)[:-10]
    shell:
        """
         python2 $COFRAG/scripts/20201019-week-43/checkVCF.py \
         -r $COFRAG/data/ref_genome/hs37-1kg/human_g1k_v37.fasta \
         -o $COFRAG/{params.output_prefix} \
         {input}
        """