rule bamtobed:
    input: 
        bam="results/20201019-week-43/genotype/sun2019gr/{entry_id}.hg19.mdups.bam",
        bai="results/20201019-week-43/genotype/sun2019gr/{entry_id}.hg19.mdups.bam.bai"
    output: 
        bed="results/20201019-week-43/genotype/sun2019gr/{entry_id}.chr{chr,[0-9]+}.bed.gz",
        tbi="results/20201019-week-43/genotype/sun2019gr/{entry_id}.chr{chr}.bed.gz.tbi"
    shell:
        """
        samtools view -f 3 -F 3852 -q 30 -h {input.bam} {wildcards.chr} | 
        samtools sort -n -O SAM -o - | 
        bedtools bamtobed -bedpe -mate1 -i - | 
        bedtools sort -i - | 
        bgzip > {output.bed}
        tabix -0 -p bed {output.bed}
        """


rule bed_with_seq:
    input:
        bed="results/20201019-week-43/genotype/sun2019gr/{entry_id}.chr{chr}.bed.gz",
        tbi="results/20201019-week-43/genotype/sun2019gr/{entry_id}.chr{chr}.bed.gz.tbi",
        bam="results/20201019-week-43/genotype/sun2019gr/{entry_id}.hg19.mdups.bam",
        bai="results/20201019-week-43/genotype/sun2019gr/{entry_id}.hg19.mdups.bam.bai"
    output:
        bed="results/20201019-week-43/genotype/sun2019gr/{entry_id}.chr{chr,[0-9]+}.seq.bed.gz",
        tbi="results/20201019-week-43/genotype/sun2019gr/{entry_id}.chr{chr}.seq.bed.gz.tbi"
    shell:
        """
        Rscript --vanilla $COFRAG/scripts/20201019-week-43/47.build.bedpe.seq.R \
        {input.bed} {input.bam} {output.bed}.tmp.gz
        zcat {output.bed}.tmp.gz | bgzip > {output.bed}
        rm {output.bed}.tmp.gz
        tabix -0 -p bed {output.bed}
        """

