# This script generates a simulation BAM file

source("./src/calc_distance.R")

number_of_reads <- 4705827
gr_start <- 9411250
gr_end <- 48119700

aligned_reads <-
  list(pos = round(sort(runif(number_of_reads, gr_start, gr_end))), isize = round(runif(number_of_reads, 50, 500)))
dm <-
  calc_distance_helper(
    aligned_reads,
    GRanges(sprintf("%s:%d-%d", 21, 9000001, 48500000)),
    bin_size = 25000,
    block_size = 5000000,
    nthreads = 8,
    opts = list(min_samples = 100, max_frag_size = 500)
  )