#!/bin/sh
#SBATCH -N 1
#SBATCH -C EGRESS
#SBATCH -p RM-shared
#SBATCH --job-name BH01-sam-dump
#SBATCH -t 12:00:00
#SBATCH --ntasks-per-node 1
#SBATCH --mail-type ALL
#SBATCH --mail-user haizi.zheng@cchmc.org
#SBATCH --output /home/haizizh/workspace/slurm_output/slurm-%j-%u-%x.out
#SBATCH --error /home/haizizh/workspace/slurm_output/slurm-%j-%u-%x.out

set -x

sam-dump -r --gzip --output-file SRR2129993.sam.gz SRR2129993
