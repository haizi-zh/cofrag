#!/bin/sh
#SBATCH -N 1
#SBATCH -C EGRESS
#SBATCH -p RM-shared
#SBATCH --job-name BH01-recalculate-mappability
#SBATCH -t 12:00:00
#SBATCH --ntasks-per-node 28
#SBATCH --mail-type ALL
#SBATCH --mail-user haizi.zheng@cchmc.org
#SBATCH --output /home/haizizh/workspace/slurm_output/slurm-%j-%u-%x.out
#SBATCH --error /home/haizizh/workspace/slurm_output/slurm-%j-%u-%x.out

set -x

NCORES=28
PREF=human_g1k_v37
KMER=45

# gem-indexer -T $NCORES -c dna -i ../$PREF.fasta -o $PREF.index
gem-mappability -T $NCORES -I $PREF.index.gem -l $KMER -o $PREF.${KMER}mer
gem-2-wig -I $PREF.index.gem -i $PREF.${KMER}mer.mappability -o $PREF.${KMER}mer
wigToBigWig $PREF.${KMER}mer.wig $PREF.sizes $PREF.${KMER}mer.bw

# Split the whole-genome bigwig into segments by chromosome, using commands similar to the following:
wig2bed < $PREF.${KMER}mer.wig | cut -f1-3,5 > $PREF.${KMER}mer.bedgraph

