#!/bin/sh
#SBATCH -N 1
#SBATCH -C EGRESS
#SBATCH -p RM-shared
#SBATCH --job-name recalculate-mappability
#SBATCH -t 12:00:00
#SBATCH --ntasks-per-node 28
#SBATCH --mail-type ALL
#SBATCH --mail-user haizi.zheng@cchmc.org
#SBATCH --output /home/haizizh/workspace/slurm_output/slurm-%j-%u-%x.out
#SBATCH --error /home/haizizh/workspace/slurm_output/slurm-%j-%u-%x.out

set -x

gem-indexer -T 28 -c dna -i ./human_g1k_v37.fasta -o human_g1k_v37.index
