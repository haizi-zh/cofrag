# This script analyze fragment sizes from selected BAM files

library(tidyverse)

isize_file <- "./stash/BH01.chr22.filtered.isize.txt"
frags <- read_delim(isize_file, "\t", col_names = c("pos", "tlen"), skip_empty_rows = T)
frags$isize <- abs(frags$tlen)
# Large fragments are those with isize greater than 1000
large_frags <- frags[frags$isize > 1000, ]
# Small fragments are those with isize less than 500
small_frags <- frags[frags$isize < 500, ]

summary(frags$isize)

# fragments over genome
qplot(large_frags$pos, large_frags$isize/1000, log = "y", main = "chr22: fragments (> 1000 kb) over genome", xlab = "coordinates", ylab = "fragment size (kb)")
# small fragments size distribution
ggplot(small_frags, aes(x = isize)) + geom_histogram(bins = 90) + ggtitle("small fragments size distribution") + xlab("fragment size")

