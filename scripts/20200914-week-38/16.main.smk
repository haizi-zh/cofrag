rule get_mappability:
    input: "data/mappability/wgEncodeDukeMapabilityUniqueness35bp.bigWig"
    output: "data/mappability/wgEncodeDukeMapabilityUniqueness35bp.{chr,chr.+}.lt0_5.bed.gz"
    shell:
        """
        bigWigToBedGraph -chrom={wildcards.chr} {input} /dev/stdout |
            awk '$4<=0.5 {{print substr($0, 4)}}' |
            bgzip > {output}
        """

rule get_frag:
    singularity: "docker://zephyre/comp-bio:v0.3.7"
    input: 
        frag="/pylon5/mc5plcp/haizizh/finaledb_data/frag/{entry_id}.hg19.frag.gz",
        mappability="data/mappability/wgEncodeDukeMapabilityUniqueness35bp.chr{chr}.lt0_5.bed.gz"
    output:
        # frag="results/{results_subdir}/{entry_id,EE[0-9]+}/{entry_id}.hg19.chr{chr,.+}.filtered.frag.len100_350.tsv.gz",
        # index="results/{results_subdir}/{entry_id,EE[0-9]+}/{entry_id}.hg19.chr{chr,.+}.filtered.frag.len100_350.tsv.gz.tbi",
        frag="results/{results_subdir}/{entry_id}.hg19.chr{chr,.+}.filtered.frag.len100_350.tsv.gz",
        index="results/{results_subdir}/{entry_id}.hg19.chr{chr,.+}.filtered.frag.len100_350.tsv.gz.tbi",
    threads: 1
    resources:
        cpus=lambda wildcards, threads: threads,
        mem_mb=lambda wildcards, threads: threads * 4200,
        time_min=90
    params:
        label=lambda wildcards: f"{wildcards.entry_id}.chr{wildcards.chr}",
        partition="RM-shared"
    shell:
        """
        LOCAL=/local/
        
        echo `date +"%F %T %Z"` "Extracting fragments..."
        tabix {input.frag} {wildcards.chr} |
            awk '$4>=30 && $3-$2<=350 && $3-$2>=100' | 
            bedtools subtract -A -a - -b data/duke_excluded_regions/wgEncodeDacMapabilityConsensusExcludable.chr{wildcards.chr}.bed |
            bedtools subtract -A -a - -b {input.mappability} -f 0.1 |
            bgzip > $LOCAL/frag.{params.label}.gz
        echo `date +"%F %T %Z"` "Indexing fragments..."
        tabix -0 -p bed $LOCAL/frag.{params.label}.gz
        
        echo `date +"%F %T %Z"` "Copying results to target directory..."
        mv $LOCAL/frag.{params.label}.gz {output.frag}.tmp
        mv $LOCAL/frag.{params.label}.gz.tbi {output.index}.tmp
        mv {output.frag}.tmp {output.frag}
        mv {output.index}.tmp {output.index}
        """ 


def determine_time_min(chr):
    chr = int(chr)
    if chr <= 4:
        return 360
    elif chr <= 8:
        return 240
    elif chr <= 12:
        return 180
    elif chr <= 16:
        return 120
    else:
        return 90


def determine_ncores(chr):
    chr = int(chr)
    if chr <= 4:
        return 8
    elif chr <= 8:
        return 12
    elif chr <= 12:
        return 16
    else:
        return 28


rule calc_cm:
    input: 
        "results/{results_subdir}/frags/{entry_id}.hg19.chr{chr}.filtered.frag.len100_350.tsv.gz"
    output: 
        "results/{results_subdir}/cm/{entry_id,EE[0-9]+}.cm.{metrics}.ss{ss,[0-9]+}k.chr{chr,.+}.seed{seed}.bed.gz"
    threads: 28
    resources:
        cpus=lambda wildcards, threads: threads,
        mem_mb=lambda wildcards, threads: threads * 4200,
        time_min=lambda wildcards: determine_time_min(wildcards.chr)
    params:
        label=lambda wildcards: f"{wildcards.entry_id}.chr{wildcards.chr}.ss{wildcards.ss}k.{wildcards.metrics}",
        partition="RM",
        ncores=lambda wildcards: determine_ncores(wildcards.chr)
    shell:
        """
        R_LIBS=/home/haizizh/Rpackages/4.0/ /opt/packages/R/4.0.0-mkl/bin/Rscript --vanilla \
            scripts/20200914-week-38/15.calc.contact_matrix.R {wildcards.ss} {wildcards.metrics} 16 5 {wildcards.seed} {input} $LOCAL/cm.{params.label}.bed.gz

        mv $LOCAL/cm.{params.label}.bed.gz {output}.tmp
        mv {output}.tmp {output}
        """

rule gc_corr_cov:
    input: 
        frag="results/20200914-week-38/training/frags/{entry_id}.hg19.chr{chr}.filtered.frag.len100_350.tsv.gz",
        gc_bias="results/20200914-week-38/training/gc/{entry_id}.hg19.gc_bias.txt",
        gc_content="results/20200914-week-38/gc/gc_content.chr{chr}.50kb.bed.gz"
    output: 
        "results/20200914-week-38/training/gc/{entry_id,EE[0-9]+}.chr{chr}.gc_corrected.bed.gz"
    threads: 1
    resources:
        cpus=lambda wildcards, threads: threads,
        mem_mb=lambda wildcards, threads: threads * 4200,
        time_min=30
    params:
        label=lambda wildcards: f"{wildcards.entry_id}.chr{wildcards.chr}",
        partition="RM-shared"
    shell:
        """
        R_LIBS=/home/haizizh/Rpackages/4.0/ /opt/packages/R/4.0.0-mkl/bin/Rscript --vanilla \
            scripts/20200914-week-38/17.cov.gc_correction.R {wildcards.chr} 50000 500000 {input.frag} {input.gc_bias} {output}
        """
