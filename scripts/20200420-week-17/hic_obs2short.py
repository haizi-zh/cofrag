#!/usr/bin/bash python

# Convert the "hic_obs.txt" format to pre/short format 
# (https://github.com/aidenlab/juicer/wiki/Pre)
# Usage: python hic_obs2short.py chr_name stdin stdout

import sys, re

chr_name = sys.argv[1]

for line in sys.stdin:
    parts = re.split(r"[\t ]+", line.strip())
    assert(len(parts) == 3)

    pos_1 = int(parts[0])
    pos_2 = int(parts[1])
    score = float(parts[2])

    # Short with score format
    # <str1> <chr1> <pos1> <frag1> <str2> <chr2> <pos2> <frag2> <score>
    print(f"0 {chr_name} {pos_1} 0 0 {chr_name} {pos_2} 1 {score}")