from snakemake.remote.S3 import RemoteProvider as S3RemoteProvider
S3 = S3RemoteProvider()

envvars:
    "AWS_ACCESS_KEY_ID",
    "AWS_SECRET_ACCESS_KEY"

# >>> Configuration >>>
OUTPUT_DIR = "results/20200420-week-17/compartment-analysis"
FULL_CORES = 15
PART_CORES = 5
METRICS = config["METRICS"].split(",") if "METRICS" in config else ["ks", "cucconi"]
NORM = config["NORM"].split(",") if "NORM" in config else ["none", "geometric", "harmonic"]
CHROM = config["CHROM"] if "CHROM" in config else []
BIN_SIZE = config["BIN_SIZE"] if "BIN_SIZE" in config else [500]
# <<< Configuration <<<

rule all:
    input:
        expand("%s/dm.chr{chrom}.{bin_size}kb.{metrics}.{norm}.txt.gz" % OUTPUT_DIR, \
            chrom=CHROM, bin_size=BIN_SIZE, metrics=METRICS, norm=NORM)

rule distance_matrix:
    input:
        "results/20200406-week-15/bam_filter/bh01.chr{chrom}.filtered.bam",
        "results/20200406-week-15/bam_filter/bh01.chr{chrom}.filtered.bam.bai"
    output:
        "%s/dm.chr{chrom}.{bin_size}kb.{metrics}.{norm}.txt.gz" % OUTPUT_DIR
    threads: FULL_CORES
    shell:
        """
        Rscript src/cfhic.R distance -m {wildcards.metrics} -s {wildcards.bin_size}000 -b 1000000 -o {output} -n {threads} {input[0]}
        """