# FASTQC reports generated from the pipeline come with common a sample name:
# merged. Need to change it to the corresponding actual sample names, such as
# C12148W.

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

import os
import zipfile
import re
import shutil
import subprocess as sp

base_dir = os.getenv("COFRAG")
fastqc_dir = os.path.join(
    base_dir, "results/20201116-week-47/trio-variant-calling/fastqc"
)
fastqc_dir_adjusted = os.path.join(fastqc_dir, "adjusted")
os.mkdir(fastqc_dir_adjusted)

for zipped_fastqc in [item for item in os.listdir(fastqc_dir) if item.endswith(".zip")]:
    m = re.match("([^\\.]+)_fastqc.zip", zipped_fastqc)
    sample = m.groups()[0]

    zip_file = os.path.join(fastqc_dir, zipped_fastqc)
    sample_dir = os.path.join(fastqc_dir_adjusted, f"{sample}_fastqc")

    with zipfile.ZipFile(zip_file, "r") as zip_ref:
        zip_ref.extractall(fastqc_dir_adjusted)
        shutil.move(os.path.join(fastqc_dir_adjusted, "merged_fastqc"), sample_dir)

    sp.Popen(
        f"sed -i 's/merged\\.bam/{sample}.bam/g' {sample_dir}/fastqc_data.txt",
        shell=True,
    )
    sp.Popen(
        f"sed -i 's/merged\\.bam/{sample}.bam/g' {sample_dir}/summary.txt",
        shell=True,
    )