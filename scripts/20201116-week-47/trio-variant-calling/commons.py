# Common utilities for snakemake jobs

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
from tempfile import TemporaryDirectory
from snakemake.shell import shell
from snakemake.logging import logger


# Generate memory-related arguments for JRE/JDK CLI
def get_java_mem(resources, factor=1):
    memory = ""
    if "mem_mb" in resources.keys():
        # Only allocate a portion of the total memory
        memory = "-Xmx{}M".format(int(int(resources["mem_mb"]) * 0.9))

    return memory


# Generate JRE/JDK opts
def get_java_opts(params, resources, factor=1):
    java_opts = params.get("java_opts", "")
    java_mem = get_java_mem(resources, factor)
    if "-Xmx" not in java_opts:
        java_opts = f"{java_opts} {java_mem} "

    return java_opts


def get_extra(params):
    return params.get("extra", "")


# Check if the job is running in a Kubernetes cluster
def is_kubernetes_mode():
    import os

    return bool(os.getenv("KUBERNETES_SERVICE_HOST"))
