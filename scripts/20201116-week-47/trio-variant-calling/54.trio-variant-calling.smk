# from snakemake.remote.S3 import RemoteProvider as S3RemoteProvider
# S3 = S3RemoteProvider(enable_cache=True, cache_ttl=120)
# from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider
# FTP = FTPRemoteProvider()
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
HTTP = HTTPRemoteProvider(enable_cache=True)

import math

CHROMS = [str(v) for v in range(1, 23)] + ["X", "Y"]

BUCKET = "cofrag2"

# Modules
include: "rules/bam.processing.smk"
include: "rules/qc.smk"
include: "rules/variant.calling.smk"
# include: "52.fetal-genotyping.rules/bam.processing.smk"
# include: "52.fetal-genotyping.rules/bqsr.smk"
# include: "52.fetal-genotyping.rules/genotype_calling.smk"
# include: "52.fetal-genotyping.rules/vqsr.smk"
