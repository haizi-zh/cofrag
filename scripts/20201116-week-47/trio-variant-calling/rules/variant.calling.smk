def load_genome_layout():
    import csv
    import os

    genome_layout_file = os.path.normpath(
        os.path.join(workflow.current_basedir, "../genome.layout.tsv")
    )
    with open(genome_layout_file, "r") as f:
        reader = csv.reader(f, delimiter="\t")
        next(reader)

        def process_row(row):
            chrom = row[0]
            start = int(row[1])
            end = int(row[2])
            return {"chrom": chrom, "start": start, "end": end}

        return [process_row(row) for row in reader]


rule call_haplotypes:
    input:
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        bam="results/20201116-week-47/trio-variant-calling/bam-bqsr/{sample_name}.chr{chrom}.bqsr.bam",
        bai="results/20201116-week-47/trio-variant-calling/bam-bqsr/{sample_name}.chr{chrom}.bqsr.bam.bai",
    output:
        gvcf="results/20201116-week-47/trio-variant-calling/haplotype-scattered/{sample_name,[^\\.]+}.chr{chrom,[0-9XY]+}:{start,[0-9]+}-{end,[0-9]+}.g.vcf.gz",
        gvcf_tbi="results/20201116-week-47/trio-variant-calling/haplotype-scattered/{sample_name}.chr{chrom}:{start}-{end}.g.vcf.gz.tbi",
        log="results/20201116-week-47/trio-variant-calling/haplotype-scattered/{sample_name}.chr{chrom}:{start}-{end}.log",
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/7f98c5/bio/gatk/haplotypecaller/environment.yaml"
    params:
        k8s_node_selector={"diskvol": "12x"}
    threads: 3
    resources:
        mem_mb=6200
    script:
        "variant.calling.py"


def aggregate_haplotype_scatters(chrom):
    prefix = workflow.default_remote_prefix if workflow.default_remote_prefix else ""


    pattern = f"{prefix}/results/20201116-week-47/trio-variant-calling/haplotype-scattered/{{sample_name}}.chr{chrom}:{{region}}.g.vcf.gz"
    if workflow.default_remote_provider:
        sample_name, region = workflow.default_remote_provider.glob_wildcards(pattern)
    else:
        sample_name, region = glob_wildcards(pattern)

    n = len(sample_name)
    logger.info(f"Found {n} scattered items for chr{chrom}")
    gvcfs = [f"results/20201116-week-47/trio-variant-calling/haplotype-scattered/{sample_name[idx]}.chr{chrom}:{region[idx]}.g.vcf.gz" for idx in range(n)]
    gvcfs_tbi = [f"{gvcf}.tbi" for gvcf in gvcfs]
    # Return GVCF files and the corresponding tbi files
    return gvcfs + gvcfs_tbi


rule gather_haplotypes:
    input:
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        gvcfs_all=lambda wildcards: aggregate_haplotype_scatters(wildcards.chrom)
    output:
        gvcf="results/20201116-week-47/trio-variant-calling/haplotype-gathered/multi-sample.chr{chrom,[0-9XY]+}.g.vcf.gz",
        gvcf_tbi="results/20201116-week-47/trio-variant-calling/haplotype-gathered/multi-sample.chr{chrom}.g.vcf.gz.tbi",
    log:
        "results/20201116-week-47/trio-variant-calling/haplotype-gathered/multi-sample.chr{chrom}.log",
    params:
        k8s_node_selector={"diskvol": "12x"}
    threads: 3
    resources:
        mem_mb=6200
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/combinegvcfs/environment.yaml"
    script:
        "variant.calling.py"


rule genotype_gvcfs:
    input:
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        dbsnp=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz"),
        dbsnp_tbi=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz.tbi"),
        gvcf="results/20201116-week-47/trio-variant-calling/haplotype-gathered/multi-sample.chr{chrom}.g.vcf.gz",
        gvcf_tbi="results/20201116-week-47/trio-variant-calling/haplotype-gathered/multi-sample.chr{chrom}.g.vcf.gz.tbi",
    output:
        vcf="results/20201116-week-47/trio-variant-calling/genotype/joint.chr{chrom,[0-9XY]+}.vcf.gz",
        vcf_tbi="results/20201116-week-47/trio-variant-calling/genotype/joint.chr{chrom}.vcf.gz.tbi",
    log:
        "results/20201116-week-47/trio-variant-calling/genotype/joint.chr{chrom}.log"
    params:
        k8s_node_selector={"diskvol": "12x"}
    threads: 3
    resources:
        mem_mb=6200
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "variant.calling.py"


rule sites_only_vcf:
    input:
        vcfs=expand("results/20201116-week-47/trio-variant-calling/genotype/joint.chr{chrom}.vcf.gz", chrom=CHROMS),
        vcfs_tbi=expand("results/20201116-week-47/trio-variant-calling/genotype/joint.chr{chrom}.vcf.gz.tbi", chrom=CHROMS),
    output:
        vcf="results/20201116-week-47/trio-variant-calling/genotype/merged.sites_only.vcf.gz",
        vcf_tbi="results/20201116-week-47/trio-variant-calling/genotype/merged.sites_only.vcf.gz.tbi",
    log:
        "results/20201116-week-47/trio-variant-calling/genotype/merged.sites_only.log"
    params:
        k8s_node_selector={"diskvol": "12x"}
    threads: 4
    resources:
        mem_mb=8192
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "variant.calling.py"


rule vqsr_model_indels:
    input: 
        vcf="results/20201116-week-47/trio-variant-calling/genotype/merged.sites_only.vcf.gz",
        vcf_tbi="results/20201116-week-47/trio-variant-calling/genotype/merged.sites_only.vcf.gz.tbi",
        mills="data/broad/hg19/hg19_v0_Mills_and_1000G_gold_standard.indels.b37.sites.vcf",
        mills_idx="data/broad/hg19/hg19_v0_Mills_and_1000G_gold_standard.indels.b37.sites.vcf.idx",
        axiom_poly="data/broad/hg19/hg19_v0_Axiom_Exome_Plus.genotypes.all_populations.poly.vcf.gz",
        axiom_poly_tbi="data/broad/hg19/hg19_v0_Axiom_Exome_Plus.genotypes.all_populations.poly.vcf.gz.tbi",
        dbsnp=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz"),
        dbsnp_tbi=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz.tbi")
    output: 
        tranches="results/20201116-week-47/trio-variant-calling/vqsr/model.indels.tranches",
        recal="results/20201116-week-47/trio-variant-calling/vqsr/model.indels.recal",
        recal_idx="results/20201116-week-47/trio-variant-calling/vqsr/model.indels.recal.idx",
    log:
        "results/20201116-week-47/trio-variant-calling/vqsr/model.indels.log"
    params:
        k8s_node_selector={"diskvol": "12x"}
    threads: 7
    resources:
        mem_mb=15360
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "variant.calling.py"


rule vqsr_model_snps:
    input: 
        vcf="results/20201116-week-47/trio-variant-calling/genotype/merged.sites_only.vcf.gz",
        vcf_tbi="results/20201116-week-47/trio-variant-calling/genotype/merged.sites_only.vcf.gz.tbi",
        hapmap="data/broad/hg19/hg19_v0_hapmap_3.3.b37.vcf.gz",
        hapmap_tbi="data/broad/hg19/hg19_v0_hapmap_3.3.b37.vcf.gz.tbi",
        omni="data/broad/hg19/hg19_v0_1000G_omni2.5.b37.vcf.gz",
        omni_tbi="data/broad/hg19/hg19_v0_1000G_omni2.5.b37.vcf.gz.tbi",
        g1k="data/broad/hg19/hg19_v0_1000G_phase1.snps.high_confidence.b37.vcf.gz",
        g1k_tbi="data/broad/hg19/hg19_v0_1000G_phase1.snps.high_confidence.b37.vcf.gz.tbi",
        dbsnp=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz"),
        dbsnp_tbi=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz.tbi")
    output: 
        tranches="results/20201116-week-47/trio-variant-calling/vqsr/model.snps.tranches",
        recal="results/20201116-week-47/trio-variant-calling/vqsr/model.snps.recal",
        recal_idx="results/20201116-week-47/trio-variant-calling/vqsr/model.snps.recal.idx",
    log:
        "results/20201116-week-47/trio-variant-calling/vqsr/model.snps.log"
    params:
        k8s_node_selector={"diskvol": "12x"}
    threads: 7
    resources:
        mem_mb=15360
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "variant.calling.py"


rule vqsr_apply_model:
    input: 
        vcf="results/20201116-week-47/trio-variant-calling/genotype/joint.chr{chrom}.vcf.gz",
        vcf_tbi="results/20201116-week-47/trio-variant-calling/genotype/joint.chr{chrom}.vcf.gz.tbi",
        snps_tranches="results/20201116-week-47/trio-variant-calling/vqsr/model.snps.tranches",
        snps_recal="results/20201116-week-47/trio-variant-calling/vqsr/model.snps.recal",
        snps_recal_idx="results/20201116-week-47/trio-variant-calling/vqsr/model.snps.recal.idx",
        indels_tranches="results/20201116-week-47/trio-variant-calling/vqsr/model.indels.tranches",
        indels_recal="results/20201116-week-47/trio-variant-calling/vqsr/model.indels.recal",
        indels_recal_idx="results/20201116-week-47/trio-variant-calling/vqsr/model.indels.recal.idx",
    output: 
        vcf="results/20201116-week-47/trio-variant-calling/genotype/vqsr.chr{chrom,[0-9XY]+}.vcf.gz",
        vcf_tbi="results/20201116-week-47/trio-variant-calling/genotype/vqsr.chr{chrom}.vcf.gz.tbi",
    log:
        "results/20201116-week-47/trio-variant-calling/genotype/vqsr.chr{chrom}.log"
    params:
        k8s_node_selector={"diskvol": "12x"}
    threads: 5
    resources:
        mem_mb=10240
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "variant.calling.py"


rule gather_final_vcf:
    input:
        vcfs=expand("results/20201116-week-47/trio-variant-calling/genotype/vqsr.chr{chrom}.vcf.gz", chrom=CHROMS),
        vcfs_tbi=expand("results/20201116-week-47/trio-variant-calling/genotype/vqsr.chr{chrom}.vcf.gz.tbi", chrom=CHROMS),
    output:
        vcf="results/20201116-week-47/trio-variant-calling/genotype/final_genotype.vcf.gz",
        vcf_tbi="results/20201116-week-47/trio-variant-calling/genotype/final_genotype.vcf.gz.tbi",
    log:
        "results/20201116-week-47/trio-variant-calling/genotype/final_genotype.log"
    params:
        k8s_node_selector={"diskvol": "12x", "ec2": "r4x"}
    threads: 3
    resources:
        mem_mb=lambda wildcards, threads: int(threads * 10 * 1024 * 0.95)
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "variant.calling.py"


rule final_vcf_metrics:
    input:
        vcf="results/20201116-week-47/trio-variant-calling/genotype/final_genotype.vcf.gz",
        vcf_tbi="results/20201116-week-47/trio-variant-calling/genotype/final_genotype.vcf.gz.tbi",
        dbsnp=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz"),
        dbsnp_tbi=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz.tbi"),
    output:
        details="results/20201116-week-47/trio-variant-calling/genotype/final_genotype.variant_calling_detail_metrics",
        summary="results/20201116-week-47/trio-variant-calling/genotype/final_genotype.variant_calling_summary_metrics",
    log:
        "results/20201116-week-47/trio-variant-calling/genotype/final_genotype.variant_calling_metrics.log"
    params:
        k8s_node_selector={"diskvol": "12x", "ec2": "r4x"}
    threads: 3
    resources:
        mem_mb=lambda wildcards, threads: int(threads * 10 * 1024 * 0.95)
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/genotypegvcfs/environment.yaml"
    script:
        "variant.calling.py"