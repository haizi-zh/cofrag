# In the original BAM files, some RG fields are missing, such as platform
# information 
rule fix_read_groups:
    input: 
        "results/20201109-week-46/fetal-genome/bam-chrom/{sample_name}.chr{chrom}.bam"
    output:
        bam="results/20201116-week-47/trio-variant-calling/bam/{sample_name,[^\\.]+}.chr{chrom,[0-9XY]+}.bam",
        bai="results/20201116-week-47/trio-variant-calling/bam/{sample_name}.chr{chrom}.bam.bai"
    log:
        "results/20201116-week-47/trio-variant-calling/bam/{sample_name}.chr{chrom}.log"
    threads: 2
    params:
        k8s_node_selector={"diskvol": "8x"}
    resources:
        mem_mb=4096
    conda:
        "conda.env.common.yaml"
    script: "bam.processing.py"


rule recalibrate_base_quality:
    input:
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        dbsnp=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz"),
        dbsnp_tbi=HTTP.remote("https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/00-common_all.vcf.gz.tbi"),
        bam="results/20201116-week-47/trio-variant-calling/bam/{sample}.chr{chrom}.bam",
        bai="results/20201116-week-47/trio-variant-calling/bam/{sample}.chr{chrom}.bam.bai",
    output:
        "results/20201116-week-47/trio-variant-calling/bam-bqsr/{sample,[^\\.]+}.chr{chrom,[0-9XY]+}.bqsr.table"
    log:
        "results/20201116-week-47/trio-variant-calling/bam-bqsr/{sample,[^\\.]+}.chr{chrom}.bqsr.table.log"
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/baserecalibrator/environment.yaml"
    params:
        k8s_node_selector={"diskvol": "12x"}
    threads: 3
    resources:
        mem_mb=6200
    script:
        "bam.processing.py"


rule apply_bqsr:
    input:
        ref="data/ref_genome/hs37-1kg/human_g1k_v37.fasta",
        ref_fai="data/ref_genome/hs37-1kg/human_g1k_v37.fasta.fai",
        ref_dict="data/ref_genome/hs37-1kg/human_g1k_v37.dict",
        bam="results/20201116-week-47/trio-variant-calling/bam/{sample}.chr{chrom}.bam",
        bai="results/20201116-week-47/trio-variant-calling/bam/{sample}.chr{chrom}.bam.bai",
        bqsr="results/20201116-week-47/trio-variant-calling/bam-bqsr/{sample}.chr{chrom}.bqsr.table",
    output:
        bam="results/20201116-week-47/trio-variant-calling/bam-bqsr/{sample,[^\\.]+}.chr{chrom,[0-9XY]+}.bqsr.bam",
        bai="results/20201116-week-47/trio-variant-calling/bam-bqsr/{sample}.chr{chrom}.bqsr.bam.bai",
    log:
        "results/20201109-week-46/fetal-genome/bqsr/{sample}.chr{chrom}.bqsr.bam.log"
    conda:
        "https://raw.githubusercontent.com/haizi-zh/snakemake-wrappers/98f920/bio/gatk/baserecalibrator/environment.yaml"
    params:
        k8s_node_selector={"diskvol": "12x"}
    threads: 3
    resources:
        mem_mb=6200
    script:
        "bam.processing.py"