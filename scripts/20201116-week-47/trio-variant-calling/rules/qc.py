# Generate QC reports

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
import os
import sys

# smk file home location
smk_path = path.normpath(path.dirname(__file__) + "/..")
# Import rule-commons
sys.path = [smk_path] + sys.path

from commons import *


def get_fastqc_report():
    import re

    logger.info(
        f"Running in Kubernetes mode: {'yes' if is_kubernetes_mode() else 'no'}"
    )

    # Split the BAM file by RG
    with TemporaryDirectory() as tempdir:
        temp_log = f"{tempdir}/output.log"

        # Merge BAMs
        temp_bam = f"{tempdir}/merged.bam"
        shell(
            "samtools merge "
            "--no-PG --threads {snakemake.threads} "
            "-c -p "
            "{temp_bam} "
            f"{' '.join(snakemake.input.bam)} "
            "2> >(tee -a {temp_log} >&2) "
        )

        if is_kubernetes_mode():
            shell(
                "echo Deleting input file copies on the node: {snakemake.input.bam} "
                "2> >(tee -a {temp_log} >&2)"
            )
            shell("rm {snakemake.input.bam}")

        # Generate the FASTQC report
        shell(
            "fastqc --threads {snakemake.threads} "
            "--outdir {tempdir} {temp_bam} "
            "2> >(tee -a {temp_log} >&2) "
        )

        shell("ls -la {tempdir}")

        shell("mv {tempdir}/merged_fastqc.html {snakemake.output.html}")
        shell("mv {tempdir}/merged_fastqc.zip {snakemake.output.zip}")
        shell("mv {temp_log} {snakemake.log}")


if snakemake.rule == "fastqc":
    get_fastqc_report()
elif snakemake.rule == "fastqc_bqsr":
    get_fastqc_report()
else:
    raise RuntimeError(f"Invalid rule: {snakemake.rule}")
