rule fastqc:
    input:
        bam=expand("results/20201116-week-47/trio-variant-calling/bam/{{sample}}.chr{chrom}.bam", chrom=CHROMS),
        bai=expand("results/20201116-week-47/trio-variant-calling/bam/{{sample}}.chr{chrom}.bam.bai", chrom=CHROMS)
    output:
        html="results/20201116-week-47/trio-variant-calling/fastqc/{sample}_fastqc.html",
        zip="results/20201116-week-47/trio-variant-calling/fastqc/{sample}_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc to find the file. If not using multiqc, you are free to choose an arbitrary filename
    log:
        "results/20201116-week-47/trio-variant-calling/fastqc/{sample}.log"
    params:
        k8s_node_selector={"diskvol": "12x", "ec2": "r4x"}
    threads: 3
    resources:
        mem_mb=lambda wildcards, threads: int(threads * 10 * 1024 * 0.95)
    conda:
        "qc.environment.yaml"
    script:
        "qc.py"


rule fastqc_bqsr:
    input:
        bam=expand("results/20201116-week-47/trio-variant-calling/bam-bqsr/{{sample}}.chr{chrom}.bqsr.bam", chrom=CHROMS),
        bai=expand("results/20201116-week-47/trio-variant-calling/bam-bqsr/{{sample}}.chr{chrom}.bqsr.bam.bai", chrom=CHROMS)
    output:
        html="results/20201116-week-47/trio-variant-calling/fastqc-bqsr/{sample}_fastqc.html",
        zip="results/20201116-week-47/trio-variant-calling/fastqc-bqsr/{sample}_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc to find the file. If not using multiqc, you are free to choose an arbitrary filename
    log:
        "results/20201116-week-47/trio-variant-calling/fastqc-bqsr/{sample}.log"
    params:
        k8s_node_selector={"diskvol": "12x", "ec2": "r4x"}
    threads: 3
    resources:
        mem_mb=lambda wildcards, threads: int(threads * 10 * 1024 * 0.95)
    conda:
        "qc.environment.yaml"
    script:
        "qc.py"