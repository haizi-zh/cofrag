# Perform individual variant calling (per sample)

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
import os
import sys
import re

# smk file home location
smk_path = path.normpath(path.dirname(__file__) + "/..")
# Import rule-commons
sys.path = [smk_path] + sys.path

from commons import *


def call_haplotypes():
    java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.95)

    with TemporaryDirectory() as tempdir:
        ref = snakemake.input.ref
        temp_gvcf = path.join(tempdir, "output.g.vcf.gz")
        temp_gvcf_tbi = path.join(tempdir, "output.g.vcf.gz.tbi")
        temp_log = path.join(tempdir, "output.log")

        shell(
            "gatk --java-options {java_opts} "
            "HaplotypeCaller "
            "--pcr-indel-model NONE "
            "--create-output-variant-index "
            "-ERC GVCF "
            "-DF NotDuplicateReadFilter "
            "-G StandardAnnotation -G StandardHCAnnotation -G AS_StandardAnnotation "
            "-GQB 10 -GQB 20 -GQB 30 -GQB 40 -GQB 50 -GQB 60 -GQB 70 -GQB 80 -GQB 90 "
            "-L {snakemake.wildcards.chrom}:{snakemake.wildcards.start}-{snakemake.wildcards.end} "
            "-R {snakemake.input.ref} "
            "-I {snakemake.input.bam} "
            "-O {temp_gvcf} "
            "2> >(tee {temp_log} >&2) "
        )

        # Move outputs into proper position.
        shell("mv {temp_gvcf} {snakemake.output.gvcf}")
        shell("mv {temp_gvcf_tbi} {snakemake.output.gvcf_tbi}")
        shell("mv {temp_log} {snakemake.output.log}")


def gather_haplotypes():
    java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.95)

    # Separate .tbi files from GVCF files
    gvcfs = [item for item in snakemake.input.gvcfs_all if item.endswith(".g.vcf.gz")]
    gvcfs = list(map("-V {}".format, gvcfs))

    with TemporaryDirectory() as tempdir:
        temp_gvcf = path.join(tempdir, "output.g.vcf.gz")

        shell(
            "gatk --java-options '{java_opts}' "
            "CombineGVCFs "
            "{gvcfs} "
            "-R {snakemake.input.ref} "
            "-O {temp_gvcf} "
            "-L {snakemake.wildcards.chrom} "
            "-G StandardAnnotation -G AS_StandardAnnotation "
            "2> >(tee {snakemake.log} >&2) "
        )

        # Move outputs into proper position.
        shell("mv {temp_gvcf} {snakemake.output.gvcf}")
        shell("mv {temp_gvcf}.tbi {snakemake.output.gvcf_tbi}")


def genotype_gvcfs():
    java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.95)

    with TemporaryDirectory() as tempdir:
        temp_vcf = path.join(tempdir, "output.vcf.gz")

        shell(
            "gatk --java-options '{java_opts}' "
            "GenotypeGVCFs "
            "-R {snakemake.input.ref} "
            "-V {snakemake.input.gvcf} "
            "-O {temp_vcf} "
            "-D {snakemake.input.dbsnp} "
            "-L {snakemake.wildcards.chrom} "
            "--only-output-calls-starting-in-intervals "
            "-G StandardAnnotation -G AS_StandardAnnotation "
            # "--use-new-qual-calculator "  # No needed in GATK 4.1.9.0
            "--merge-input-intervals "
            "2> >(tee {snakemake.log} >&2) "
        )

        # Move outputs into proper position.
        shell("mv {temp_vcf} {snakemake.output.vcf}")
        shell("mv {temp_vcf}.tbi {snakemake.output.vcf_tbi}")


def sites_only_vcf():
    java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.95)

    shell("echo '' > {snakemake.log}")
    with TemporaryDirectory() as tempdir:
        # Make sites-only VCFs
        for idx in range(len(snakemake.input.vcfs)):
            input_vcf = snakemake.input.vcfs[idx]
            temp_vcf = path.join(tempdir, f"output.{idx}.vcf")

            shell(
                "gatk --java-options '{java_opts}' "
                "MakeSitesOnlyVcf "
                "-I {input_vcf} "
                "-O {temp_vcf} "
                "2> >(tee -a {snakemake.log} >&2) "
            )

        # Gather them all
        temp_vcfs = [
            path.join(tempdir, f"output.{idx}.vcf")
            for idx in range(len(snakemake.input.vcfs))
        ]

        # Remove unnecessary input files
        if is_kubernetes_mode():
            shell(
                "echo Deleting input file copies on the node: {snakemake.input.vcfs} "
                "2> >(tee -a {snakemake.log} >&2)"
            )
            shell("rm {snakemake.input.vcfs}")

        temp_vcfs = " ".join([f"-I {v}" for v in temp_vcfs])
        temp_gathered = path.join(tempdir, "output_gathered.vcf.gz")
        shell(
            "gatk --java-options '{java_opts}' "
            "GatherVcfs "
            "--CREATE_INDEX "
            "{temp_vcfs} "
            "-O {temp_gathered} "
            "2> >(tee -a {snakemake.log} >&2) "
        )

        # Generate the index
        shell(
            "gatk --java-options '{java_opts}' "
            "IndexFeatureFile "
            "-I {temp_gathered} "
            "2> >(tee -a {snakemake.log} >&2) "
        )

        # Move outputs into proper position.
        shell("mv {temp_gathered} {snakemake.output.vcf}")
        shell("mv {temp_gathered}.tbi {snakemake.output.vcf_tbi}")


def vqsr_model_indels():
    java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.95)

    with TemporaryDirectory() as tempdir:
        temp_tranches = path.join(tempdir, "indels.tranches")
        temp_recal = path.join(tempdir, "indels.recal")
        temp_recal_idx = path.join(tempdir, "indels.recal.idx")

        tranche_values = [
            "100.0",
            "99.95",
            "99.9",
            "99.5",
            "99.0",
            "97.0",
            "96.0",
            "95.0",
            "94.0",
            "93.5",
            "93.0",
            "92.0",
            "91.0",
            "90.0",
        ]
        tranche_args = " ".join([f"-tranche {v}" for v in tranche_values])
        annotation_values = ["FS", "ReadPosRankSum", "MQRankSum", "QD", "SOR", "DP"]
        annotation_args = " ".join([f"-an {v}" for v in annotation_values])

        shell(
            "gatk --java-options '{java_opts}' "
            "VariantRecalibrator "
            "-O {temp_recal} "
            "-V {snakemake.input.vcf} "
            "--tranches-file {temp_tranches} "
            "--trust-all-polymorphic "
            "{tranche_args} "
            "{annotation_args} "
            "--use-allele-specific-annotations "
            "-mode INDEL "
            "--max-gaussians 4 "
            "-resource:mills,known=false,training=true,truth=true,prior=12 {snakemake.input.mills} "
            "-resource:axiomPoly,known=false,training=true,truth=false,prior=10 {snakemake.input.axiom_poly} "
            "-resource:dbsnp,known=true,training=false,truth=false,prior=2 {snakemake.input.dbsnp} "
            "2> >(tee {snakemake.log} >&2) "
        )

        # Move outputs into proper position.
        shell("mv {temp_tranches} {snakemake.output.tranches}")
        shell("mv {temp_recal} {snakemake.output.recal}")
        shell("mv {temp_recal_idx} {snakemake.output.recal_idx}")


def vqsr_model_snps():
    java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.95)

    with TemporaryDirectory() as tempdir:
        temp_tranches = path.join(tempdir, "nsps.tranches")
        temp_recal = path.join(tempdir, "snps.recal")
        temp_recal_idx = path.join(tempdir, "snps.recal.idx")

        tranche_values = [
            "100.0",
            "99.95",
            "99.9",
            "99.8",
            "99.6",
            "99.5",
            "99.4",
            "99.3",
            "99.0",
            "98.0",
            "97.0",
            "90.0",
        ]
        tranche_args = " ".join([f"-tranche {v}" for v in tranche_values])
        annotation_values = [
            "QD",
            "MQRankSum",
            "ReadPosRankSum",
            "FS",
            "MQ",
            "SOR",
            "DP",
        ]
        annotation_args = " ".join([f"-an {v}" for v in annotation_values])

        shell(
            "gatk --java-options '{java_opts}' "
            "VariantRecalibrator "
            "-O {temp_recal} "
            "-V {snakemake.input.vcf} "
            "--tranches-file {temp_tranches} "
            "--trust-all-polymorphic "
            "{tranche_args} "
            "{annotation_args} "
            "--use-allele-specific-annotations "
            "-mode SNP "
            "--max-gaussians 6 "
            "-resource:hapmap,known=false,training=true,truth=true,prior=15 {snakemake.input.hapmap} "
            "-resource:omni,known=false,training=true,truth=true,prior=12 {snakemake.input.omni} "
            "-resource:1000G,known=false,training=true,truth=false,prior=10 {snakemake.input.g1k} "
            "-resource:dbsnp,known=true,training=false,truth=false,prior=7 {snakemake.input.dbsnp} "
            "2> >(tee {snakemake.log} >&2) "
        )

        # Move outputs into proper position.
        shell("mv {temp_tranches} {snakemake.output.tranches}")
        shell("mv {temp_recal} {snakemake.output.recal}")
        shell("mv {temp_recal_idx} {snakemake.output.recal_idx}")


def vqsr_apply_model():
    java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.95)

    shell("echo '' > {snakemake.log}")
    with TemporaryDirectory() as tempdir:
        temp_vcf1 = path.join(tempdir, "output.1.vcf.gz")
        temp_vcf2 = path.join(tempdir, "output.2.vcf.gz")

        # Apply the indels model
        shell(
            "gatk --java-options '{java_opts}' "
            "ApplyVQSR "
            "-O {temp_vcf1} "
            "-V {snakemake.input.vcf} "
            "--recal-file {snakemake.input.indels_recal} "
            "--tranches-file {snakemake.input.indels_tranches} "
            "--use-allele-specific-annotations "
            "--truth-sensitivity-filter-level 99.0 "
            "--create-output-variant-index true "
            "-mode INDEL "
            "2> >(tee -a {snakemake.log} >&2) "
        )

        # Apply the SNPs model
        shell(
            "gatk --java-options '{java_opts}' "
            "ApplyVQSR "
            "-O {temp_vcf2} "
            "-V {temp_vcf1} "
            "--recal-file {snakemake.input.snps_recal} "
            "--tranches-file {snakemake.input.snps_tranches} "
            "--use-allele-specific-annotations "
            "--truth-sensitivity-filter-level 99.7 "
            "--create-output-variant-index true "
            "-mode SNP "
            "2> >(tee -a {snakemake.log} >&2) "
        )

        # Move outputs into proper position.
        shell("mv {temp_vcf2} {snakemake.output.vcf}")
        shell("mv {temp_vcf2}.tbi {snakemake.output.vcf_tbi}")


def gather_final_vcf():
    java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.95)

    with TemporaryDirectory() as tempdir:
        temp_vcf = path.join(tempdir, "output.vcf.gz")

        # Gather them all
        input_vcfs = " ".join([f"-I {v}" for v in snakemake.input.vcfs])
        shell(
            "gatk --java-options '{java_opts}' "
            "GatherVcfs "
            "--CREATE_INDEX "
            "{input_vcfs} "
            "-O {temp_vcf} "
            "2> >(tee {snakemake.log} >&2) "
        )

        # Generate the index
        shell(
            "gatk --java-options '{java_opts}' "
            "IndexFeatureFile "
            "-I {temp_vcf} "
            "2> >(tee -a {snakemake.log} >&2) "
        )

        # Move outputs into proper position.
        shell("mv {temp_vcf} {snakemake.output.vcf}")
        shell("mv {temp_vcf}.tbi {snakemake.output.vcf_tbi}")


def final_vcf_metrics():
    java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.95)

    with TemporaryDirectory() as tempdir:
        # Generate the report
        prefix = path.join(
            tempdir, snakemake.output.details.split("/")[-1].split(".")[0]
        )
        logger.info(f"Output prefix: {prefix}")
        shell(
            "gatk --java-options '{java_opts}' "
            "CollectVariantCallingMetrics "
            "--INPUT {snakemake.input.vcf} "
            "--DBSNP {snakemake.input.dbsnp} "
            "--OUTPUT {prefix} "
            "--THREAD_COUNT {snakemake.threads} "
            "2> >(tee {snakemake.log} >&2) "
        )

        # Move outputs into proper position.
        shell("mv {prefix}.variant_calling_detail_metrics {snakemake.output.details}")
        shell("mv {prefix}.variant_calling_summary_metrics {snakemake.output.summary}")


m = re.match("^split_bam_chr([0-9XY]+)", snakemake.rule)

if m:
    chrom = m.groups[1]
    split_bam(chrom)
elif snakemake.rule == "call_haplotypes":
    call_haplotypes()
elif snakemake.rule == "gather_haplotypes":
    gather_haplotypes()
elif snakemake.rule == "genotype_gvcfs":
    genotype_gvcfs()
elif snakemake.rule == "sites_only_vcf":
    sites_only_vcf()
elif snakemake.rule == "vqsr_model_indels":
    vqsr_model_indels()
elif snakemake.rule == "vqsr_model_snps":
    vqsr_model_snps()
elif snakemake.rule == "vqsr_apply_model":
    vqsr_apply_model()
elif snakemake.rule == "gather_final_vcf":
    gather_final_vcf()
elif snakemake.rule == "final_vcf_metrics":
    final_vcf_metrics()
else:
    raise RuntimeError(f"Invalid rule: {snakemake.rule}")