# BAM preprocessing tasks

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

from os import path
import os
import sys

# smk file home location
smk_path = path.normpath(path.dirname(__file__) + "/..")
# Import rule-commons
sys.path = [smk_path] + sys.path

from commons import *


def fix_read_groups():
    import re

    logger.info(
        f"Running in Kubernetes mode: {'yes' if is_kubernetes_mode() else 'no'}"
    )

    # Split the BAM file by RG
    with TemporaryDirectory() as tempdir:
        os.mkdir(f"{tempdir}/original")
        os.mkdir(f"{tempdir}/new")
        bam_prefix = f"{tempdir}/original/%!.%."

        temp_log = f"{tempdir}/output.log"

        shell(
            "samtools split -f {bam_prefix} "
            "-v --no-PG --threads {snakemake.threads} "
            "{snakemake.input} "
            "2> >(tee -a {temp_log} >&2) "
        )
        if is_kubernetes_mode():
            shell(
                "echo Deleting input file copies on the node: {snakemake.input} "
                "2> >(tee -a {temp_log} >&2)"
            )
            shell("rm {snakemake.input}")

        for bam_file in os.listdir(f"{tempdir}/original"):
            # For each single-RG BAM file, try fixing it
            m = re.match(
                "^([A-Z0-9]+)(_plasma)?_([A-Z0-9]+)_[^\\.]+_L([0-9]+)", bam_file
            )

            catch_groups = m.groups()
            sample_name = catch_groups[0] + (catch_groups[1] or "")
            flowcell = catch_groups[2]
            lane = int(catch_groups[3])

            m = re.match("^([^\\.]+)\\..+", bam_file)
            rgid = m.groups()[0]
            logger.info(
                f"Processed file {bam_file}: sample = {sample_name}, flowcell = {flowcell}, lane = {lane}, RGID = {rgid}"
            )

            rglb = sample_name
            rgid = f"{sample_name}.{flowcell}.{lane}"
            rgpu = rgid

            # gatk AddOrReplaceReadGroups I=C12148W.chr1_7.bam
            # O=C12148W.chr1_7.annotated.bam RGPL=ILLUMINA RGLB=C12148W
            # RGID=C12148W.C7Y5HACXX.8 RGSM=C12148W RGPU=C7Y5HACXX.8

            java_opts = get_java_opts(
                snakemake.params, snakemake.resources, factor=0.95
            )
            shell(
                "gatk --java-options '{java_opts}' "
                "AddOrReplaceReadGroups "
                "-O {tempdir}/new/{rgid}.bam "
                "-I {tempdir}/original/{bam_file} "
                "-RGPL ILLUMINA "
                "-RGID {rgid} "
                "-RGLB {rglb} "
                "-RGPU {rgpu} "
                "-RGSM {sample_name} "
                "2> >(tee -a {temp_log} >&2) "
            )
            if is_kubernetes_mode():
                shell(
                    "echo Deleting intermediate files: "
                    "{tempdir}/original/{bam_file} "
                    "2> >(tee -a {temp_log} >&2)"
                )
                shell("rm {tempdir}/original/{bam_file} ")

        # Merge BAMs
        temp_bam = f"{tempdir}/output.bam"
        input_bams = " ".join(
            [
                f"{tempdir}/new/{f}"
                for f in os.listdir(f"{tempdir}/new")
                if f.endswith(".bam")
            ]
        )
        shell(
            "samtools merge "
            "--no-PG --threads {snakemake.threads} "
            "{temp_bam} "
            "{input_bams} "
            "2> >(tee -a {temp_log} >&2) "
        )
        if is_kubernetes_mode():
            shell(
                "echo Deleting temporary files: {input_bams} "
                "2> >(tee -a {temp_log} >&2)"
            )
            shell("rm {input_bams}")

        # Create BAM index
        shell("samtools index {temp_bam} " "2> >(tee -a {temp_log} >&2) ")

        shell("mv {temp_bam} {snakemake.output.bam}")
        shell("mv {temp_bam}.bai {snakemake.output.bai}")
        shell("mv {temp_log} {snakemake.log}")


def recalibrate_base_quality():
    java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.95)

    with TemporaryDirectory() as tempdir:
        temp_out = path.join(tempdir, "output.table")
        temp_log = path.join(tempdir, "output.log")

        shell(
            "gatk --java-options '{java_opts}' "
            "BaseRecalibrator "
            # "{extra} "
            "-O {temp_out} "
            "-R {snakemake.input.ref} "
            "-I {snakemake.input.bam} "
            "-L {snakemake.wildcards.chrom} "
            "-known-sites {snakemake.input.dbsnp} "
            "--use-original-qualities "
            "2> >(tee {temp_log} >&2) "
        )

        if is_kubernetes_mode():
            shell(
                "echo Deleting input file copies on the node: {snakemake.input.bam} {snakemake.input.ref} {snakemake.input.dbsnp} "
            )
            shell(
                "rm {snakemake.input.bam} {snakemake.input.ref} {snakemake.input.dbsnp}"
            )

        # Move outputs into proper position.
        shell("mv {temp_out} {snakemake.output}")
        shell("mv {temp_log} {snakemake.log}")


def apply_bqsr():
    java_opts = get_java_opts(snakemake.params, snakemake.resources, factor=0.95)

    with TemporaryDirectory() as tempdir:
        temp_bam = path.join(tempdir, "output.bam")
        temp_bai = path.join(tempdir, "output.bai")
        temp_log = path.join(tempdir, "output.log")

        shell(
            "gatk --java-options '{java_opts}' "
            "ApplyBQSR "
            "-O {temp_bam} "
            "-R {snakemake.input.ref} "
            "-I {snakemake.input.bam} "
            "-bqsr {snakemake.input.bqsr} "
            "-L {snakemake.wildcards.chrom} "
            "--add-output-sam-program-record "
            "--use-original-qualities "
            "--create-output-bam-index "
            "2> >(tee {temp_log} >&2) "
        )

        if is_kubernetes_mode():
            shell(
                "echo Deleting input file copies on the node: {snakemake.input.bam} {snakemake.input.ref} "
            )
            shell("rm {snakemake.input.bam} {snakemake.input.ref} ")

        # Move outputs into proper position.
        shell("mv {temp_bam} {snakemake.output.bam}")
        shell("mv {temp_bai} {snakemake.output.bai}")
        shell("mv {temp_log} {snakemake.log}")


if snakemake.rule == "fix_read_groups":
    fix_read_groups()
elif snakemake.rule == "recalibrate_base_quality":
    recalibrate_base_quality()
elif snakemake.rule == "apply_bqsr":
    apply_bqsr()
else:
    raise RuntimeError(f"Invalid rule: {snakemake.rule}")
