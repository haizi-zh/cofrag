rule homer_hic_matrix:
    input: 
        "data/IMR90-hic-encode/IMR90_hic_encode.for_homer/tagInfo.txt"
    output:
        "results/20201116-week-47/hexa/IMR90-ENCODE/hi-c.chr{chrom,[0-9XY]+}.{res,[0-9]+}kb.txt"
    log:
        "results/20201116-week-47/hexa/IMR90-ENCODE/hi-c.chr{chrom}.{res}kb.log"
    threads: 1
    resources:
        mem_mb=4 * 4096,
        time=480,
        time_min=60
    params:
        label=lambda wildcards: f"hic.chr{wildcards.chrom}.{wildcards.res}kb"
    script: "55.hexa.py"


rule homer_hic_matrix_balance:
    input: 
        "data/IMR90-hic-encode/IMR90_hic_encode.for_homer/tagInfo.txt"
    output:
        "results/20201116-week-47/hexa/IMR90-ENCODE/balance/hi-c.chr{chrom,[0-9XY]+}.{res,[0-9]+}kb.balance.txt"
    log:
        "results/20201116-week-47/hexa/IMR90-ENCODE/balance/hi-c.chr{chrom}.{res}kb.balance.log"
    threads: 1
    resources:
        mem_mb=4 * 4096,
        time=480,
        time_min=60
    params:
        label=lambda wildcards: f"hic.chr{wildcards.chrom}.{wildcards.res}kb.balance"
    script: "55.hexa.py"