# Generate Hi-C matrix using HOMER

__author__ = "Haizi Zheng"
__copyright__ = "Copyright 2020, Haizi Zheng"
__email__ = "haizi.zh@gmail.com"
__license__ = "MIT"

import os
from tempfile import TemporaryDirectory
from snakemake.shell import shell
from snakemake.logging import logger

def homer_hic_matrix(balance=False):
    with TemporaryDirectory() as tempdir:
        temp_output = os.path.join(tempdir, "output.txt")

        chrom = snakemake.wildcards.chrom
        resolution = int(snakemake.wildcards.res) * 1000
        window_size = resolution * 2

        balance_flag = "-balance" if balance else ""
        shell(
            "$SCRATCH/tools/homer/bin/analyzeHiC "
            "data/IMR90-hic-encode/IMR90_hic_encode.for_homer "
            "-pos {chrom} -res {resolution} -window {window_size} "
            "{balance_flag} "
            "2> >(tee -a {snakemake.log} >&2) "
            "> {temp_output} "
        )

        shell(
            "mv {temp_output} {snakemake.output}"
        )


def homer_hic_matrix_balance():
    homer_hic_matrix(balance=True)        


# Call procedure based on snakemake rule name
globals()[snakemake.rule]()