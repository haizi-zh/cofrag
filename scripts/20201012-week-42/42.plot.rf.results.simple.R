plot_track <- function(entry, predicted_comps, chr, frag_profile) {
  comp1 <- env_wbc$comps %>% filter(wbc == "wbc.rep1" & chr == !!chr)
  comp2 <- env_wbc$comps %>% filter(wbc == "wbc.rep2" & chr == !!chr)
  comp3 <- predicted_comps[[entry]] %>% 
    filter(chr == !!chr) %>% 
    arrange(start) %>% 
    transmute(start = start, end = end, score = score.x)
  
  pearson.c <- calc_compartment_correlation(comp1, comp3)
  spearman.c <- calc_compartment_correlation(comp1, comp3, method = "spearman")
  title <- cowplot::ggdraw() + cowplot::draw_label(str_interp("Compartment: ${entry} vs. wbc, chr${chr}, Pearson.: $[.2f]{pearson.c}, Spearman.: $[.2f]{spearman.c}"), size = 8)
  
  theme_simple <- theme(
    axis.line = element_blank(),
    axis.text.x = element_blank(),
    # axis.text.y = element_blank(),
    axis.ticks = element_blank(),
    axis.title.x = element_blank(),
    axis.title.y = element_blank(),
    legend.position = "none",
    panel.background = element_blank(),
    panel.border = element_blank(),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    plot.background = element_blank()
  )
  
  p1 <-
    comp1 %>% na.omit() %>% plot_compartments() + theme(legend.position = "none") + xlab("") + 
    # ylim(-0.05, .1) +
    theme_simple + geom_hline(yintercept = 0, size = 0.1)
  p2 <-
    comp2 %>% na.omit() %>% plot_compartments() + theme(legend.position = "none") + xlab("") + 
    # ylim(-0.05, .1) +
    theme_simple + geom_hline(yintercept = 0, size = 0.1)
  p3 <-
    comp3 %>% na.omit() %>% plot_compartments() + theme(legend.position = "none") + xlab("") + 
    # ylim(-0.05, .1) +
    theme_simple + geom_hline(yintercept = 0, size = 0.1)
  
  frag_data <- frag_profile %>% na.omit() %>% filter(coverage > 100)
  
  remove_outlier <- function(data, col_name) {
    values <- data[[col_name]] %>% na.omit()
    avg_v <- mean(values)
    quantile_v <- quantile(values)
    IQR_v <- IQR(values)
    min_v <- quantile_v[2] - 1.5 * IQR_v
    max_v <- quantile_v[4] + 1.5 * IQR_v
    data[between(values, min_v, max_v),]
  }
  
  # ylim_length <- c(quantile(frag_data$length)[2] - 1.5 * IQR(frag_data$length),
  #           quantile(frag_data$length)[4] + 1.5 * IQR(frag_data$length))
  p4 <- frag_data %>%
    mutate(length = (length - mean(length)) / sd(length)) %>%
    remove_outlier("length") %>%
    ggplot(aes(x = start, y = length)) + geom_col(width = 500e3L * 0.75) + 
    scale_x_continuous(labels = scales::comma) + 
    # coord_cartesian(ylim = ylim_length) +
    # coord_cartesian(ylim = c(164, 174)) +
    coord_cartesian(ylim = c(-2, 2)) +
    theme_simple + geom_hline(yintercept = 0, size = 0.1)
  
  # ylim_coverage <- c(quantile(frag_data$coverage)[2] - 1.5 * IQR(frag_data$coverage),
  #                  quantile(frag_data$coverage)[4] + 1.5 * IQR(frag_data$coverage))
  p5 <- frag_data %>%
    mutate(coverage = coverage / sum(coverage)) %>%
    mutate(coverage = (coverage - mean(coverage)) / sd(coverage)) %>%
    remove_outlier("coverage") %>%
    ggplot(aes(x = start, y = coverage)) + geom_col(width = 500e3L * 0.75) + 
    scale_x_continuous(labels = scales::comma) + 
    # coord_cartesian(ylim = ylim_coverage) +
    coord_cartesian(ylim = c(-1, 1)) +
    theme_simple + geom_hline(yintercept = 0, size = 0.1)
  
  cowplot::plot_grid(p1, p2, p3, p4, p5, ncol = 1)
}
plot_track("EE88158", env_rf$predicted_comps, "9", binned_cov_EE88158)
plot_track("EE87835", env_rf$predicted_comps, "9", binned_cov_EE87835)


env_rf$predicted_comps$EE88158 %>% select(chr, start, end)


bedr::bedr(
  method = "intersect",
  input = list(a = tmp %>% slice_head(n = 10e3), b = env_rf$predicted_comps$EE88158 %>% filter(chr == "14") %>% select(chr, start, end)),
  params = "-wa, -loj, "
)


(function(entry, predicted_comps) {
  data <- predicted_comps[[entry]] %>% na.omit() 
  lm_model <- lm(formula = score.y ~ score.x, data = data)
  r2 <- summary(lm_model)$r.squared
  pvalue <- summary(lm_model)$coefficients[2, 4]
  pvalue <- ifelse(pvalue < 2.2e-16, "< 2.2e-16", str_interp("$[.2f]{pvalue}"))
  
  theme_simple <- theme(
    axis.line = element_blank(),
    axis.line.x = element_line(size = 0.1),
    axis.line.y = element_line(size = 0.1),
    # axis.text.x = element_blank(),
    # axis.text.y = element_blank(),
    axis.ticks = element_blank(),
    axis.title.x = element_blank(),
    axis.title.y = element_blank(),
    legend.position = "none",
    panel.background = element_blank(),
    panel.border = element_blank(),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    plot.background = element_blank()
  )
  
  data %>%
    na.omit() %>%
    ggplot(aes(x = score.x, y = score.y)) +
    geom_point(color = "steelblue", alpha = 0.2, size = 1.5) + theme_simple
})("EE88158", env_rf$predicted_comps)

c("EE88158", "EE87835", "EE86217") %>% map_dfr(~ {
  entry <- .
  val <- env_rf$predicted_comps[[entry]]$score.x %>% sd(na.rm = TRUE)
  tibble(
    entry_id = entry,
    sd = val
  )
})

## Fragment profile
calc_binned_coverage <- function(entry, bin_size = 500e3L) {
  frags <- read_tsv(
    here(str_interp("results/20201012-week-42/rf/test/${entry}/${entry}.hg19.chr9.filtered.frag.len100_350.tsv.gz")),
                    col_names = c("chr", "start", "end", "mapq", "strand")) %>%
    mutate(chr = as.character(chr), start = as.integer(start), end = as.integer(end))
  
  frags %>% 
    mutate(bin_idx = start %/% bin_size, length = end - start) %>%
    group_by(chr, bin_idx) %>% 
    summarize(coverage = length(start), length = mean(length, na.rm = TRUE), .groups = "drop") %>%
    mutate(start = as.integer(bin_idx * bin_size), end = as.integer(start + bin_size)) %>%
    select(chr, start, end, coverage, length)
}

binned_cov_EE88158 <- calc_binned_coverage("EE88158")
binned_cov_EE87835 <- calc_binned_coverage("EE87835")

