rule unpack_fa:
        input:
                "ref/{ref_name}.fa.gz"
        output:
                protected("ref/{ref_name}.fa")
        shell:
                """
                set +e
                gunzip -k -c {input} > {output}
                exitcode=$?
                if [ $exitcode -eq 2 ]
                then
                        exit 0
                fi
                """

rule postproc_fa:
        input:
                "ref/{ref_name}.fa"
        output:
                temp("works/mappability/{ref_name}.postproc.fa")
        shell:
                "sed 's/\\(^>[^ ]\\+\\).\\+/\\1/g' {input} > {output}"

rule gem_indexer:
        input:
                "works/mappability/{ref_name}.postproc.fa"
        output:
                protected("works/mappability/{ref_name}.gem")
        threads: config["threads"]
        shell:
                "gem-indexer -T {threads} -c dna -i {input} -o works/mappability/{wildcards.ref_name}"

rule gem_mappability:
        input:
                "works/mappability/{ref_name}.gem"
        output:
                protected("works/mappability/{ref_name}.{kmer}mer.mappability")
        threads: config["threads"]
        shell:
                "gem-mappability -T {threads} -I {input} -l {wildcards.kmer} -o works/mappability/{wildcards.ref_name}.{wildcards.kmer}mer"

rule gem_wig:
        input:
                idx="works/mappability/{ref_name}.gem",
                mapb="works/mappability/{ref_name}.{kmer}mer.mappability"
        output:
                "works/mappability/{ref_name}.{kmer}mer.wig",
                "works/mappability/{ref_name}.{kmer}mer.sizes"
        shell:
                "gem-2-wig -I {input.idx} -i {input.mapb} -o works/mappability/{wildcards.ref_name}.{wildcards.kmer}mer"

rule wig_bw:
        input:
                "works/mappability/{ref_name}.{kmer}mer.wig",
                "works/mappability/{ref_name}.{kmer}mer.sizes"
        output:
                "works/mappability/{ref_name}.{kmer}mer.bw"
        conda:  "conda_env.yaml"
        shell:
                "wigToBigWig {input} {output}"

rule split_chr_bedgraph:
        input:  "works/mappability/{ref_name}.{kmer}mer.wig"
        output: temp("works/mappability/{ref_name}.{chr}.{kmer}mer.bedgraph")
        conda:  "conda_env.yaml"
        shell:
                "wig2bed < {input} | cut -f1-3,5 | sed -n '/^{wildcards.chr}/p' > {output}"

rule split_chr_bw:
        input:
                "works/mappability/{ref_name}.{chr}.{kmer}mer.bedgraph",
                "works/mappability/{ref_name}.{kmer}mer.sizes"
        output: "works/mappability/{ref_name}.{chr}.{kmer}mer.bw"
        conda:  "conda_env.yaml"
        shell:
                "bedGraphToBigWig {input} {output}"
