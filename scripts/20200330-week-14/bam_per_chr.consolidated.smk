configfile:     "bam_per_chr.smk.config"

rule get_bam:
       output: temp("bh01/bh01.chr{chr_name,[^\\.]+}.bam")
       conda:  "conda_env.yaml"
       shell:  "sam-dump --aligned-region {wildcards.chr_name} SRR2129993 | samtools view -b - > {output}"

rule consolidated_process:
        input:  "bh01/bh01.chr{chr_name}.bam"
        output:
            bam="bh01/bh01.chr{chr_name}.sorted.bam",
            bai="bh01/bh01.chr{chr_name}.sorted.bam.bai",
            filtered_bam="samtools_filter/bh01.chr{chr_name}.filtered.bam",
            filtered_bai="samtools_filter/bh01.chr{chr_name}.filtered.bam.bai"
        conda:  "conda_env.yaml"
        threads:    config["threads"]
        params:
            min_mapq=config["min_mapq"]
        shell:
            """
            samtools sort -O bam -o {output.bam} -@ {threads} {input}
            samtools index -@ {threads} {output.bam} {output.bai}

            samtools view -b -@ {threads} -q {params.min_mapq} -f 83 {output.bam} > samtools_filter/bh01.chr{wildcards.chr_name}.filtered.flag_83.bam
            samtools view -b -@ {threads} -q {params.min_mapq} -f 99 {output.bam} > samtools_filter/bh01.chr{wildcards.chr_name}.filtered.flag_99.bam

            samtools merge -@ {threads} {output.filtered_bam} samtools_filter/bh01.chr{wildcards.chr_name}.filtered.flag_83.bam samtools_filter/bh01.chr{wildcards.chr_name}.filtered.flag_99.bam
            rm samtools_filter/bh01.chr{wildcards.chr_name}.filtered.flag_83.bam
            rm samtools_filter/bh01.chr{wildcards.chr_name}.filtered.flag_99.bam

            samtools index -@ {threads} {output.filtered_bam} {output.filtered_bai}
            """
