from snakemake.remote.S3 import RemoteProvider as S3RemoteProvider
S3 = S3RemoteProvider()

S3_PREFIX = "cofrag.epifluid.cchmc.org/results/20200330-week-14"

envvars:
    "AWS_ACCESS_KEY_ID",
    "AWS_SECRET_ACCESS_KEY"

rule simple:
    output:  "summary.txt"
    shell:
        """
        env > {output}
        echo "Root: /" >> {output}
        ls -la / >> {output}
        echo "Current dir:" >> {output}
        ls -la . >> {output}
        """

rule filter_bam_reads:
    input:  
        samtools_filtered="samtools_filter/bh01.chr{chr}.filtered.bam",
        mappability="mappability/hs37d5.chr{chr}.45mer.bw"
    output: "gem_filter/bh01.chr{chr}.filtered.bam"
    threads: 14
    resources:
        mem_mb=14000
    shell:
        """
        Rscript --vanilla bam_filter.R -n {threads} -o {output} --grange chr22:1-50818468 --map-track {input.mappability} {input.samtools_filtered}
        """
