configfile:     "bam_per_chr.smk.config"

rule get_bam:
        output: temp("bh01/bh01.chr{chr_name,[^\\.]+}.bam")
        conda:  "conda_env.yaml"
        shell:  "sam-dump --aligned-region {wildcards.chr_name} SRR2129993 | samtools view -b - > {output}"

rule sort_bam:
        input:  "bh01/bh01.chr{chr_name}.bam"
        output:
                bam="bh01/bh01.chr{chr_name}.sorted.bam",
                bai="bh01/bh01.chr{chr_name}.sorted.bam.bai"
        conda:  "conda_env.yaml"
        threads:        config["threads"]
        shell:
                """
                samtools sort -O bam -o {output.bam} -@ {threads} {input}
                samtools index -@ {threads} {output.bam} {output.bai}
                """

rule samtools_filter:
        input:  "bh01/bh01.chr{chr_name}.sorted.bam"
        output:
                flag83=temp("samtools_filter/bh01.chr{chr_name}.filtered.flag_83.bam"),
                flag99=temp("samtools_filter/bh01.chr{chr_name}.filtered.flag_99.bam")
        conda:  "conda_env.yaml"
        threads:        config["threads"]
        params:
                min_mapq=config["min_mapq"]
        shell:
                """
                samtools view -b -@ {threads} -q {params.min_mapq} -f 83 {input} > {output.flag83}
                samtools view -b -@ {threads} -q {params.min_mapq} -f 99 {input} > {output.flag99}
                """

rule samtools_merge:
        input:
                "samtools_filter/bh01.chr{chr_name}.filtered.flag_83.bam",
                "samtools_filter/bh01.chr{chr_name}.filtered.flag_99.bam"
        output:
                bam="samtools_filter/bh01.chr{chr_name}.filtered.bam",
                bai="samtools_filter/bh01.chr{chr_name}.filtered.bam.bai"
        conda:  "conda_env.yaml"
        threads:        config["threads"]
        shell:
                """
                samtools merge -@ {threads} {output.bam} {input}
                samtools index -@ {threads} {output.bam} {output.bai}
                """


